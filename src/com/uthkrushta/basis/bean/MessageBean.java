package com.uthkrushta.basis.bean;

import java.util.ArrayList;

public class MessageBean {
    
    private String strMsgType;
    private boolean blnIsLeadingMessage;
    private String strMsgText;
    private String strSolution;
    private String strClassName;
    private String strMethodName;
    private String strUIObject;
        
    public MessageBean(String strMsgType, boolean blnIsLeadingMessage, String strMsgText) {
		super();
		this.strMsgType = strMsgType;
		this.blnIsLeadingMessage = blnIsLeadingMessage;
		this.strMsgText = strMsgText;
	}
    
	public MessageBean(String strMsgType, String strMsgText, String strSolution,
			String strClassName, String strMethodName, String strUIObject,boolean blnIsLeadingMessage) {
		super();
		this.strMsgType = strMsgType;
		this.strMsgText = strMsgText;
		this.strSolution = strSolution;
		this.strClassName = strClassName;
		this.strMethodName = strMethodName;
		this.strUIObject = strUIObject;
		this.blnIsLeadingMessage = blnIsLeadingMessage;
	}
	
	public MessageBean(){
		
	}
	
	public String getStrMsgType() {
		return strMsgType;
	}

	public void setStrMsgType(String strMsgType) {
		this.strMsgType = strMsgType;
	}

	public String getStrMsgText() {
		return strMsgText;
	}
	public void setStrMsgText(String strMsgText) {
		this.strMsgText = strMsgText;
	}
	public String getStrSolution() {
		return strSolution;
	}
	public void setStrSolution(String strSolution) {
		this.strSolution = strSolution;
	}
	public String getStrClassName() {
		return strClassName;
	}
	public void setStrClassName(String strClassName) {
		this.strClassName = strClassName;
	}
	public String getStrMethodName() {
		return strMethodName;
	}
	public void setStrMethodName(String strMethodName) {
		this.strMethodName = strMethodName;
	}
	public String getStrUIObject() {
		return strUIObject;
	}
	public void setStrUIObject(String strUIObject) {
		this.strUIObject = strUIObject;
	}

	public boolean getIsBlnIsLeadingMessage() {
		return blnIsLeadingMessage;
	}

	public void setBlnIsLeadingMessage(boolean blnIsLeadingMessage) {
		this.blnIsLeadingMessage = blnIsLeadingMessage;
	}
    
    public static ArrayList<MessageBean> getMessagesByType(String strMessageType, ArrayList<MessageBean> arrMsgList)
    {
    	MessageBean objMessage = null;
    	ArrayList<MessageBean> arrMsgReturnList = new ArrayList<MessageBean>();
    	for(int intIndex = 0; intIndex < arrMsgList.size(); intIndex++)
    	{
    		objMessage = (MessageBean) arrMsgList.get(intIndex);
    		if(objMessage.getStrMsgType().equalsIgnoreCase(strMessageType))
    		{
    			arrMsgReturnList.add(objMessage);
    		}
    		
    	}
    	return arrMsgReturnList;
    }
    
    public static MessageBean getLeadingMessage(ArrayList<MessageBean> arrMsgList)
    {
    	MessageBean objMessage = null;
    	for(int intIndex = 0; intIndex < arrMsgList.size(); intIndex++)
    	{
    		objMessage = (MessageBean) arrMsgList.get(intIndex);
    		if(objMessage.getIsBlnIsLeadingMessage())
    		{
    			return objMessage;
    		}
    		
    	}
    	return null;
    }
}
