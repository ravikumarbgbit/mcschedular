package com.uthkrushta.basis.bean;

public class NavigationBean {
	
	long lngID;
	long lngRoleID;
	long lngMenuID;
	long lngActivityID;
	
	long lngDisplayOrder;
	int intIsSecured;
	
	String strMenu;
	String strActivity;
	String strPageURL;
	
	public long getLngID() {
		return lngID;
	}
	public void setLngID(long lngID) {
		this.lngID = lngID;
	}
	public long getLngRoleID() {
		return lngRoleID;
	}
	public void setLngRoleID(long lngRoleID) {
		this.lngRoleID = lngRoleID;
	}
	public long getLngMenuID() {
		return lngMenuID;
	}
	public void setLngMenuID(long lngMenuID) {
		this.lngMenuID = lngMenuID;
	}
	public long getLngActivityID() {
		return lngActivityID;
	}
	public void setLngActivityID(long lngActivityID) {
		this.lngActivityID = lngActivityID;
	}
	public long getLngDisplayOrder() {
		return lngDisplayOrder;
	}
	public void setLngDisplayOrder(long lngDisplayOrder) {
		this.lngDisplayOrder = lngDisplayOrder;
	}
	public int getIntIsSecured() {
		return intIsSecured;
	}
	public void setIntIsSecured(int intIsSecured) {
		this.intIsSecured = intIsSecured;
	}
	public String getStrMenu() {
		return strMenu;
	}
	public void setStrMenu(String strMenu) {
		this.strMenu = strMenu;
	}
	public String getStrActivity() {
		return strActivity;
	}
	public void setStrActivity(String strActivity) {
		this.strActivity = strActivity;
	}
	public String getStrPageURL() {
		return strPageURL;
	}
	public void setStrPageURL(String strPageURL) {
		this.strPageURL = strPageURL;
	}
	
}
