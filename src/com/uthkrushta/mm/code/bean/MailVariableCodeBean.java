
package com.uthkrushta.mm.code.bean;


public class MailVariableCodeBean {

	private long lngID;
	private String strHostName;
	private String strUserName;
	private String strPassword;
	private long lngCompanyID;
	private int intStatus;
	public long getLngID() {
		return lngID;
	}
	public void setLngID(long lngID) {
		this.lngID = lngID;
	}
	public String getStrHostName() {
		return strHostName;
	}
	public void setStrHostName(String strHostName) {
		this.strHostName = strHostName;
	}
	public String getStrUserName() {
		return strUserName;
	}
	public void setStrUserName(String strUserName) {
		this.strUserName = strUserName;
	}
	public String getStrPassword() {
		return strPassword;
	}
	public void setStrPassword(String strPassword) {
		this.strPassword = strPassword;
	}
	public long getLngCompanyID() {
		return lngCompanyID;
	}
	public void setLngCompanyID(long lngCompanyID) {
		this.lngCompanyID = lngCompanyID;
	}
	public int getIntStatus() {
		return intStatus;
	}
	public void setIntStatus(int intStatus) {
		this.intStatus = intStatus;
	}
	
	
}
