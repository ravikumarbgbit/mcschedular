package com.uthkrushta.mm.frameWork.bean;

public class DateRangeBean {

	private String strStartDate;
	private String strEndDate;
	public String getStrStartDate() {
		return strStartDate;
	}
	public void setStrStartDate(String strStartDate) {
		this.strStartDate = strStartDate;
	}
	public String getStrEndDate() {
		return strEndDate;
	}
	public void setStrEndDate(String strEndDate) {
		this.strEndDate = strEndDate;
	}
	
	
}
