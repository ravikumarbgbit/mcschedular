package com.uthkrushta.mm.master.bean;

import java.util.Date;


public class CompanyMasterBean {
	
	private long lngID;
	long lngCompanyID;
	private String strName;
	long lngCompanyTypeID;//1-admin, 2-Branch, 3-Supplier(Seller), 4-Service Provider, 5-manufacturer
	private String strAddress;
	private long lngAreaID;
	private long lngCityID;
	private long lngStateID;
	private long lngCountryID;
	private String strPrimaryPhoneNumber;
	private String strPrimaryMobileNumber;
	private String strSecondaryPhoneNumber;
	private String strSecondaryMobileNumber;
	private String strAdminEmail;
	private String strOutgoingEmail;
	private String strContactEmail;
	private String strWebsite;
	private String strLogoPath;
	
	private long lngTimeZoneID;
	private long lngLanguageID;
	private long lngCurrenyID;
	private int intIsParentCompany;
	private long lngParentCompanyID;
	private long lngAdminID;
	private Date dtEncorporationDate;
	private String strEncorporationNumber;
	private String strTinNumber;
	private String strCstNumber;
	private String strPanNumber;
	private long lngCompanyOwnedTypeID;
	private int intIsActive;
	private String strActivationCode;
	private long lngPlanID;
	private Date dtStartDate;
	private Date dtRenewalDate;
	private int intNotificationCounter;
	private int intIsPayer;
	private String strCompanyCode;
	private String strComments;
	private long lngCreatedBy;
	private Date dtCreatedOn;
	private long lngUpdatedBy;
	private Date dtUpdatedOn;
	private int intStatus;
	private String strSelfComments;
	
	String strContactPersonName;
	String strContactPersonEmail;
	String strContactPersonMobile;
	String strPincode;
	long lngFinancialYearID;
	
	//Added on DEC18
	private String strDesktopPhotoPath;
	
	//Added on JAN13
	private String strSMSAPI;
	
	public long getLngFinancialYearID() {
		return lngFinancialYearID;
	}

	public void setLngFinancialYearID(long lngFinancialYearID) {
		this.lngFinancialYearID = lngFinancialYearID;
	}
	
	
	public String getStrPincode() {
		return strPincode;
	}

	public void setStrPincode(String strPincode) {
		this.strPincode = strPincode;
	}

	public CompanyMasterBean() {
		// TODO Auto-generated constructor stub
     this.strName=this.strAddress=this.strPrimaryPhoneNumber=this.strPrimaryMobileNumber=this.strSecondaryPhoneNumber=this.strSecondaryMobileNumber=
     this.strAdminEmail=this.strOutgoingEmail=this.strContactEmail=this.strWebsite=this.strLogoPath=this.strEncorporationNumber=this.strTinNumber=this.strCstNumber=
     this.strPanNumber=this.strActivationCode=this.strCompanyCode=this.strComments=this.strSelfComments= this.strContactPersonName = this.strContactPersonEmail = this.strContactPersonMobile= this.strPincode= "";
     
    // dtEncorporationDate = dtStartDate = dtRenewalDate = WebUtil.getCurrentDate(request);
	}
	
	public String getStrContactPersonName() {
		return strContactPersonName;
	}
	public void setStrContactPersonName(String strContactPersonName) {
		this.strContactPersonName = strContactPersonName;
	}
	public String getStrContactPersonEmail() {
		return strContactPersonEmail;
	}
	public void setStrContactPersonEmail(String strContactPersonEmail) {
		this.strContactPersonEmail = strContactPersonEmail;
	}
	public String getStrContactPersonMobile() {
		return strContactPersonMobile;
	}
	public void setStrContactPersonMobile(String strContactPersonMobile) {
		this.strContactPersonMobile = strContactPersonMobile;
	}
	
	public long getLngCompanyID() {
		return lngCompanyID;
	}


	public void setLngCompanyID(long lngCompanyID) {
		this.lngCompanyID = lngCompanyID;
	}


	public long getLngID() {
		return lngID;
	}
	public void setLngID(long lngID) {
		this.lngID = lngID;
	}
	public String getStrName() {
		return strName;
	}
	public void setStrName(String strName) {
		this.strName = strName;
	}
	public String getStrAddress() {
		return strAddress;
	}
	public void setStrAddress(String strAddress) {
		this.strAddress = strAddress;
	}
	public long getLngAreaID() {
		return lngAreaID;
	}
	public void setLngAreaID(long lngAreaID) {
		this.lngAreaID = lngAreaID;
	}
	public long getLngCityID() {
		return lngCityID;
	}
	public void setLngCityID(long lngCityID) {
		this.lngCityID = lngCityID;
	}
	public long getLngStateID() {
		return lngStateID;
	}
	public void setLngStateID(long lngStateID) {
		this.lngStateID = lngStateID;
	}
	public long getLngCountryID() {
		return lngCountryID;
	}
	public void setLngCountryID(long lngCountryID) {
		this.lngCountryID = lngCountryID;
	}
	public String getStrPrimaryPhoneNumber() {
		return strPrimaryPhoneNumber;
	}
	public void setStrPrimaryPhoneNumber(String strPrimaryPhoneNumber) {
		this.strPrimaryPhoneNumber = strPrimaryPhoneNumber;
	}
	public String getStrPrimaryMobileNumber() {
		return strPrimaryMobileNumber;
	}
	public void setStrPrimaryMobileNumber(String strPrimaryMobileNumber) {
		this.strPrimaryMobileNumber = strPrimaryMobileNumber;
	}
	public String getStrSecondaryPhoneNumber() {
		return strSecondaryPhoneNumber;
	}
	public void setStrSecondaryPhoneNumber(String strSecondaryPhoneNumber) {
		this.strSecondaryPhoneNumber = strSecondaryPhoneNumber;
	}
	public String getStrSecondaryMobileNumber() {
		return strSecondaryMobileNumber;
	}
	public void setStrSecondaryMobileNumber(String strSecondaryMobileNumber) {
		this.strSecondaryMobileNumber = strSecondaryMobileNumber;
	}
	public String getStrAdminEmail() {
		return strAdminEmail;
	}
	public void setStrAdminEmail(String strAdminEmail) {
		this.strAdminEmail = strAdminEmail;
	}
	public String getStrOutgoingEmail() {
		return strOutgoingEmail;
	}
	public void setStrOutgoingEmail(String strOutgoingEmail) {
		this.strOutgoingEmail = strOutgoingEmail;
	}
	public String getStrContactEmail() {
		return strContactEmail;
	}
	public void setStrContactEmail(String strContactEmail) {
		this.strContactEmail = strContactEmail;
	}
	public String getStrWebsite() {
		return strWebsite;
	}
	public void setStrWebsite(String strWebsite) {
		this.strWebsite = strWebsite;
	}
	public String getStrLogoPath() {
		return strLogoPath;
	}
	public void setStrLogoPath(String strLogoPath) {
		this.strLogoPath = strLogoPath;
	}
	public long getLngTimeZoneID() {
		return lngTimeZoneID;
	}
	public void setLngTimeZoneID(long lngTimeZoneID) {
		this.lngTimeZoneID = lngTimeZoneID;
	}
	public long getLngLanguageID() {
		return lngLanguageID;
	}
	public void setLngLanguageID(long lngLanguageID) {
		this.lngLanguageID = lngLanguageID;
	}
	public long getLngCurrenyID() {
		return lngCurrenyID;
	}
	public void setLngCurrenyID(long lngCurrenyID) {
		this.lngCurrenyID = lngCurrenyID;
	}
	public int getIntIsParentCompany() {
		return intIsParentCompany;
	}
	public void setIntIsParentCompany(int intIsParentCompany) {
		this.intIsParentCompany = intIsParentCompany;
	}
	public long getLngParentCompanyID() {
		return lngParentCompanyID;
	}
	public void setLngParentCompanyID(long lngParentCompanyID) {
		this.lngParentCompanyID = lngParentCompanyID;
	}
	public long getLngAdminID() {
		return lngAdminID;
	}
	public void setLngAdminID(long lngAdminID) {
		this.lngAdminID = lngAdminID;
	}
	public Date getDtEncorporationDate() {
		return dtEncorporationDate;
	}
	public void setDtEncorporationDate(Date dtEncorporationDate) {
		this.dtEncorporationDate = dtEncorporationDate;
	}
	public String getStrEncorporationNumber() {
		return strEncorporationNumber;
	}
	public void setStrEncorporationNumber(String strEncorporationNumber) {
		this.strEncorporationNumber = strEncorporationNumber;
	}
	public String getStrTinNumber() {
		return strTinNumber;
	}
	public void setStrTinNumber(String strTinNumber) {
		this.strTinNumber = strTinNumber;
	}
	public String getStrCstNumber() {
		return strCstNumber;
	}
	public void setStrCstNumber(String strCstNumber) {
		this.strCstNumber = strCstNumber;
	}
	public String getStrPanNumber() {
		return strPanNumber;
	}
	public void setStrPanNumber(String strPanNumber) {
		this.strPanNumber = strPanNumber;
	}
	public long getLngCompanyTypeID() {
		return lngCompanyTypeID;
	}

	public void setLngCompanyTypeID(long lngCompanyTypeID) {
		this.lngCompanyTypeID = lngCompanyTypeID;
	}

	public long getLngCompanyOwnedTypeID() {
		return lngCompanyOwnedTypeID;
	}

	public void setLngCompanyOwnedTypeID(long lngCompanyOwnedTypeID) {
		this.lngCompanyOwnedTypeID = lngCompanyOwnedTypeID;
	}

	public int getIntIsActive() {
		return intIsActive;
	}
	public void setIntIsActive(int intIsActive) {
		this.intIsActive = intIsActive;
	}
	public String getStrActivationCode() {
		return strActivationCode;
	}
	public void setStrActivationCode(String strActivationCode) {
		this.strActivationCode = strActivationCode;
	}
	public long getLngPlanID() {
		return lngPlanID;
	}
	public void setLngPlanID(long lngPlanID) {
		this.lngPlanID = lngPlanID;
	}
	public Date getDtStartDate() {
		return dtStartDate;
	}
	public void setDtStartDate(Date dtStartDate) {
		this.dtStartDate = dtStartDate;
	}
	public Date getDtRenewalDate() {
		return dtRenewalDate;
	}
	public void setDtRenewalDate(Date dtRenewalDate) {
		this.dtRenewalDate = dtRenewalDate;
	}
	public int getIntNotificationCounter() {
		return intNotificationCounter;
	}
	public void setIntNotificationCounter(int intNotificationCounter) {
		this.intNotificationCounter = intNotificationCounter;
	}
	public int getIntIsPayer() {
		return intIsPayer;
	}
	public void setIntIsPayer(int intIsPayer) {
		this.intIsPayer = intIsPayer;
	}
	public String getStrCompanyCode() {
		return strCompanyCode;
	}
	public void setStrCompanyCode(String strCompanyCode) {
		this.strCompanyCode = strCompanyCode;
	}
	public String getStrComments() {
		return strComments;
	}
	public void setStrComments(String strComments) {
		this.strComments = strComments;
	}
	public long getLngCreatedBy() {
		return lngCreatedBy;
	}
	public void setLngCreatedBy(long lngCreatedBy) {
		this.lngCreatedBy = lngCreatedBy;
	}
	public Date getDtCreatedOn() {
		return dtCreatedOn;
	}
	public void setDtCreatedOn(Date dtCreatedOn) {
		this.dtCreatedOn = dtCreatedOn;
	}
	public long getLngUpdatedBy() {
		return lngUpdatedBy;
	}
	public void setLngUpdatedBy(long lngUpdatedBy) {
		this.lngUpdatedBy = lngUpdatedBy;
	}
	public Date getDtUpdatedOn() {
		return dtUpdatedOn;
	}
	public void setDtUpdatedOn(Date dtUpdatedOn) {
		this.dtUpdatedOn = dtUpdatedOn;
	}
	public int getIntStatus() {
		return intStatus;
	}
	public void setIntStatus(int intStatus) {
		this.intStatus = intStatus;
	}
	public String getStrSelfComments() {
		return strSelfComments;
	}
	public void setStrSelfComments(String strSelfComments) {
		this.strSelfComments = strSelfComments;
	}

	public String getStrDesktopPhotoPath() {
		return strDesktopPhotoPath;
	}

	public void setStrDesktopPhotoPath(String strDesktopPhotoPath) {
		this.strDesktopPhotoPath = strDesktopPhotoPath;
	}

	public String getStrSMSAPI() {
		return strSMSAPI;
	}

	public void setStrSMSAPI(String strSMSAPI) {
		this.strSMSAPI = strSMSAPI;
	}
	
	
	
}
