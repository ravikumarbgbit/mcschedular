package com.uthkrushta.mm.master.bean;

import java.util.Date;

public class FinancialYearMasterBean {
	long lngID;
	Date dtStartDate;
	Date dtEndDate;
	String strFinancialYear;
	int intIsActive;
	int intStatus;
	
	
	public FinancialYearMasterBean() {
		// TODO Auto-generated constructor stub
	   this.strFinancialYear="";
	}
	
	public long getLngID() {
		return lngID;
	}
	public void setLngID(long lngID) {
		this.lngID = lngID;
	}
	public Date getDtStartDate() {
		return dtStartDate;
	}
	public void setDtStartDate(Date dtStartDate) {
		this.dtStartDate = dtStartDate;
	}
	public Date getDtEndDate() {
		return dtEndDate;
	}
	public void setDtEndDate(Date dtEndDate) {
		this.dtEndDate = dtEndDate;
	}
	public String getStrFinancialYear() {
		return strFinancialYear;
	}
	public void setStrFinancialYear(String strFinancialYear) {
		this.strFinancialYear = strFinancialYear;
	}
	public int getIntIsActive() {
		return intIsActive;
	}
	public void setIntIsActive(int intIsActive) {
		this.intIsActive = intIsActive;
	}
	public int getIntStatus() {
		return intStatus;
	}
	public void setIntStatus(int intStatus) {
		this.intStatus = intStatus;
	}
	

}
