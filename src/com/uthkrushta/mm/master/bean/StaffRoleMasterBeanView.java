package com.uthkrushta.mm.master.bean;

import java.util.Date;

public class StaffRoleMasterBeanView {
	long lngID;
	 long lngNamePrefixID;
	 String strName;
	 String strLastName;
	 long lngCompanyID;
	 long lngRoleID;
	 long lngPrimaryBatchID;
	 String strEmail;
	 Date dtDOB;
	 long lngGenderID;
	 long lngBloodGroupID;
	 String strMobileNumber;
	 String strLandlineNumber;
	 String strAddress;
	 long lngAreaID;
	 long lngCityID;
	 long lngStateID;
	 long lngCountryID;
	 String strICEName;
	 long lngICERelationshipID;
	 String strICEMobileNumber;
	 Date dtDOJ;
	 double dblSalary;
	 String strPhotoPath;
	 String strUserName;
	 String strPassword;
	 long lngSecurityQuestionID;
	 String strSecurityAnswer;
	 long lngStaffID;
	 String strStaffCode;
	 long lngStaffStatus;
	 Date dtRelievedDate;
	 String strRelievedComments;
	 long lngCreatedBy;
	 Date dtCreatedOn;
	 long lngUpdatedBy;
	 Date dtUpdatedOn;
	 int intStatus;
	 String strPincode;
	 
	 String strRoleName;
	 long lngRoleTypeID;
	 String strNamePrefix;
	 
	 public String getStrNamePrefix() {
		return strNamePrefix;
	}

	public void setStrNamePrefix(String strNamePrefix) {
		this.strNamePrefix = strNamePrefix;
	}

	public StaffRoleMasterBeanView() {
			super();
			strName = strLastName =  strEmail = strMobileNumber = strLandlineNumber = strPincode = strAddress = strICEName = strICEMobileNumber = strPhotoPath = strUserName = strPassword = strSecurityAnswer = strStaffCode  = strRelievedComments = "";
			
		}
	 
	 public String getStrRoleName() {
		return strRoleName;
	}

	public void setStrRoleName(String strRoleName) {
		this.strRoleName = strRoleName;
	}

	public long getLngRoleTypeID() {
		return lngRoleTypeID;
	}

	public void setLngRoleTypeID(long lngRoleTypeID) {
		this.lngRoleTypeID = lngRoleTypeID;
	}
	Date dtDOM;
		
		public Date getDtDOM() {
			return dtDOM;
		}

		public void setDtDOM(Date dtDOM) {
			this.dtDOM = dtDOM;
		}

		public String getStrPincode() {
			return strPincode;
		}

		public void setStrPincode(String strPincode) {
			this.strPincode = strPincode;
		}
	 
	 
	 
	



	public String getStrName() {
		return strName;
	}

	public void setStrName(String strName) {
		this.strName = strName;
	}

	public String getStrLastName() {
		return strLastName;
	}

	public void setStrLastName(String strLastName) {
		this.strLastName = strLastName;
	}

	public long getLngNamePrefixID() {
		return lngNamePrefixID;
	}
	public void setLngNamePrefixID(long lngNamePrefixID) {
		this.lngNamePrefixID = lngNamePrefixID;
	}
	public long getLngID() {
		return lngID;
	}
	public void setLngID(long lngID) {
		this.lngID = lngID;
	}
	public long getLngCompanyID() {
		return lngCompanyID;
	}
	public void setLngCompanyID(long lngCompanyID) {
		this.lngCompanyID = lngCompanyID;
	}
	public long getLngRoleID() {
		return lngRoleID;
	}
	public void setLngRoleID(long lngRoleID) {
		this.lngRoleID = lngRoleID;
	}
	public long getLngPrimaryBatchID() {
		return lngPrimaryBatchID;
	}
	public void setLngPrimaryBatchID(long lngPrimaryBatchID) {
		this.lngPrimaryBatchID = lngPrimaryBatchID;
	}
	public String getStrEmail() {
		return strEmail;
	}
	public void setStrEmail(String strEmail) {
		this.strEmail = strEmail;
	}
	public Date getDtDOB() {
		return dtDOB;
	}
	public void setDtDOB(Date dtDOB) {
		this.dtDOB = dtDOB;
	}
	public long getLngGenderID() {
		return lngGenderID;
	}
	public void setLngGenderID(long lngGenderID) {
		this.lngGenderID = lngGenderID;
	}
	public long getLngBloodGroupID() {
		return lngBloodGroupID;
	}
	public void setLngBloodGroupID(long lngBloodGroupID) {
		this.lngBloodGroupID = lngBloodGroupID;
	}
	public String getStrMobileNumber() {
		return strMobileNumber;
	}
	public void setStrMobileNumber(String strMobileNumber) {
		this.strMobileNumber = strMobileNumber;
	}
	public String getStrLandlineNumber() {
		return strLandlineNumber;
	}
	public void setStrLandlineNumber(String strLandlineNumber) {
		this.strLandlineNumber = strLandlineNumber;
	}
	public String getStrAddress() {
		return strAddress;
	}
	public void setStrAddress(String strAddress) {
		this.strAddress = strAddress;
	}
	public long getLngAreaID() {
		return lngAreaID;
	}
	public void setLngAreaID(long lngAreaID) {
		this.lngAreaID = lngAreaID;
	}
	public long getLngCityID() {
		return lngCityID;
	}
	public void setLngCityID(long lngCityID) {
		this.lngCityID = lngCityID;
	}
	public long getLngStateID() {
		return lngStateID;
	}
	public void setLngStateID(long lngStateID) {
		this.lngStateID = lngStateID;
	}
	public long getLngCountryID() {
		return lngCountryID;
	}
	public void setLngCountryID(long lngCountryID) {
		this.lngCountryID = lngCountryID;
	}
	public String getStrICEName() {
		return strICEName;
	}
	public void setStrICEName(String strICEName) {
		this.strICEName = strICEName;
	}
	public long getLngICERelationshipID() {
		return lngICERelationshipID;
	}
	public void setLngICERelationshipID(long lngICERelationshipID) {
		this.lngICERelationshipID = lngICERelationshipID;
	}
	public String getStrICEMobileNumber() {
		return strICEMobileNumber;
	}
	public void setStrICEMobileNumber(String strICEMobileNumber) {
		this.strICEMobileNumber = strICEMobileNumber;
	}
	public Date getDtDOJ() {
		return dtDOJ;
	}
	public void setDtDOJ(Date dtDOJ) {
		this.dtDOJ = dtDOJ;
	}
	public double getDblSalary() {
		return dblSalary;
	}
	public void setDblSalary(double dblSalary) {
		this.dblSalary = dblSalary;
	}
	public String getStrPhotoPath() {
		return strPhotoPath;
	}
	public void setStrPhotoPath(String strPhotoPath) {
		this.strPhotoPath = strPhotoPath;
	}
	public String getStrUserName() {
		return strUserName;
	}
	public void setStrUserName(String strUserName) {
		this.strUserName = strUserName;
	}
	public String getStrPassword() {
		return strPassword;
	}
	public void setStrPassword(String strPassword) {
		this.strPassword = strPassword;
	}
	public long getLngSecurityQuestionID() {
		return lngSecurityQuestionID;
	}
	public void setLngSecurityQuestionID(long lngSecurityQuestionID) {
		this.lngSecurityQuestionID = lngSecurityQuestionID;
	}
	public String getStrSecurityAnswer() {
		return strSecurityAnswer;
	}
	public void setStrSecurityAnswer(String strSecurityAnswer) {
		this.strSecurityAnswer = strSecurityAnswer;
	}
	public long getLngStaffID() {
		return lngStaffID;
	}
	public void setLngStaffID(long lngStaffID) {
		this.lngStaffID = lngStaffID;
	}
	public String getStrStaffCode() {
		return strStaffCode;
	}
	public void setStrStaffCode(String strStaffCode) {
		this.strStaffCode = strStaffCode;
	}
	public long getLngStaffStatus() {
		return lngStaffStatus;
	}
	public void setLngStaffStatus(long lngStaffStatus) {
		this.lngStaffStatus = lngStaffStatus;
	}

	public Date getDtRelievedDate() {
		return dtRelievedDate;
	}
	public void setDtRelievedDate(Date dtRelievedDate) {
		this.dtRelievedDate = dtRelievedDate;
	}
	public String getStrRelievedComments() {
		return strRelievedComments;
	}
	public void setStrRelievedComments(String strRelievedComments) {
		this.strRelievedComments = strRelievedComments;
	}
	public long getLngCreatedBy() {
		return lngCreatedBy;
	}
	public void setLngCreatedBy(long lngCreatedBy) {
		this.lngCreatedBy = lngCreatedBy;
	}
	public Date getDtCreatedOn() {
		return dtCreatedOn;
	}
	public void setDtCreatedOn(Date dtCreatedOn) {
		this.dtCreatedOn = dtCreatedOn;
	}
	public long getLngUpdatedBy() {
		return lngUpdatedBy;
	}
	public void setLngUpdatedBy(long lngUpdatedBy) {
		this.lngUpdatedBy = lngUpdatedBy;
	}
	public Date getDtUpdatedOn() {
		return dtUpdatedOn;
	}
	public void setDtUpdatedOn(Date dtUpdatedOn) {
		this.dtUpdatedOn = dtUpdatedOn;
	}
	public int getIntStatus() {
		return intStatus;
	}
	public void setIntStatus(int intStatus) {
		this.intStatus = intStatus;
	} 

	 
	 


}
