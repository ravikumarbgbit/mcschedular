package com.uthkrushta.mm.supreme.control;

public class TriggerSuperConfigurationBean {

	private long lngID;
	private String strName;
	private long lngTriggerControl;
	private int intStatus;
	public long getLngID() {
		return lngID;
	}
	public void setLngID(long lngID) {
		this.lngID = lngID;
	}
	public String getStrName() {
		return strName;
	}
	public void setStrName(String strName) {
		this.strName = strName;
	}
	public long getLngTriggerControl() {
		return lngTriggerControl;
	}
	public void setLngTriggerControl(long lngTriggerControl) {
		this.lngTriggerControl = lngTriggerControl;
	}
	public int getIntStatus() {
		return intStatus;
	}
	public void setIntStatus(int intStatus) {
		this.intStatus = intStatus;
	}
	
	
	
	
	
}
