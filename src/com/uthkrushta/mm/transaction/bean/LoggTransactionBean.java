package com.uthkrushta.mm.transaction.bean;

import java.util.Date;

public class LoggTransactionBean {

	private long lngID;
	private long lngLoggReasonID;
	private long lngMemID;
	private long lngRedemptionType;//Adding of points or redemption of points;
	private int  intStatus;
	private double dblPoints;
	private long lngCreatedBy;
	private long lngUpdatedBy;
	private Date dtCreatedOn;
	private Date dtUpdatedOn;
	private long lngCompanyID;
	private long lngFinancialYearID;
	private long lngFormsRefID;
	
	public long getLngID() {
		return lngID;
	}
	public void setLngID(long lngID) {
		this.lngID = lngID;
	}
	public long getLngLoggReasonID() {
		return lngLoggReasonID;
	}
	public void setLngLoggReasonID(long lngLoggReasonID) {
		this.lngLoggReasonID = lngLoggReasonID;
	}
	public long getLngMemID() {
		return lngMemID;
	}
	public void setLngMemID(long lngMemID) {
		this.lngMemID = lngMemID;
	}
	public long getLngRedemptionType() {
		return lngRedemptionType;
	}
	public void setLngRedemptionType(long lngRedemptionType) {
		this.lngRedemptionType = lngRedemptionType;
	}
	public int getIntStatus() {
		return intStatus;
	}
	public void setIntStatus(int intStatus) {
		this.intStatus = intStatus;
	}
	public double getDblPoints() {
		return dblPoints;
	}
	public void setDblPoints(double dblPoints) {
		this.dblPoints = dblPoints;
	}
	public long getLngCreatedBy() {
		return lngCreatedBy;
	}
	public void setLngCreatedBy(long lngCreatedBy) {
		this.lngCreatedBy = lngCreatedBy;
	}
	public long getLngUpdatedBy() {
		return lngUpdatedBy;
	}
	public void setLngUpdatedBy(long lngUpdatedBy) {
		this.lngUpdatedBy = lngUpdatedBy;
	}
	public Date getDtCreatedOn() {
		return dtCreatedOn;
	}
	public void setDtCreatedOn(Date dtCreatedOn) {
		this.dtCreatedOn = dtCreatedOn;
	}
	public Date getDtUpdatedOn() {
		return dtUpdatedOn;
	}
	public void setDtUpdatedOn(Date dtUpdatedOn) {
		this.dtUpdatedOn = dtUpdatedOn;
	}
	public long getLngCompanyID() {
		return lngCompanyID;
	}
	public void setLngCompanyID(long lngCompanyID) {
		this.lngCompanyID = lngCompanyID;
	}
	public long getLngFinancialYearID() {
		return lngFinancialYearID;
	}
	public void setLngFinancialYearID(long lngFinancialYearID) {
		this.lngFinancialYearID = lngFinancialYearID;
	}
	public long getLngFormsRefID() {
		return lngFormsRefID;
	}
	public void setLngFormsRefID(long lngFormsRefID) {
		this.lngFormsRefID = lngFormsRefID;
	}
	
	
	
	
}
