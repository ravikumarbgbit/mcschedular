package com.uthkrushta.mm.transaction.bean;

import java.util.Date;

public class MemberPauseTransactionBean {
	long lngID;
	int intIsPaused;
	long lngMemeberID;
	long lngPauseTypeID;
	Date dtMemberShipExtension;
	String strReason;
	double dblFees;
	Date dtFrom;
	Date dtTo;
	int intLatestRecord;
	int intStatus;
	long lngCreatedBy;	
	Date dtCreatedOn;
	long lngUpdatedBy;
	Date dtUpdatedOn;
	
	long lngPlanSubID;
	long lngCompanyID;
	
	
	
	public long getLngCompanyID() {
		return lngCompanyID;
	}
	public void setLngCompanyID(long lngCompanyID) {
		this.lngCompanyID = lngCompanyID;
	}
	public long getLngPlanSubID() {
		return lngPlanSubID;
	}
	public void setLngPlanSubID(long lngPlanSubID) {
		this.lngPlanSubID = lngPlanSubID;
	}
	public MemberPauseTransactionBean() {
		super();
		strReason= "";
	}
	public int getIntIsPaused() {
		return intIsPaused;
	}
	public void setIntIsPaused(int intIsPaused) {
		this.intIsPaused = intIsPaused;
	}
	public int getIntLatestRecord() {
		return intLatestRecord;
	}
	public void setIntLatestRecord(int intLatestRecord) {
		this.intLatestRecord = intLatestRecord;
	}
	public int getIntStatus() {
		return intStatus;
	}
	public void setIntStatus(int intStatus) {
		this.intStatus = intStatus;
	}
	public long getLngCreatedBy() {
		return lngCreatedBy;
	}
	public void setLngCreatedBy(long lngCreatedBy) {
		this.lngCreatedBy = lngCreatedBy;
	}
	public Date getDtCreatedOn() {
		return dtCreatedOn;
	}
	public void setDtCreatedOn(Date dtCreatedOn) {
		this.dtCreatedOn = dtCreatedOn;
	}
	public long getLngUpdatedBy() {
		return lngUpdatedBy;
	}
	public void setLngUpdatedBy(long lngUpdatedBy) {
		this.lngUpdatedBy = lngUpdatedBy;
	}
	public Date getDtUpdatedOn() {
		return dtUpdatedOn;
	}
	public void setDtUpdatedOn(Date dtUpdatedOn) {
		this.dtUpdatedOn = dtUpdatedOn;
	}
	public long getLngID() {
		return lngID;
	}
	public void setLngID(long lngID) {
		this.lngID = lngID;
	}
	public long getLngMemeberID() {
		return lngMemeberID;
	}
	public void setLngMemeberID(long lngMemeberID) {
		this.lngMemeberID = lngMemeberID;
	}
	public long getLngPauseTypeID() {
		return lngPauseTypeID;
	}
	public void setLngPauseTypeID(long lngPauseTypeID) {
		this.lngPauseTypeID = lngPauseTypeID;
	}
	
	public Date getDtMemberShipExtension() {
		return dtMemberShipExtension;
	}
	public void setDtMemberShipExtension(Date dtMemberShipExtension) {
		this.dtMemberShipExtension = dtMemberShipExtension;
	}
	public String getStrReason() {
		return strReason;
	}
	public void setStrReason(String strReason) {
		this.strReason = strReason;
	}
	public double getDblFees() {
		return dblFees;
	}
	public void setDblFees(double dblFees) {
		this.dblFees = dblFees;
	}
	public Date getDtFrom() {
		return dtFrom;
	}
	public void setDtFrom(Date dtFrom) {
		this.dtFrom = dtFrom;
	}
	public Date getDtTo() {
		return dtTo;
	}
	public void setDtTo(Date dtTo) {
		this.dtTo = dtTo;
	}
}
