package com.uthkrushta.mm.transaction.bean;

import java.util.Date;

public class MemberPaymentTransactionBean {

	private long lngID;
	private long lngMemberID;
	private long lngUserID;
	private Date dtReceiptDate;
	private long lngReceiptNumber;
	private double dbPaymentAmount;
	private double dbDiscountPercent;
	private double dbDiscountAmount;
	private double dbPayableAmount;
	private double dbPayingAmount;
	private long lngPaymentMethodID;
	private String strPaymentMemo;
	private long lngPlanID;
	private long lngSubscriptionID;
	private long lngPaymentTypeID;
	private long lngTaxID;
	private double dbTaxRate;
	private double dbTaxAmount; 
	private double dbAmountAfterDiscount;
	private long lngFinancialYearID;
	private long lngCompanyID;
	private int intStatus;
	private Date dtCreatedOn;
	private long lngCreatedBy;
	//extra fields
	private long lngReasonForDiscountID;
	private int intPageType;
	
	//Extra
		private int intIsRoundOff;
		private double dbRoundOffAmt;
	
   //Added on 1/9/2015
		
		private long lngPlanSubsID;
		
  //Added on 7/9/2015
		private double dblBalance;
		private int intIsLatest;
		 /*private int intIsLatestReceipt;*/
		private double dblActualPayable;
		private double dblNetAmount;

 //Added on 8/9/2015		
		private Date dtPlanStartDate;
		private Date dtPlanEndDate;
		
	//Added on 22/12/2015
		private String strPdfFilePath;
        
		private double dblCashPayment;
		private double dblCardPayment;
		private double dblChequePayment;
		

		public String getStrPdfFilePath() {
			return strPdfFilePath;
		}

		public void setStrPdfFilePath(String strPdfFilePath) {
			this.strPdfFilePath = strPdfFilePath;
		}

		public Date getDtPlanStartDate() {
			return dtPlanStartDate;
		}

		public void setDtPlanStartDate(Date dtPlanStartDate) {
			this.dtPlanStartDate = dtPlanStartDate;
		}

		public Date getDtPlanEndDate() {
			return dtPlanEndDate;
		}

		public void setDtPlanEndDate(Date dtPlanEndDate) {
			this.dtPlanEndDate = dtPlanEndDate;
		}

		public double getDblNetAmount() {
			return dblNetAmount;
		}

		public void setDblNetAmount(double dblNetAmount) {
			this.dblNetAmount = dblNetAmount;
		}

		public double getDbRoundOffAmt() {
			return dbRoundOffAmt;
		}

		public void setDbRoundOffAmt(double dbRoundOffAmt) {
			this.dbRoundOffAmt = dbRoundOffAmt;
		}

		public int getIntIsRoundOff() {
			return intIsRoundOff;
		}

		public void setIntIsRoundOff(int intIsRoundOff) {
			this.intIsRoundOff = intIsRoundOff;
		}

		public MemberPaymentTransactionBean() {
			// TODO Auto-generated constructor stub
			this.strPaymentMemo="";
			this.intIsRoundOff=1;
		}
	
	public long getLngID() {
		return lngID;
	}
	public void setLngID(long lngID) {
		this.lngID = lngID;
	}
	public long getLngMemberID() {
		return lngMemberID;
	}
	public void setLngMemberID(long lngMemberID) {
		this.lngMemberID = lngMemberID;
	}
	public long getLngUserID() {
		return lngUserID;
	}
	public void setLngUserID(long lngUserID) {
		this.lngUserID = lngUserID;
	}
	public Date getDtReceiptDate() {
		return dtReceiptDate;
	}
	public void setDtReceiptDate(Date dtReceiptDate) {
		this.dtReceiptDate = dtReceiptDate;
	}
	public long getLngReceiptNumber() {
		return lngReceiptNumber;
	}
	public void setLngReceiptNumber(long lngReceiptNumber) {
		this.lngReceiptNumber = lngReceiptNumber;
	}
	public double getDbPaymentAmount() {
		return dbPaymentAmount;
	}
	public void setDbPaymentAmount(double dbPaymentAmount) {
		this.dbPaymentAmount = dbPaymentAmount;
	}
	public double getDbDiscountPercent() {
		return dbDiscountPercent;
	}
	public void setDbDiscountPercent(double dbDiscountPercent) {
		this.dbDiscountPercent = dbDiscountPercent;
	}
	public double getDbDiscountAmount() {
		return dbDiscountAmount;
	}
	public void setDbDiscountAmount(double dbDiscountAmount) {
		this.dbDiscountAmount = dbDiscountAmount;
	}
	public double getDbPayableAmount() {
		return dbPayableAmount;
	}
	public void setDbPayableAmount(double dbPayableAmount) {
		this.dbPayableAmount = dbPayableAmount;
	}
	public double getDbPayingAmount() {
		return dbPayingAmount;
	}
	public void setDbPayingAmount(double dbPayingAmount) {
		this.dbPayingAmount = dbPayingAmount;
	}
	public long getLngPaymentMethodID() {
		return lngPaymentMethodID;
	}
	public void setLngPaymentMethodID(long lngPaymentMethodID) {
		this.lngPaymentMethodID = lngPaymentMethodID;
	}
	public String getStrPaymentMemo() {
		return strPaymentMemo;
	}
	public void setStrPaymentMemo(String strPaymentMemo) {
		this.strPaymentMemo = strPaymentMemo;
	}
	public long getLngPlanID() {
		return lngPlanID;
	}
	public void setLngPlanID(long lngPlanID) {
		this.lngPlanID = lngPlanID;
	}
	public long getLngSubscriptionID() {
		return lngSubscriptionID;
	}
	public void setLngSubscriptionID(long lngSubscriptionID) {
		this.lngSubscriptionID = lngSubscriptionID;
	}
	public long getLngPaymentTypeID() {
		return lngPaymentTypeID;
	}
	public void setLngPaymentTypeID(long lngPaymentTypeID) {
		this.lngPaymentTypeID = lngPaymentTypeID;
	}
	public long getLngTaxID() {
		return lngTaxID;
	}
	public void setLngTaxID(long lngTaxID) {
		this.lngTaxID = lngTaxID;
	}
	public double getDbTaxRate() {
		return dbTaxRate;
	}
	public void setDbTaxRate(double dbTaxRate) {
		this.dbTaxRate = dbTaxRate;
	}
	public double getDbTaxAmount() {
		return dbTaxAmount;
	}
	public void setDbTaxAmount(double dbTaxAmount) {
		this.dbTaxAmount = dbTaxAmount;
	}
	public double getDbAmountAfterDiscount() {
		return dbAmountAfterDiscount;
	}
	public void setDbAmountAfterDiscount(double dbAmountAfterDiscount) {
		this.dbAmountAfterDiscount = dbAmountAfterDiscount;
	}
	public long getLngFinancialYearID() {
		return lngFinancialYearID;
	}
	public void setLngFinancialYearID(long lngFinancialYearID) {
		this.lngFinancialYearID = lngFinancialYearID;
	}
	public long getLngCompanyID() {
		return lngCompanyID;
	}
	public void setLngCompanyID(long lngCompanyID) {
		this.lngCompanyID = lngCompanyID;
	}
	public int getIntStatus() {
		return intStatus;
	}
	public void setIntStatus(int intStatus) {
		this.intStatus = intStatus;
	}
	public Date getDtCreatedOn() {
		return dtCreatedOn;
	}
	public void setDtCreatedOn(Date dtCreatedOn) {
		this.dtCreatedOn = dtCreatedOn;
	}
	public long getLngCreatedBy() {
		return lngCreatedBy;
	}
	public void setLngCreatedBy(long lngCreatedBy) {
		this.lngCreatedBy = lngCreatedBy;
	}

	public long getLngReasonForDiscountID() {
		return lngReasonForDiscountID;
	}

	public void setLngReasonForDiscountID(long lngReasonForDiscountID) {
		this.lngReasonForDiscountID = lngReasonForDiscountID;
	}

	public int getIntPageType() {
		return intPageType;
	}

	public void setIntPageType(int intPageType) {
		this.intPageType = intPageType;
	}

	public long getLngPlanSubsID() {
		return lngPlanSubsID;
	}

	public void setLngPlanSubsID(long lngPlanSubsID) {
		this.lngPlanSubsID = lngPlanSubsID;
	}

	public double getDblBalance() {
		return dblBalance;
	}

	public void setDblBalance(double dblBalance) {
		this.dblBalance = dblBalance;
	}

	public int getIntIsLatest() {
		return intIsLatest;
	}

	public void setIntIsLatest(int intIs_Latest) {
		this.intIsLatest = intIs_Latest;
	}

	public double getDblActualPayable() {
		return dblActualPayable;
	}

	public void setDblActualPayable(double dblActualPayable) {
		this.dblActualPayable = dblActualPayable;
	}

	public double getDblCashPayment() {
		return dblCashPayment;
	}

	public void setDblCashPayment(double dblCashPayment) {
		this.dblCashPayment = dblCashPayment;
	}

	public double getDblCardPayment() {
		return dblCardPayment;
	}

	public void setDblCardPayment(double dblCardPayment) {
		this.dblCardPayment = dblCardPayment;
	}

	public double getDblChequePayment() {
		return dblChequePayment;
	}

	public void setDblChequePayment(double dblChequePayment) {
		this.dblChequePayment = dblChequePayment;
	}

	/*public int getIntIsLatestReceipt() {
		return intIsLatestReceipt;
	}

	public void setIntIsLatestReceipt(int intIsLatestReceipt) {
		this.intIsLatestReceipt = intIsLatestReceipt;
	}*/
	
	
	
	
	
	
	
	
}
