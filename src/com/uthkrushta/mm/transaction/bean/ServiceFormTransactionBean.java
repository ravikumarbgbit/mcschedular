package com.uthkrushta.mm.transaction.bean;

import java.util.Date;

public class ServiceFormTransactionBean {

	private long lngID;
	private long lngMemID;
	private long lngStaffID;
	private long lngServiceTypeID;
	private Date dtSessionFromTime;
	private Date dtSessionToTime;
	private Date dtSessionFromDuration;
	private Date dtSessionToDuration;
	private int intDayType;//Whether weekly or daily
	private String strDays;//M,T,W
	private long lngSessions;
	private int intStatus;
	private long lngCompanyID;
	private long lngFinancialYearID;
	private long lngCreatedBy;
	private Date dtCreatedOn;
	private long lngUpdatedBy;
	private Date dtUpdatedOn;
	private int intSun;
	private int intMon;
	private int intTue;
	private int intWed;
	private int intThur;
	private int intFri;
	private int intSat;
	
	/////////////////////////
	/*private int intMusc;*/
	private int intBMI;
	
	private int intQuestionaire;
	private int intFitnessTest;
	private int intMeasurements;
	private int intCounseling;
	private int intWorkoutCard;
	private String strNote;
	
	/*private int intAscost;
	private int intFat;
	private int intCol;*/
	private int intDone;
	private long lngReceiptID;
	
	//Added for managing sessions
	private long lngAttendedSessions;
	private long lngRemaningSessions;
	
	//added extra
	private Date dtBookingDate;
	
	private double dblCostPerSession;
	private double dblPayoutPercent;
	
	 private int intSMSCount;
	  private Date dtlastSMSDate;
	
	public ServiceFormTransactionBean() {
		// TODO Auto-generated constructor stub
	  this.strNote="";
	}
	
	public long getLngID() {
		return lngID;
	}
	public void setLngID(long lngID) {
		this.lngID = lngID;
	}
	public long getLngMemID() {
		return lngMemID;
	}
	public void setLngMemID(long lngMemID) {
		this.lngMemID = lngMemID;
	}
	public long getLngStaffID() {
		return lngStaffID;
	}
	public void setLngStaffID(long lngStaffID) {
		this.lngStaffID = lngStaffID;
	}
	public long getLngServiceTypeID() {
		return lngServiceTypeID;
	}
	public void setLngServiceTypeID(long lngServiceTypeID) {
		this.lngServiceTypeID = lngServiceTypeID;
	}
	public Date getDtSessionFromTime() {
		return dtSessionFromTime;
	}
	public void setDtSessionFromTime(Date dtSessionFromTime) {
		this.dtSessionFromTime = dtSessionFromTime;
	}
	public Date getDtSessionToTime() {
		return dtSessionToTime;
	}
	public void setDtSessionToTime(Date dtSessionToTime) {
		this.dtSessionToTime = dtSessionToTime;
	}
	public Date getDtSessionFromDuration() {
		return dtSessionFromDuration;
	}
	public void setDtSessionFromDuration(Date dtSessionFromDuration) {
		this.dtSessionFromDuration = dtSessionFromDuration;
	}
	public Date getDtSessionToDuration() {
		return dtSessionToDuration;
	}
	public void setDtSessionToDuration(Date dtSessionToDuration) {
		this.dtSessionToDuration = dtSessionToDuration;
	}
	public int getIntDayType() {
		return intDayType;
	}
	public void setIntDayType(int intDayType) {
		this.intDayType = intDayType;
	}
	public String getStrDays() {
		return strDays;
	}
	public void setStrDays(String strDays) {
		this.strDays = strDays;
	}
	public int getIntStatus() {
		return intStatus;
	}
	public void setIntStatus(int intStatus) {
		this.intStatus = intStatus;
	}
	public long getLngCompanyID() {
		return lngCompanyID;
	}
	public void setLngCompanyID(long lngCompanyID) {
		this.lngCompanyID = lngCompanyID;
	}
	public long getLngFinancialYearID() {
		return lngFinancialYearID;
	}
	public void setLngFinancialYearID(long lngFinancialYearID) {
		this.lngFinancialYearID = lngFinancialYearID;
	}
	public long getLngCreatedBy() {
		return lngCreatedBy;
	}
	public void setLngCreatedBy(long lngCreatedBy) {
		this.lngCreatedBy = lngCreatedBy;
	}
	public Date getDtCreatedOn() {
		return dtCreatedOn;
	}
	public void setDtCreatedOn(Date dtCreatedOn) {
		this.dtCreatedOn = dtCreatedOn;
	}
	public long getLngUpdatedBy() {
		return lngUpdatedBy;
	}
	public void setLngUpdatedBy(long lngUpdatedBy) {
		this.lngUpdatedBy = lngUpdatedBy;
	}
	public Date getDtUpdatedOn() {
		return dtUpdatedOn;
	}
	public void setDtUpdatedOn(Date dtUpdatedOn) {
		this.dtUpdatedOn = dtUpdatedOn;
	}
	public long getLngSessions() {
		return lngSessions;
	}
	public void setLngSessions(long lngSessions) {
		this.lngSessions = lngSessions;
	}
	public int getIntSun() {
		return intSun;
	}
	public void setIntSun(int intSun) {
		this.intSun = intSun;
	}
	public int getIntMon() {
		return intMon;
	}
	public void setIntMon(int intMon) {
		this.intMon = intMon;
	}
	public int getIntTue() {
		return intTue;
	}
	public void setIntTue(int intTue) {
		this.intTue = intTue;
	}
	public int getIntWed() {
		return intWed;
	}
	public void setIntWed(int intWed) {
		this.intWed = intWed;
	}
	public int getIntThur() {
		return intThur;
	}
	public void setIntThur(int intThur) {
		this.intThur = intThur;
	}
	public int getIntFri() {
		return intFri;
	}
	public void setIntFri(int intFri) {
		this.intFri = intFri;
	}
	public int getIntSat() {
		return intSat;
	}
	public void setIntSat(int intSat) {
		this.intSat = intSat;
	}
	public int getIntBMI() {
		return intBMI;
	}
	public void setIntBMI(int intBMI) {
		this.intBMI = intBMI;
	}
	
	public int getIntDone() {
		return intDone;
	}
	public void setIntDone(int intDone) {
		this.intDone = intDone;
	}
	public long getLngReceiptID() {
		return lngReceiptID;
	}
	public void setLngReceiptID(long lngReceiptID) {
		this.lngReceiptID = lngReceiptID;
	}
	public int getIntQuestionaire() {
		return intQuestionaire;
	}
	public void setIntQuestionaire(int intQuestionaire) {
		this.intQuestionaire = intQuestionaire;
	}
	public int getIntFitnessTest() {
		return intFitnessTest;
	}
	public void setIntFitnessTest(int intFitnessTest) {
		this.intFitnessTest = intFitnessTest;
	}
	public int getIntMeasurements() {
		return intMeasurements;
	}
	public void setIntMeasurements(int intMeasurements) {
		this.intMeasurements = intMeasurements;
	}
	public int getIntCounseling() {
		return intCounseling;
	}
	public void setIntCounseling(int intCounseling) {
		this.intCounseling = intCounseling;
	}
	public int getIntWorkoutCard() {
		return intWorkoutCard;
	}
	public void setIntWorkoutCard(int intWorkoutCard) {
		this.intWorkoutCard = intWorkoutCard;
	}
	public String getStrNote() {
		return strNote;
	}
	public void setStrNote(String strNote) {
		this.strNote = strNote;
	}
	public long getLngAttendedSessions() {
		return lngAttendedSessions;
	}
	public void setLngAttendedSessions(long lngAttendedSessions) {
		this.lngAttendedSessions = lngAttendedSessions;
	}
	public long getLngRemaningSessions() {
		return lngRemaningSessions;
	}
	public void setLngRemaningSessions(long lngRemaningSessions) {
		this.lngRemaningSessions = lngRemaningSessions;
	}
	public Date getDtBookingDate() {
		return dtBookingDate;
	}
	public void setDtBookingDate(Date dtBookingDate) {
		this.dtBookingDate = dtBookingDate;
	}
	public double getDblCostPerSession() {
		return dblCostPerSession;
	}
	public void setDblCostPerSession(double dblCostPerSession) {
		this.dblCostPerSession = dblCostPerSession;
	}
	public double getDblPayoutPercent() {
		return dblPayoutPercent;
	}
	public void setDblPayoutPercent(double dblPayoutPercent) {
		this.dblPayoutPercent = dblPayoutPercent;
	}

	public int getIntSMSCount() {
		return intSMSCount;
	}

	public void setIntSMSCount(int intSMSCount) {
		this.intSMSCount = intSMSCount;
	}

	public Date getDtlastSMSDate() {
		return dtlastSMSDate;
	}

	public void setDtlastSMSDate(Date dtlastSMSDate) {
		this.dtlastSMSDate = dtlastSMSDate;
	}
}
