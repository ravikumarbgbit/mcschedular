package com.uthkrushta.mm.utility.bean;

public class SMSTemplateNotificationBean {

	private long lngID;
	private long lngMsgCategoryID;
	private long lngMsgTypeID;
	private int intSend;
	private String strContent;
	private long lngCompanyID;
	private int intStatus;
	//Added for Excel For Upload
   private String strFileUploadExcel;
	
	public SMSTemplateNotificationBean() {
		// TODO Auto-generated constructor stub
		this.strContent="";
	}
	
	public long getLngID() {
		return lngID;
	}
	public void setLngID(long lngID) {
		this.lngID = lngID;
	}
	
	
	public long getLngMsgCategoryID() {
		return lngMsgCategoryID;
	}
	public void setLngMsgCategoryID(long lngMsgCategoryID) {
		this.lngMsgCategoryID = lngMsgCategoryID;
	}
	public long getLngMsgTypeID() {
		return lngMsgTypeID;
	}
	public void setLngMsgTypeID(long lngMsgTypeID) {
		this.lngMsgTypeID = lngMsgTypeID;
	}
	public int getIntSend() {
		return intSend;
	}
	public void setIntSend(int intSend) {
		this.intSend = intSend;
	}
	public String getStrContent() {
		return strContent;
	}
	public void setStrContent(String strContent) {
		this.strContent = strContent;
	}
	
	public long getLngCompanyID() {
		return lngCompanyID;
	}
	public void setLngCompanyID(long lngCompanyID) {
		this.lngCompanyID = lngCompanyID;
	}
	public int getIntStatus() {
		return intStatus;
	}
	public void setIntStatus(int intStatus) {
		this.intStatus = intStatus;
	}

	public String getStrFileUploadExcel() {
		return strFileUploadExcel;
	}

	public void setStrFileUploadExcel(String strFileUploadExcel) {
		this.strFileUploadExcel = strFileUploadExcel;
	}
	
	
	
}
