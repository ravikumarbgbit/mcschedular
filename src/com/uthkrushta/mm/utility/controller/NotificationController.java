package com.uthkrushta.mm.utility.controller;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.net.HttpURLConnection;
import java.net.MalformedURLException;
import java.net.URL;
import java.text.ParseException;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import com.ibm.icu.text.DateFormat;
import com.ibm.icu.text.SimpleDateFormat;
import com.ibm.icu.util.Calendar; 
import com.uthkrushta.basis.constants.IUMCConstants;
import com.uthkrushta.mm.configuration.bean.TimeZoneConfigurationBean;
import com.uthkrushta.mm.code.bean.SMSVariableCodeBean;
import com.uthkrushta.mm.configuration.bean.GlobalSettingBean;
import com.uthkrushta.mm.configuration.bean.RewardPointsConfigurationBean;
import com.uthkrushta.mm.frameWork.bean.DateRangeBean;
import com.uthkrushta.mm.gym.bean.AttendanceGymBean;
import com.uthkrushta.mm.master.bean.CompanyMasterBean;
import com.uthkrushta.mm.master.bean.FinancialYearMasterBean;
import com.uthkrushta.mm.master.bean.MemberMasterBean;
import com.uthkrushta.mm.master.bean.StaffMasterBean;

import com.uthkrushta.mm.supreme.control.EntityPlanMasterView;
import com.uthkrushta.mm.supreme.control.TriggerSuperConfigurationBean;
import com.uthkrushta.mm.transaction.bean.LoggTransactionBean;
import com.uthkrushta.mm.transaction.bean.MemberPauseTransactionBean;
import com.uthkrushta.mm.transaction.bean.PlanSubsPaidMemNameTransactionViewBean;
import com.uthkrushta.mm.transaction.bean.ServiceFormTransactionViewBean;
import com.uthkrushta.mm.utility.bean.SMSTemplateNotificationBean;

import com.uthkrushta.utils.DBUtilStatic;

import com.uthkrushta.utils.UMBException;
import com.uthkrushta.utils.WebUtil;

public class NotificationController {
	
	public static String strTimeGMT="";
	
	public static boolean delegateCall(EntityPlanMasterView objEPMV){
		boolean blnExecuted = false;
		
		
		
		//Here goes the code for Birthday/aniversary and weekly attendance rewardpoint update
		    boolean blnAttendaceReward=attendanceRewards(objEPMV);
		    boolean blnBAReward=birthdayAniversaryRewards(objEPMV);
		//Ends the  logic for Birthday/aniversary and weekly attendance;
		
		 blnExecuted=activateAndDeactivateExtendedMembers(objEPMV);
		 
		//Send SMS is center has enabled SMS notification
		if(!isSMSEnabledForCenter(objEPMV)) return true;
		
		blnExecuted=sendWishesToMembers( objEPMV);
		
		blnExecuted=sendPlanExpiryRemaindersToMembersBfrGivenExpDays(objEPMV);
		//blnExecuted=sendPlanExpiryRemainderToMembers(objEPMV);
		blnExecuted=sendPlanExpiryRemaindersToMembersBfrOneDay(objEPMV);
		blnExecuted=sendPlanExpiryRemaindersToMembersOnDay(objEPMV);
		blnExecuted=sendPlanExpiryRemaindersToMembersAfterDay(objEPMV);
		blnExecuted=sendPaymentDueMembershipRemainderToMembers(objEPMV);
		blnExecuted=sendPaymentDueServiceRemainderToMembers(objEPMV);
		
		blnExecuted=sendAlertToAbsentMembers(objEPMV);
		
		
		
		return blnExecuted;
	}

	
  //This code related to reward points for birthday and aniversary
	private static boolean birthdayAniversaryRewards(EntityPlanMasterView objEPMV) {
		// TODO Auto-generated method stub
		String strTableName = IUMCConstants.BN_GLOBAL_SETTING;
		String strSelectList = "";
		String  strWhereClause = IUMCConstants.GET_ACTIVE_ROWS;
		String strSortClause = "strName, strLastName";
		boolean blnUpdatedReward=false;
		
		GlobalSettingBean objGSB = (GlobalSettingBean)	DBUtilStatic.getRecordFromCenter(objEPMV, strTableName, strSelectList, strWhereClause, "");
		
		if( null!=objGSB && objGSB.getIntIsRewards()==1 )
		{
			String strStartDate="";
			String strEndDate="";
		   
			//To get the selected time zone of different countries
			     strTimeGMT =getTimeZone(objEPMV);
			//Getting  the time zone of different countries ends
			
			TriggerSuperConfigurationBean objTSCB=(TriggerSuperConfigurationBean) DBUtilStatic.getRecordFromSup(IUMCConstants.BN_TRIGGERSUPCONFIG);
			
			//For Week to trigger
			if(null!=objTSCB && objTSCB.getLngTriggerControl()==IUMCConstants.TG_INT_WEEK)
			{
			
				
				//Get the end Day of the week 
				Date dtCurrent = WebUtil.getCurrentDate(strTimeGMT);
				Calendar c = Calendar.getInstance();
				c.setTime(dtCurrent);
				int dayOfWeek = c.get(Calendar.DAY_OF_WEEK) - c.getFirstDayOfWeek();
				c.add(Calendar.DAY_OF_MONTH, -dayOfWeek);
				Date dtWeekStart=c.getTime();
				c.add(Calendar.DAY_OF_MONTH, 6); 
				Date dtWeekEnd = c.getTime();
				//Ends
				
				SimpleDateFormat sdf=new SimpleDateFormat("yyyy-MM-dd");
				strStartDate=sdf.format(dtWeekStart);
				strEndDate=sdf.format(dtWeekEnd);
				String strCurrentDate=sdf.format(WebUtil.getCurrentDate(strTimeGMT));
				
				try {
					dtWeekStart=sdf.parse(strStartDate);
					dtCurrent=sdf.parse(strCurrentDate);
				} catch (ParseException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				}
				
				
				
				//Check whether the current date is weekstart date
				if(null!=dtWeekStart && dtWeekStart.compareTo(dtCurrent)==0)
				{
					blnUpdatedReward=addbirthDayAniversaryRewards(objEPMV,strStartDate,strEndDate);
				}
			} //For Month to Trigger
			else if(null!=objTSCB && objTSCB.getLngTriggerControl()==IUMCConstants.TG_INT_MONTH)
			{
			  	DateRangeBean objDRB=WebUtil.getStartAndEndDate(strTimeGMT,IUMCConstants.DB_DATE_FORMAT);
			  	SimpleDateFormat sdf=new SimpleDateFormat("dd-MM-yyyy");
			  	Date dtStartMonth;
			  	try
			  	{
			  	     dtStartMonth=sdf.parse(objDRB.getStrStartDate());//Check whether current date is start date of month
				  	if(null!=dtStartMonth && dtStartMonth.compareTo(sdf.parse(WebUtil.formatDate(WebUtil.getCurrentDate(strTimeGMT), IUMCConstants.DATE_FORMAT)))==0)
					{
						blnUpdatedReward=addbirthDayAniversaryRewards(objEPMV,objDRB.getStrStartDate(),objDRB.getStrEndDate());
					}
			  	}
			  	catch (Exception e) {
					// TODO: handle exception
			  		e.printStackTrace();
				}
			     
			}
		}
		return blnUpdatedReward;
	}


    //this code executes on call from the above function
	private static boolean addbirthDayAniversaryRewards(EntityPlanMasterView objEPMV, String strStartDate, String strEndDate) {
		// TODO Auto-generated method stub
		String strWhereBDay=IUMCConstants.GET_ACTIVE_ROWS+" AND DATE_FORMAT(dtDOB,'%m-%d') >= DATE_FORMAT('" + strStartDate+ "','%m-%d')  AND DATE_FORMAT(dtDOB,'%m-%d') <= DATE_FORMAT('" + strEndDate+ "','%m-%d')";
		String strWhereADay=IUMCConstants.GET_ACTIVE_ROWS+" AND DATE_FORMAT(dtDOM,'%m-%d') >= DATE_FORMAT('" + strStartDate+ "','%m-%d')  AND DATE_FORMAT(dtDOM,'%m-%d') <= DATE_FORMAT('" + strEndDate+ "','%m-%d')";
		boolean blnUpdatedReward=false;
		try {
			
		    List lstMemBday=DBUtilStatic.getRecordsFromCenter(objEPMV, IUMCConstants.BN_MEMBER_MASTER_BEAN, "", strWhereBDay, "");
		    List lstMemAday=DBUtilStatic.getRecordsFromCenter(objEPMV, IUMCConstants.BN_MEMBER_MASTER_BEAN, "", strWhereADay, "");
		    RewardPointsConfigurationBean objRPCB=(RewardPointsConfigurationBean)DBUtilStatic.getRecordFromCenter(objEPMV, IUMCConstants.BN_REWARD_POINTS_CFG_BEAN, "",IUMCConstants.GET_ACTIVE_ROWS, "");
		    
		    //For birthday adding reward points
		    if(null!=lstMemBday && lstMemBday.size()>0)
		    { 
		    	
		    	for(int i=0;i<lstMemBday.size();i++)
		    	{
		    		MemberMasterBean objMMB=(MemberMasterBean)lstMemBday.get(i);
		    		if(null!=objMMB && null!=objEPMV && objEPMV.getIntEnableRewardSystem()==1 && isRewardEnabledForCenter(objEPMV) && null!=objRPCB && objRPCB.getIntBA()==1)
		    		{
		    		 	updateBDayAniversaryAttendRewardPoint(objMMB,objEPMV,objRPCB,IUMCConstants.LNG_BIRTHDAY);
		    		}
		    	}
		    }
		    
		  //For birthday adding reward points
		    if(null!=lstMemAday && lstMemAday.size()>0)
		    { 
		    	
		    	for(int i=0;i<lstMemAday.size();i++)
		    	{
		    		MemberMasterBean objMMB=(MemberMasterBean)lstMemAday.get(i);
		    		if(null!=objMMB && null!=objEPMV && objEPMV.getIntEnableRewardSystem()==1 && isRewardEnabledForCenter(objEPMV) && null!=objRPCB && objRPCB.getIntBA()==1)
		    		{
		    			updateBDayAniversaryAttendRewardPoint(objMMB,objEPMV,objRPCB,IUMCConstants.LNG_ANIVERSARY);
		    		}
		    	}
		    }
		    DBUtilStatic.updateRecordsOfCenter(objEPMV, IUMCConstants.BN_GLOBAL_SETTING, "dtLastBATrigger = '"+WebUtil.formatDate(WebUtil.getCurrentDate(strTimeGMT), IUMCConstants.DB_DATE_FORMAT)+"'", IUMCConstants.GET_ACTIVE_ROWS);
		 } catch (UMBException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
			return false;
		} 
		return blnUpdatedReward;
	}


///Attendance related  rewardpoints
	private static boolean attendanceRewards(EntityPlanMasterView objEPMV) {
		// TODO Auto-generated method stub
		
		
		String strTableName = IUMCConstants.BN_GLOBAL_SETTING;
		String strSelectList = "";
		String  strWhereClause = IUMCConstants.GET_ACTIVE_ROWS;
		String strSortClause = "strName, strLastName";
		boolean blnUpdatedReward=false;
		
		GlobalSettingBean objGSB = (GlobalSettingBean)	DBUtilStatic.getRecordFromCenter(objEPMV, strTableName, strSelectList, strWhereClause, "");
		
		if( null!=objGSB && objGSB.getIntIsRewards()==1 )
		{
			//To get the selected time zone of different countries
			   strTimeGMT=getTimeZone(objEPMV);
			//Getting  the time zone of different countries ends
			
			
			
			//Get the end Day of the week 
			Date dtCurrent = WebUtil.getCurrentDate(strTimeGMT);
			Calendar c = Calendar.getInstance();
			c.setTime(dtCurrent);
			int dayOfWeek = c.get(Calendar.DAY_OF_WEEK) - c.getFirstDayOfWeek();
			c.add(Calendar.DAY_OF_MONTH, -dayOfWeek);
			c.add(Calendar.DAY_OF_MONTH, 6); 
			Date weekEnd = c.getTime();
			SimpleDateFormat sdf=new SimpleDateFormat("dd-MM-yyyy");
			String strWeekEnd= sdf.format(weekEnd);
			String strCurrentDate=sdf.format(dtCurrent);
			try {
				weekEnd=sdf.parse(strWeekEnd);
				dtCurrent=sdf.parse(strCurrentDate);
			} catch (ParseException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
			//Ends
			
			 if(weekEnd.compareTo(dtCurrent)==0)
			 {
				blnUpdatedReward=addAttendanceRewardPoints(objEPMV,objGSB);
			 }
			 
			//Check the last attendance trigger date
			/*if(last attendance trigger date==null)
			{
				//trigger now
				
			}else
			{
			   get the last triggered date and add 7 days to it
			   if(the 7 day is current day then trigger)
			   {
			       trigger now
			   }
			   else
			   {
			   return false;
			}*/
			 
			 return blnUpdatedReward;
		}
		else
		{
		return blnUpdatedReward;
		}
		
		
		
		
	}
 //this code executes on call from the above code
private static boolean addAttendanceRewardPoints(EntityPlanMasterView objEPMV,GlobalSettingBean objGSB) {
		// TODO Auto-generated method stub
	boolean blnUpdatedReward=false;
	try {
		/*DBUtilStatic.updateRecordsOfCenter(objEPMV, IUMCConstants.BN_GLOBAL_SETTING, "dtLastAttendanceTrigger = "+WebUtil.getCurrentDate(request), IUMCConstants.GET_ACTIVE_ROWS);*/
	     
		
	List lstCenterMembers=DBUtilStatic.getRecordsFromCenter(objEPMV, IUMCConstants.BN_MEMBER_MASTER_BEAN, "", IUMCConstants.GET_ACTIVE_ROWS, "");
	   if(null!=lstCenterMembers && lstCenterMembers.size()>0)
	   {
		   RewardPointsConfigurationBean objRPCB=(RewardPointsConfigurationBean)DBUtilStatic.getRecordFromCenter(objEPMV, IUMCConstants.BN_REWARD_POINTS_CFG_BEAN, "",IUMCConstants.GET_ACTIVE_ROWS, "");
		  
		   for(int i=0;i<lstCenterMembers.size();i++)
		   {
			   MemberMasterBean objMMB=(MemberMasterBean)lstCenterMembers.get(i);
			   if(null!=objMMB)
			   {
				   List lstPresentDays=null;   
			   
				   if(null!=objGSB && objGSB.getDtLastAttendanceTrigger()==null)
				   {
				        lstPresentDays = DBUtilStatic.getRecordsFromCenter(objEPMV, IUMCConstants.BN_ATTENDANCE_GYM_BEAN, "", IUMCConstants.GET_ACTIVE_ROWS+" and intLoggTypeID =1  and lngMemberID = "+objMMB.getLngID()+" group by date(dtLoginDate)", "");
				   }
				   else if(null!=objGSB && objGSB.getDtLastAttendanceTrigger()!=null)
				   {
					   lstPresentDays = DBUtilStatic.getRecordsFromCenter(objEPMV, IUMCConstants.BN_ATTENDANCE_GYM_BEAN, "", IUMCConstants.GET_ACTIVE_ROWS+" and intLoggTypeID =1 and lngMemberID = "+objMMB.getLngID()+" and dtLoginDate > "+WebUtil.formatDate(objGSB.getDtLastAttendanceTrigger(), IUMCConstants.DB_DATE_FORMAT) +" group by date(dtLoginDate)", "");  
				   }
				   
			    
			       if(null!=objRPCB && objRPCB.getIntAttendance()==1 && null!=lstPresentDays && lstPresentDays.size()>0 &&  objRPCB.getLngPresentDays()>=lstPresentDays.size() && null!=objEPMV && objEPMV.getIntEnableRewardSystem()==1 && isRewardEnabledForCenter(objEPMV) )
			       {                
			    	   updateBDayAniversaryAttendRewardPoint(objMMB,objEPMV,objRPCB,IUMCConstants.LNG_ATTENDANCE);
			       }
			    
			   }
		   }
	   }
	   DBUtilStatic.updateRecordsOfCenter(objEPMV, IUMCConstants.BN_GLOBAL_SETTING, "dtLastAttendanceTrigger = '"+WebUtil.formatDate(WebUtil.getCurrentDate(strTimeGMT), IUMCConstants.DB_DATE_FORMAT)+"'", IUMCConstants.GET_ACTIVE_ROWS); 
	} catch (UMBException e) {
		// TODO Auto-generated catch block
		e.printStackTrace();
		return false;
	}
		return blnUpdatedReward;
	}



/*trigger logic
 * 
 * Get list of all member of the current center
 * 
 * lstBirthDayMemberList = DBUtilStatic.getRecordsFromCenter(objEPMV, strTableName, strSelectList, strBDWhereClause, strSortClause);
 * loop over each member 
 * get the no of days present (last week till yesterday)
 * if(the no of days is greater than the centers specified day)
 * {
 *   for that member profile add points of that center
 * }
 * 
 * */	

   //Checking whether the client has enabled the SMS in Global Setting
	private static boolean isSMSEnabledForCenter(EntityPlanMasterView objEPMV) {
		String strTableName = IUMCConstants.BN_GLOBAL_SETTING;
		String strSelectList = "";
		String  strWhereClause = IUMCConstants.GET_ACTIVE_ROWS;
		String strSortClause = "strName, strLastName";
		
		GlobalSettingBean objGSB = (GlobalSettingBean)	DBUtilStatic.getRecordFromCenter(objEPMV, strTableName, strSelectList, strWhereClause, "");
		
		if( null!=objGSB && objGSB.getIntIsSMS()==1 )
		{
			//To get the selected time zone of different countries
			 strTimeGMT=getTimeZone(objEPMV);
			//Getting  the time zone of different countries ends
			
			return true;
		}
		else
		{
		return false;
		}
	}

	

	public static boolean sendWishesToMembers(EntityPlanMasterView objEPMV){
		List lstBirthDayMemberList = null;
		List lstAnniversaryMemberList = null;
		List lstPlanExpirySevenBfrMemberList=null;
		
		 Calendar c = Calendar.getInstance();
		 c.add(Calendar.DATE, 7);  // number of days to add
		 String strNextsevenDate= WebUtil.formatDate(c.getTime(), IUMCConstants.DB_DATE_FORMAT);
		
		String strTableName = IUMCConstants.BN_MEMBER_MASTER_BEAN;
		String strSelectList = "";
		String  strBDWhereClause =IUMCConstants.GET_ACTIVE_ROWS+" and DAY(dtDOB)=DAY('"+WebUtil.formatDate(WebUtil.getCurrentDate(strTimeGMT), IUMCConstants.DB_DATE_FORMAT)+"') and MONTH(dtDOB)=MONTH('"+WebUtil.formatDate(WebUtil.getCurrentDate(strTimeGMT), IUMCConstants.DB_DATE_FORMAT)+"') and intSMSNotification = 1"; //IUBESPOKEConstants.GET_ACTIVE_ROWS;
		String  strADWhereClause = IUMCConstants.GET_ACTIVE_ROWS+" and DAY(dtDOM)=DAY('"+WebUtil.formatDate(WebUtil.getCurrentDate(strTimeGMT), IUMCConstants.DB_DATE_FORMAT)+"') and MONTH(dtDOM)=MONTH('"+WebUtil.formatDate(WebUtil.getCurrentDate(strTimeGMT), IUMCConstants.DB_DATE_FORMAT)+"') and intSMSNotification = 1";//IUBESPOKEConstants.GET_ACTIVE_ROWS;
		String  strMemExpWhereClause=IUMCConstants.GET_ACTIVE_ROWS+" and dtToDate = '"+strNextsevenDate+"'  and intSMSNotification = 1";
		String strSortClause = "strName, strLastName";
		boolean blnExecuted = false;
		
       
		
		MemberMasterBean objMemberMasterBean = new MemberMasterBean();
		
		// Get SMS configuration
		 String strWhereSMS=IUMCConstants.GET_ACTIVE_ROWS+" and intSMSType = "+IUMCConstants.TRANSACTIONAL;
		 SMSVariableCodeBean objSMSVar=(SMSVariableCodeBean) DBUtilStatic.getRecordFromCenter(objEPMV, IUMCConstants.BN_SMS_VARIABLE_CODE_BEAN, strSelectList, strWhereSMS, "");
		 if(null==objSMSVar) return false;
		
		 // Get Members who has b'day and anniversary and sms notification set to true
		lstBirthDayMemberList = DBUtilStatic.getRecordsFromCenter(objEPMV, strTableName, strSelectList, strBDWhereClause, strSortClause);
		lstAnniversaryMemberList = DBUtilStatic.getRecordsFromCenter(objEPMV, strTableName, strSelectList, strADWhereClause, strSortClause);
		
		MemberMasterBean objMMB=new MemberMasterBean();
		// Loop on the list and send SMS wishes
		//Bday Loop
		  if(null!=lstBirthDayMemberList && lstBirthDayMemberList.size()>0)  
		  {
		// for each member record, get appropriate sms template for bday
		   for(int i=0;i<lstBirthDayMemberList.size();i++)
		   {
			   objMMB=new MemberMasterBean();
			   objMMB=(MemberMasterBean)lstBirthDayMemberList.get(i);
			   if(null!=objMMB)
			   {
				   sendMessage(objMMB,objSMSVar,objEPMV,IUMCConstants.BIRTHDAY);
			   }
		   }
		// send sms
				
		  }
		// End of Bday Loop
		
		//Ann loop
		
		  if(null!=lstAnniversaryMemberList && lstAnniversaryMemberList.size()>0)  
		  {
		// for each member record, get appropriate sms template for Aday
			   for(int i=0;i<lstAnniversaryMemberList.size();i++)
			   {
				   objMMB=new MemberMasterBean();
				   objMMB=(MemberMasterBean)lstAnniversaryMemberList.get(i);
				   if(null!=objMMB)
				   {
					   sendMessage(objMMB,objSMSVar,objEPMV,IUMCConstants.ANIVERSARY);
				   }
			   }
		// send sms
				
		  }
		
		
		//End of Ann loop
		  
			  
		return blnExecuted;
	}
	
	
	
	
	
	//main calling function
	private static boolean sendPlanExpiryRemaindersToMembersBfrGivenExpDays(EntityPlanMasterView objEPMV){
		boolean blnSendToPlanExpMem=false;
		 GlobalSettingBean objGSB=(GlobalSettingBean) DBUtilStatic.getRecordFromCenter(objEPMV, IUMCConstants.BN_GLOBAL_SETTING, "", IUMCConstants.GET_ACTIVE_ROWS, "");
		 int intPlanExpiryBeforeDays=0;
		 if(null!=objGSB){
			 intPlanExpiryBeforeDays=objGSB.getIntPlanExpiryNotificationDays();
		 }
		 
		return sendPlanExpiryRemainderToMembers(objEPMV,intPlanExpiryBeforeDays,IUMCConstants.RENEWAL);
	}
	
	//main calling function
	private static boolean sendPlanExpiryRemaindersToMembersBfrOneDay(EntityPlanMasterView objEPMV){
		boolean blnSendToPlanExpMem=false;
		 int intPlanExpiryBeforeOneDay=1;
		return sendPlanExpiryRemainderToMembers(objEPMV,intPlanExpiryBeforeOneDay,IUMCConstants.RENEWAL);
	}
	
	//main calling function
		private static boolean sendPlanExpiryRemaindersToMembersOnDay(EntityPlanMasterView objEPMV){
			boolean blnSendToPlanExpMem=false;
			 int intPlanExpiryBeforeOnDay=0;
			return sendPlanExpiryRemainderToMembers(objEPMV,intPlanExpiryBeforeOnDay,IUMCConstants.TODAY_EXPIRY);
		}
		//main calling function
				private static boolean sendPlanExpiryRemaindersToMembersAfterDay(EntityPlanMasterView objEPMV){
					boolean blnSendToPlanExpMem=false;
					 int intPlanExpiryAfterDay=-1;
					return sendPlanExpiryRemainderToMembers(objEPMV,intPlanExpiryAfterDay,IUMCConstants.PLAN_EXPIRED);
				}	
	
	private static boolean sendPlanExpiryRemainderToMembers(EntityPlanMasterView objEPMV,int intDays, int intMSGType) {
		// TODO Auto-generated method stub
		
		List lstPlanExpirySevenBfrMemberList=null;
		boolean blnSendToPlanExpMem=false;
		 Calendar c = Calendar.getInstance();
		// GlobalSettingBean objGSB=(GlobalSettingBean) DBUtilStatic.getRecordFromCenter(objEPMV, IUMCConstants.BN_GLOBAL_SETTING, "", IUMCConstants.GET_ACTIVE_ROWS, "");
		 /*int intPlanExpiryBeforeDays=0;
		 if(null!=objGSB){
			 intPlanExpiryBeforeDays=objGSB.getIntPlanExpiryNotificationDays();
		 }*/
		 c.add(Calendar.DATE, intDays);  // number of days to add
		 String strNextsevenDate= WebUtil.formatDate(c.getTime(), IUMCConstants.DB_DATE_FORMAT);
		 
		 String  strMemExpWhereClause=IUMCConstants.GET_ACTIVE_ROWS+" and dtToDate = '"+strNextsevenDate+"'  and intSMSNotification = 1 and intIsCurrent = 1";
		 lstPlanExpirySevenBfrMemberList=DBUtilStatic.getRecordsFromCenter(objEPMV, IUMCConstants.BN_PLAN_SUBS_MEM_NAME, "", strMemExpWhereClause, "");
		// Get SMS configuration
				 String strWhereSMS=IUMCConstants.GET_ACTIVE_ROWS+" and intSMSType = "+IUMCConstants.TRANSACTIONAL;
				 SMSVariableCodeBean objSMSVar=(SMSVariableCodeBean) DBUtilStatic.getRecordFromCenter(objEPMV, IUMCConstants.BN_SMS_VARIABLE_CODE_BEAN, "", strWhereSMS, "");
				 if(null==objSMSVar) return false;
		 
				//Members with plan expiring in seven days
				 PlanSubsPaidMemNameTransactionViewBean objPSPMN= new PlanSubsPaidMemNameTransactionViewBean();
				   if(null!=lstPlanExpirySevenBfrMemberList)
				   {
				     //for loop of the list of members
					 for(int i=0;i<lstPlanExpirySevenBfrMemberList.size();i++)
					 {
						 objPSPMN=new PlanSubsPaidMemNameTransactionViewBean(); 
						 objPSPMN = (PlanSubsPaidMemNameTransactionViewBean)lstPlanExpirySevenBfrMemberList.get(i);
						 
		                if(null!=objPSPMN)
		                {
				           //for each member get the appropriate sms template
		                	String strWhereSMSTemplate=IUMCConstants.GET_ACTIVE_ROWS+" and lngMsgTypeID = "+intMSGType+" and intSend = 1 and lngCompanyID= "+objPSPMN.getLngCompanyID();
		 				    SMSTemplateNotificationBean objSMSTNB=(SMSTemplateNotificationBean)DBUtilStatic.getRecordFromCenter(objEPMV, IUMCConstants.BN_SMS_TEMP_NOTIFICATION, "", strWhereSMSTemplate, "");
		 				   
		 				   String strWhereCompany=IUMCConstants.GET_ACTIVE_ROWS+" and lngID = "+objPSPMN.getLngCompanyID();
		 				   CompanyMasterBean objCMB=(CompanyMasterBean)DBUtilStatic.getRecordFromCenter(objEPMV, IUMCConstants.BN_COMPANY_MASTER_BEAN, "", strWhereCompany, "");
		                   
		 				  //look
						   StringBuilder builder = new StringBuilder();
						   String strSMSAPI = "";
							 if(null!=objSMSVar && objSMSVar.getStrName().trim().length()>0)
							 {
							 strSMSAPI=objSMSVar.getStrName();
							 }
							 String strSMSAPIForCust = strSMSAPI ;
							 URL url = null; 
							 HttpURLConnection uc;
							 //look
		 				    
							 if(null!=objSMSTNB)
							 {
								//look
						         String strContent="";
						         String strSpecificContent = "";
							     strContent =objSMSTNB.getStrContent();
								 strContent = strContent.replaceAll(" ", "%20");
								 strSpecificContent=strContent;
								 
								 //To get the Admin Name of the center
								 String strWhereStaff= IUMCConstants.GET_ACTIVE_ROWS+" and lngID = "+objCMB.getLngAdminID();
								 StaffMasterBean objAdminStaff=(StaffMasterBean) DBUtilStatic.getRecordFromCenter(objEPMV, IUMCConstants.BN_STAFF_MASTER_BEAN, "", strWhereStaff, "");
								 String strAdminName="";
								 if(null!=objAdminStaff && objAdminStaff.getStrName().trim().length()>0)
								 {
									 strAdminName=objAdminStaff.getStrName()+" "+objAdminStaff.getStrLastName();
								 }
							   //look
								 
								 
								 // replace the sms template with actual text : replacing tokens to actual values
								   
									
								 String strName=objPSPMN.getStrMemberName()+" "+objPSPMN.getStrMemberLastName();
						 			if(null!=strName && strName.trim().length()>0)
						 			 {
						 			    strSpecificContent=strSpecificContent.replace(IUMCConstants.ST_CUST_NAME, strName);
						 			 }
						 			 else
						 			 {
						 				strSpecificContent=strSpecificContent.replace(IUMCConstants.ST_CUST_NAME,"");
						 			 }
						 			    
						 			  strSpecificContent=strSpecificContent.replace(IUMCConstants.ST_CUST_ADDRESS, "");
						 			  strSpecificContent=strSpecificContent.replace(IUMCConstants.ST_CUST_PH_NO, "");
						 			   
						 			  if(null!=objPSPMN.getStrContactNo() &&  objPSPMN.getStrContactNo().trim().length()>0)
						 			  {
						 			    strSpecificContent=strSpecificContent.replace(IUMCConstants.ST_CUST_MOB_NO, objPSPMN.getStrContactNo());
						 			  }
						 			  else
						 			  {
						 				 strSpecificContent=strSpecificContent.replace(IUMCConstants.ST_CUST_MOB_NO,"");
						 			  }
						 			 if(null!=objPSPMN.getStrEmailID() &&  objPSPMN.getStrEmailID().trim().length()>0)
						 			  {
							 			strSpecificContent=strSpecificContent.replace(IUMCConstants.ST_CUST_EMAIL, objPSPMN.getStrEmailID());
						 			  }
						 			 else
						 			 {
						 				strSpecificContent=strSpecificContent.replace(IUMCConstants.ST_CUST_EMAIL,"");
						 			 }
						 			 
						 			strSpecificContent=strSpecificContent.replace(IUMCConstants.ST_DOB,"");
						 		    strSpecificContent=strSpecificContent.replace(IUMCConstants.ST_DOM,"");
						 			
						 		    if(null!=objPSPMN.getDtToDate()){
						 		    	strSpecificContent=strSpecificContent.replace(IUMCConstants.ST_DOC_DATE, ""+objPSPMN.getDtToDate());
						 		    }else{
						 		    	strSpecificContent=strSpecificContent.replace(IUMCConstants.ST_DOC_DATE, "");
						 		    }
						 		    
						 			 if(null!=objCMB.getStrName() &&  objCMB.getStrName().trim().length()>0)
						 			  {
							 			strSpecificContent=strSpecificContent.replace(IUMCConstants.ST_CENTER_NAME, objCMB.getStrName());
						 			  }	
						 			 else
						 			 {
						 				strSpecificContent=strSpecificContent.replace(IUMCConstants.ST_CENTER_NAME, "");
						 			 }
						 			if(null!=objCMB.getStrPrimaryMobileNumber() &&  objCMB.getStrPrimaryMobileNumber().trim().length()>0)
						 			  {
							 			strSpecificContent=strSpecificContent.replace(IUMCConstants.ST_CENTER_NO, objCMB.getStrPrimaryMobileNumber());
						 			  }
						 			else
						 			{
						 				strSpecificContent=strSpecificContent.replace(IUMCConstants.ST_CENTER_NO,"");
						 			}
						 			if(null!=objCMB.getStrAddress() &&  objCMB.getStrAddress().trim().length()>0)
						 			  {
							 			strSpecificContent=strSpecificContent.replace(IUMCConstants.ST_CENTER_ADDRESS, objCMB.getStrAddress());
						 			  }
						 			else
						 			{
						 				strSpecificContent=strSpecificContent.replace(IUMCConstants.ST_CENTER_ADDRESS,"");
						 			}
						 			if(null!=strAdminName &&  strAdminName.trim().length()>0)
						 			  {
							 			strSpecificContent=strSpecificContent.replace(IUMCConstants.ST_CENTER_ADMIN, strAdminName);
						 			  }
						 			else
						 			{
						 				strSpecificContent=strSpecificContent.replace(IUMCConstants.ST_CENTER_ADMIN, "");
						 			}
						 			if(null!=objCMB.getStrContactEmail() &&  objCMB.getStrContactEmail().trim().length()>0)
						 			  {
							 			strSpecificContent=strSpecificContent.replace(IUMCConstants.ST_CENTER_EMAIL, objCMB.getStrContactEmail());
						 			  }
						 			else
						 			{
						 				strSpecificContent=strSpecificContent.replace(IUMCConstants.ST_CENTER_EMAIL, "");
						 			}
						 			 
						 			 
						 			 
						 			 strSpecificContent = strSpecificContent.replaceAll(" ", "%20");
									 strSMSAPIForCust=strSMSAPIForCust.replace(IUMCConstants.ST_SEND_TO,objPSPMN.getStrContactNo());
									 strSMSAPIForCust = strSMSAPIForCust.replace(IUMCConstants.ST_SMS_TEXT, builder.append(strSpecificContent).toString());
						         
								 System.out.print(strSMSAPIForCust);
							     try {
									url = new URL(strSMSAPIForCust);
									System.out.println(strSMSAPIForCust);
									uc = (HttpURLConnection)url.openConnection();
									uc.getResponseMessage();
								    uc.disconnect();
								    System.out.print(strSMSAPIForCust);
								    blnSendToPlanExpMem= true;
								} catch (MalformedURLException e) {
									// TODO Auto-generated catch block
									e.printStackTrace();
									blnSendToPlanExpMem= false;
								}
								catch (IOException e) {
							// TODO Auto-generated catch block
									e.printStackTrace();
									blnSendToPlanExpMem= false;
								}
							 }
						 	 
							 
		                }
					 }
				  //End of loop
				   }
				 //End of Members with plan expiring in seven days
		
		 
		 return blnSendToPlanExpMem;
	}
	

	private static boolean sendAlertToAbsentMembers(EntityPlanMasterView objEPMV) {
		// TODO Auto-generated method stub
		
		String  strSortClause=" strName,strLastName";
		AttendanceGymBean objAttBean = new AttendanceGymBean();
		List lstAbsentMembers=new ArrayList();
		boolean blnSendToAbsent=false;
		
		
      GlobalSettingBean objGSB = (GlobalSettingBean)DBUtilStatic.getRecordFromCenter(objEPMV, IUMCConstants.BN_GLOBAL_SETTING, "", IUMCConstants.GET_ACTIVE_ROWS, "");
		
		//first get the list of members who are absent in a week
		List lstMember=DBUtilStatic.getRecordsFromCenter(objEPMV, IUMCConstants.BN_MEMBER_MASTER_BEAN, "", IUMCConstants.GET_ACTIVE_ROWS+" and lngMemberStatus="+IUMCConstants.MEM_STATUS_ACTIVE, strSortClause);
		if(null!=lstMember && lstMember.size()>0)
		{
			for(int i=0;i<lstMember.size();i++)
			{
				MemberMasterBean objMMBA=new MemberMasterBean();
				objMMBA=(MemberMasterBean)lstMember.get(i);
			     if(null!=objMMBA)
			     {
			    	 int intMemberAbsentCount=0;
						
			    	
			    	     
			    	 Date dtCurrentDate=WebUtil.getCurrentDate(strTimeGMT);
			    	 Date dtLastAttendedDate=objMMBA.getDtLastAttendedDate();
			    	 int intAbsentDays =0;
			    	 
			    	 //Checking whether the extension date is lesser than the current date and last attended date is also less than extension toDate
			    	 MemberPauseTransactionBean objMPTB =(MemberPauseTransactionBean)DBUtilStatic.getRecordFromCenter(objEPMV,IUMCConstants.BN_MEMBER_PAUSE_TRANSACTION_BEAN,"", " lngMemeberID = "+objMMBA.getLngID()+" and lngPauseTypeID="+IUMCConstants.FREEZED+" and intLatestRecord=1", ""); 
    				 if(null!=objMPTB && null!=objMPTB.getDtTo() && objMPTB.getDtTo().getTime() < dtCurrentDate.getTime() && null!=objMMBA.getDtLastAttendedDate() && objMMBA.getDtLastAttendedDate().getTime()< objMPTB.getDtTo().getTime()){
    					 intAbsentDays= (int)( (dtCurrentDate.getTime() - (objMPTB.getDtTo().getTime()+1* 24 * 60 * 60 * 1000))/ (1000 * 60 * 60 * 24) );
    				 }else
			    			 if(null!=objMMBA.getDtLastAttendedDate())
			    			 {
			    				 intAbsentDays= (int)( (dtCurrentDate.getTime() - dtLastAttendedDate.getTime())/ (1000 * 60 * 60 * 24) );
			    			 }
			    				else
			    				 if(null!=objMMBA.getDtDOJ()){//Checking if he is a new joinee and absent 
			    				 intAbsentDays= (int)( (dtCurrentDate.getTime() - objMMBA.getDtDOJ().getTime())/ (1000 * 60 * 60 * 24) );
			    				 }
			    			 
			    			 
			    	 int intIntervalSMSDays=0;
			    	        if(null!= objMMBA.getDtLastSentSMS())
			    	        {
			    	        	intIntervalSMSDays= (int)((dtCurrentDate.getTime() - objMMBA.getDtLastSentSMS() .getTime())/ (1000 * 60 * 60 * 24) );
			    	        }
			    	    if(intAbsentDays > objGSB.getIntContinuousAbsent() && objMMBA.getIntSMSSentCount() <= objGSB.getIntNoOfAlertSMS() && (intIntervalSMSDays >objGSB.getIntSMSIntervalDays()||intIntervalSMSDays==0) )   
			    	    {
			    	    	lstAbsentMembers.add(objMMBA);
			    	        int	intMemberSMSCount=objMMBA.getIntSMSSentCount()+1;
			    	    	String strMemberUpdateClause="intSMSSentCount = "+intMemberSMSCount+",dtLastSentSMS = '"+WebUtil.formatDate(WebUtil.getCurrentDate(strTimeGMT), IUMCConstants.DB_DATE_FORMAT)+"'";
			    	    	String strWhereClause="lngID ="+objMMBA.getLngID();
			    	    	
			    	    	try {
								DBUtilStatic.updateRecordsOfCenter(objEPMV, IUMCConstants.BN_MEMBER_MASTER_BEAN, strMemberUpdateClause, strWhereClause);
							} catch (UMBException e) {
								// TODO Auto-generated catch block
								e.printStackTrace();
							}
			    	    }
			    	 
						
			     }	
			}
		}
		//End of getting Absent members list
		
		
		String strTableName = IUMCConstants.BN_MEMBER_MASTER_BEAN;
		String strSelectList = "";
		
         MemberMasterBean objMemberMasterBean = new MemberMasterBean();
		
		// Get SMS configuration
		 String strWhereSMS=IUMCConstants.GET_ACTIVE_ROWS+" and intSMSType = "+IUMCConstants.TRANSACTIONAL;
		 SMSVariableCodeBean objSMSVar=(SMSVariableCodeBean) DBUtilStatic.getRecordFromCenter(objEPMV, IUMCConstants.BN_SMS_VARIABLE_CODE_BEAN, strSelectList, strWhereSMS, "");
		 if(null==objSMSVar) return false;
		
		 
		
		MemberMasterBean objMMB=new MemberMasterBean();
		// Loop on the list and send SMS Alert
		//Absent Member Loop
		  if(null!=lstAbsentMembers && lstAbsentMembers.size()>0)  
		  {
		// for each member record, get appropriate sms template for Absent
		   for(int i=0;i<lstAbsentMembers.size();i++)
		   {
			   objMMB=new MemberMasterBean();
			   objMMB=(MemberMasterBean)lstAbsentMembers.get(i);
			   if(null!=objMMB)
			   {
				   sendMessage(objMMB, objSMSVar, objEPMV, IUMCConstants.ABSENT);
				   
			   }
		   }
		// send sms
				
		  }
		// End of Bday Loop
		
		
		return blnSendToAbsent;
	}
	
	
	private static boolean activateAndDeactivateExtendedMembers(EntityPlanMasterView objEPMV) {
		// TODO Auto-generated method stub
		
		DateFormat dateFormat = new SimpleDateFormat("yyyy-MM-dd");
        Calendar cal = Calendar.getInstance();
        cal.add(Calendar.DATE, -1);    
        String strToDate=dateFormat.format(cal.getTime());
		boolean blnActivate=false;
		
        //Getting the time zone of the country
		
		strTimeGMT=getTimeZone(objEPMV);
		//Getting the time zone of the country ends
		
		
        String strCurrentDate=dateFormat.format(WebUtil.getCurrentDate(strTimeGMT));
		
		String strTableName=IUMCConstants.BN_MEMBER_PAUSE_TRANSACTION_BEAN;
		String strMemPauseWhereClause=IUMCConstants.GET_ACTIVE_ROWS+" and dtTo = '"+strToDate+"'";
		String strSortClause="";
		String strMemDeactiveWhereClause=IUMCConstants.GET_ACTIVE_ROWS+" and intLatestRecord = 1 and dtFrom = '"+strCurrentDate+"'";
		
		//Getting list of members who are about to freeze
		String strFreezeDeactivate=IUMCConstants.GET_ACTIVE_ROWS+" and intLatestRecord = 1 and lngPauseTypeID =1 and dtMemberShipExtension = '"+strCurrentDate+"'";
		List lstMemberFreezeDeactivate=DBUtilStatic.getRecordsFromCenter(objEPMV, strTableName, "", strFreezeDeactivate, "");
		
		//List of Members who are going to deactivate today based on extension
		List lstMemberDeactivate=DBUtilStatic.getRecordsFromCenter(objEPMV, strTableName, "", strMemDeactiveWhereClause, strSortClause);
		
		//Get the List of members where current date-1=to_date; Here the extension of the members ends and he will be activated
		List lstMemberExtensionEnds=DBUtilStatic.getRecordsFromCenter(objEPMV, strTableName, "", strMemPauseWhereClause, strSortClause);
		if(lstMemberExtensionEnds!=null)
		{
			//for each member update the member status to active
			for(int i=0;i<lstMemberExtensionEnds.size();i++)
			{
				MemberPauseTransactionBean objMPauseTransactionBean= new MemberPauseTransactionBean();
				objMPauseTransactionBean=(MemberPauseTransactionBean)lstMemberExtensionEnds.get(i);
				if(null!=objMPauseTransactionBean)
				{
					String strMemUpdateClause="lngMemberStatus = 1,intIsPaused = 0,lngPauseTypeID = 0";
					String strMemWhereClause=IUMCConstants.GET_ACTIVE_ROWS+" and lngID = "+objMPauseTransactionBean.getLngMemeberID();
					
					String strPauseWhere=IUMCConstants.GET_ACTIVE_ROWS+" and lngID = "+objMPauseTransactionBean.getLngID();
					String strPauseUpdate="intIsPaused =0";
					
					try {
						DBUtilStatic.updateRecordsOfCenter(objEPMV, IUMCConstants.BN_MEMBER_MASTER_BEAN, strMemUpdateClause, strMemWhereClause);
						DBUtilStatic.updateRecordsOfCenter(objEPMV,IUMCConstants.BN_MEMBER_PAUSE_TRANSACTION_BEAN, strPauseUpdate, strPauseWhere);
						blnActivate=true;
					} catch (UMBException e) {
						// TODO Auto-generated catch block
						e.printStackTrace();
						blnActivate=false;
					}
				}
			}
			
		}
		
		
		//For Deactivating member with extension
		if(lstMemberDeactivate!=null)
		{
			//for each member update the member status to de-active
			for(int i=0;i<lstMemberDeactivate.size();i++)
			{
				MemberPauseTransactionBean objMPauseTransactionBean= new MemberPauseTransactionBean();
				objMPauseTransactionBean=(MemberPauseTransactionBean)lstMemberDeactivate.get(i);
				if(null!=objMPauseTransactionBean)
				{
					String strMemUpdateClause="lngMemberStatus = 2";
					String strMemWhereClause=IUMCConstants.GET_ACTIVE_ROWS+" and lngID = "+objMPauseTransactionBean.getLngMemeberID();
					try {
						DBUtilStatic.updateRecordsOfCenter(objEPMV, IUMCConstants.BN_MEMBER_MASTER_BEAN, strMemUpdateClause, strMemWhereClause);
						blnActivate=true;
					} catch (UMBException e) {
						// TODO Auto-generated catch block
						e.printStackTrace();
						blnActivate=false;
					}
				}
			}
			
		}
		
		//For Deactivating member with freeze
		
		
				if(lstMemberFreezeDeactivate!=null)
				{
					//for each member update the member status to de-active
					for(int i=0;i<lstMemberFreezeDeactivate.size();i++)
					{
						MemberPauseTransactionBean objMPauseTransactionBean= new MemberPauseTransactionBean();
						objMPauseTransactionBean=(MemberPauseTransactionBean)lstMemberFreezeDeactivate.get(i);
						if(null!=objMPauseTransactionBean)
						{
							String strMemUpdateClause="lngMemberStatus = 2";
							String strMemWhereClause=IUMCConstants.GET_ACTIVE_ROWS+" and lngID = "+objMPauseTransactionBean.getLngMemeberID();
							try {
								DBUtilStatic.updateRecordsOfCenter(objEPMV, IUMCConstants.BN_MEMBER_MASTER_BEAN, strMemUpdateClause, strMemWhereClause);
								blnActivate=true;
							} catch (UMBException e) {
								// TODO Auto-generated catch block
								e.printStackTrace();
								blnActivate=false;
							}
						}
					}
					
				}
		
				//For Deactivating member with MemberShip expiry
		List  lstMembershipEnds=DBUtilStatic.getRecordsFromCenter(objEPMV, IUMCConstants.BN_PLAN_SUBS_MEM_NAME, "", IUMCConstants.GET_ACTIVE_ROWS+" and intIsCurrent=1 and dtToDate >'"+strCurrentDate+"'", "");
		
		if(null!=lstMembershipEnds && lstMembershipEnds.size()>0){
		   String strMemIDs="";
			for(int i=0;i<lstMembershipEnds.size();i++){
				 PlanSubsPaidMemNameTransactionViewBean objPSMTV=(PlanSubsPaidMemNameTransactionViewBean)lstMembershipEnds.get(i);
				 if(null!=objPSMTV){
					 strMemIDs +=objPSMTV.getLngMemID()+";";
				 }
			}
			
			if(null!=strMemIDs && strMemIDs.trim().length()>0 && !strMemIDs.equalsIgnoreCase(";")){
				strMemIDs=strMemIDs.substring(0, strMemIDs.length()-1);
				try {
					DBUtilStatic.updateRecordsOfCenter(objEPMV, IUMCConstants.BN_MEMBER_MASTER_BEAN, "lngMemberStatus = 2", IUMCConstants.GET_ACTIVE_ROWS+" and lngID in ("+strMemIDs+")");
					blnActivate=true;
				} catch (UMBException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
					blnActivate=false;
				}
			}
		}
		
		
		
		
		return blnActivate;
		
	}
	
	
	
	public static List getMemberRecords(EntityPlanMasterView objEPMV){
		List lstMemberList = null;
		String strTableName = IUMCConstants.BN_MEMBER_MASTER_BEAN;
		String strSelectList = "";
		String  strWhereClause = IUMCConstants.GET_ACTIVE_ROWS;
		String strSortClause = "strName, strLastName";
		
		// Get Session for center and query member table
		return DBUtilStatic.getRecordsFromCenter(objEPMV, strTableName, strSelectList, strWhereClause, strSortClause);
		
		
		
		
	}
	
	private static boolean isRewardEnabledForCenter(EntityPlanMasterView objEPMV) {
		String strTableName = IUMCConstants.BN_GLOBAL_SETTING;
		String strSelectList = "";
		String  strWhereClause = IUMCConstants.GET_ACTIVE_ROWS;
		String strSortClause = "strName, strLastName";
		
		GlobalSettingBean objGSB = (GlobalSettingBean)	DBUtilStatic.getRecordFromCenter(objEPMV, strTableName, strSelectList, strWhereClause, "");
		
		if( null!=objGSB && objGSB.getIntIsRewards()==1 )
		{
			return true;
		}
		else
		{
		return false;
		}
	}
	
	public static String getTimeZone(EntityPlanMasterView objEPMV)
	{
	GlobalSettingBean objGSB = (GlobalSettingBean)	DBUtilStatic.getRecordFromCenter(objEPMV, IUMCConstants.BN_GLOBAL_SETTING, "", IUMCConstants.GET_ACTIVE_ROWS, "");
			
			if( null!=objGSB )
			{
				//To get the selected time zone of different countries
				 List lstTimeZone=DBUtilStatic.getRecordsFromCenter(objEPMV, IUMCConstants.BN_TIME_ZONE, "", IUMCConstants.GET_ACTIVE_ROWS, "");
		         
		         
		         if(null!=lstTimeZone)
		         {
		        	 
		        	 for(int i=0;i<lstTimeZone.size();i++)
		        	 {
		        	    TimeZoneConfigurationBean objTZCB=(TimeZoneConfigurationBean)lstTimeZone.get(i);
		        	    if(null!=objTZCB && objTZCB.getLngID()==objGSB.getLngTimeZone())
		        	    {
		        	    	if(null!=objTZCB.getStrTimeZone())
		        	    	{
		        	    		strTimeGMT=objTZCB.getStrTimeZone();
		        	    	}
		        	    }
		        	 }
		         }
				//Getting  the time zone of different countries ends
			}
			
			return strTimeGMT;
	}
	
	
	//Replace the tokens and send message for bday and aniversary and absent members
	public static void sendMessage(MemberMasterBean objMMB, SMSVariableCodeBean objSMSVar,EntityPlanMasterView objEPMV,long lngMessageType){
		
		boolean blnExecuted = false;
		
		 String strWhereSMSTemplate=IUMCConstants.GET_ACTIVE_ROWS+" and lngMsgTypeID = "+lngMessageType+" and intSend = 1 and lngCompanyID ="+objMMB.getLngCompanyID();
		   SMSTemplateNotificationBean objSMSTNB=(SMSTemplateNotificationBean)DBUtilStatic.getRecordFromCenter(objEPMV, IUMCConstants.BN_SMS_TEMP_NOTIFICATION, "", strWhereSMSTemplate, "");
		   
		   String strWhereCompany=IUMCConstants.GET_ACTIVE_ROWS+" and lngID = "+objMMB.getLngCompanyID();
		   CompanyMasterBean objCMB=(CompanyMasterBean)DBUtilStatic.getRecordFromCenter(objEPMV, IUMCConstants.BN_COMPANY_MASTER_BEAN, "", strWhereCompany, "");
		  
		   
		   
		   
		   
		   //look
		   StringBuilder builder = new StringBuilder();
		   String strSMSAPI = "";
			 if(null!=objSMSVar && objSMSVar.getStrName().trim().length()>0)
			 {
			 strSMSAPI=objSMSVar.getStrName();
			 }
			 String strSMSAPIForCust = strSMSAPI ;
			 URL url = null; 
			 HttpURLConnection uc;
			 //look
		   if(null!=objSMSTNB)
		   {
			   //look
		         String strContent="";
		         String strSpecificContent = "";
			     strContent =objSMSTNB.getStrContent();
				 strContent = strContent.replaceAll(" ", "%20");
				 strSpecificContent=strContent;
				 
				 //To get the Admin Name of the center
				 String strWhereStaff= IUMCConstants.GET_ACTIVE_ROWS+" and lngID = "+objCMB.getLngAdminID();
				 StaffMasterBean objAdminStaff=(StaffMasterBean) DBUtilStatic.getRecordFromCenter(objEPMV, IUMCConstants.BN_STAFF_MASTER_BEAN, "", strWhereStaff, "");
				 String strAdminName="";
				 if(null!=objAdminStaff && objAdminStaff.getStrName().trim().length()>0)
				 {
					 strAdminName=objAdminStaff.getStrName()+" "+objAdminStaff.getStrLastName();
				 }
			   //look
			   
       // replace the sms template with actual text : replacing tokens to actual values
			   
				
		 			 String strName=objMMB.getStrName()+" "+objMMB.getStrLastName();
		 			 
		 			 if(null!=strName && strName.trim().length()>0)
		 			 {
		 			    strSpecificContent=strSpecificContent.replace(IUMCConstants.ST_CUST_NAME, strName);
		 			 }
		 			 else
		 			 {
		 				strSpecificContent=strSpecificContent.replace(IUMCConstants.ST_CUST_NAME,"");
		 			 }
		 			    if(null!=objMMB.getStrAddress() &&  objMMB.getStrAddress().trim().length()>0)
		 			    {
		 			    strSpecificContent=strSpecificContent.replace(IUMCConstants.ST_CUST_ADDRESS, objMMB.getStrAddress());
		 			    }
		 			    else
		 			    {
		 			    	strSpecificContent=strSpecificContent.replace(IUMCConstants.ST_CUST_ADDRESS, "");
		 			    }
		 			    
		 			   if(null!=objMMB.getStrLandlineNumber() &&  objMMB.getStrLandlineNumber().trim().length()>0)
		 			    {
		 			    strSpecificContent=strSpecificContent.replace(IUMCConstants.ST_CUST_PH_NO, objMMB.getStrLandlineNumber());
		 			    }
		 			   else
		 			   {
		 				  strSpecificContent=strSpecificContent.replace(IUMCConstants.ST_CUST_PH_NO, "");
		 			   }
		 			  if(null!=objMMB.getStrMobileNumber() &&  objMMB.getStrMobileNumber().trim().length()>0)
		 			  {
		 			    strSpecificContent=strSpecificContent.replace(IUMCConstants.ST_CUST_MOB_NO, objMMB.getStrMobileNumber());
		 			  }
		 			  else
		 			  {
		 				 strSpecificContent=strSpecificContent.replace(IUMCConstants.ST_CUST_MOB_NO,"");
		 			  }
		 			 if(null!=objMMB.getStrEmailID() &&  objMMB.getStrEmailID().trim().length()>0)
		 			  {
			 			strSpecificContent=strSpecificContent.replace(IUMCConstants.ST_CUST_EMAIL, objMMB.getStrEmailID());
		 			  }
		 			 else
		 			 {
		 				strSpecificContent=strSpecificContent.replace(IUMCConstants.ST_CUST_EMAIL,"");
		 			 }
		 			 if(null!=objMMB.getDtDOB())
		 			  {
			 			strSpecificContent=strSpecificContent.replace(IUMCConstants.ST_DOB, objMMB.getDtDOB()+"");
		 			  }	
		 			 else
		 			 {
		 				strSpecificContent=strSpecificContent.replace(IUMCConstants.ST_DOB,"");
		 			 }
		 			if(null!=objMMB.getDtDOM() )
		 			  {
			 			strSpecificContent=strSpecificContent.replace(IUMCConstants.ST_DOM, objMMB.getDtDOM()+"");
		 			  }
		 			else
		 			{
		 				strSpecificContent=strSpecificContent.replace(IUMCConstants.ST_DOM,"");
		 			}
		 			 if(null!=objCMB.getStrName() &&  objCMB.getStrName().trim().length()>0)
		 			  {
			 			strSpecificContent=strSpecificContent.replace(IUMCConstants.ST_CENTER_NAME, objCMB.getStrName());
		 			  }	
		 			 else
		 			 {
		 				strSpecificContent=strSpecificContent.replace(IUMCConstants.ST_CENTER_NAME, "");
		 			 }
		 			if(null!=objCMB.getStrPrimaryMobileNumber() &&  objCMB.getStrPrimaryMobileNumber().trim().length()>0)
		 			  {
			 			strSpecificContent=strSpecificContent.replace(IUMCConstants.ST_CENTER_NO, objCMB.getStrPrimaryMobileNumber());
		 			  }
		 			else
		 			{
		 				strSpecificContent=strSpecificContent.replace(IUMCConstants.ST_CENTER_NO,"");
		 			}
		 			if(null!=objCMB.getStrAddress() &&  objCMB.getStrAddress().trim().length()>0)
		 			  {
			 			strSpecificContent=strSpecificContent.replace(IUMCConstants.ST_CENTER_ADDRESS, objCMB.getStrAddress());
		 			  }
		 			else
		 			{
		 				strSpecificContent=strSpecificContent.replace(IUMCConstants.ST_CENTER_ADDRESS,"");
		 			}
		 			if(null!=strAdminName &&  strAdminName.trim().length()>0)
		 			  {
			 			strSpecificContent=strSpecificContent.replace(IUMCConstants.ST_CENTER_ADMIN, strAdminName);
		 			  }
		 			else
		 			{
		 				strSpecificContent=strSpecificContent.replace(IUMCConstants.ST_CENTER_ADMIN, "");
		 			}
		 			if(null!=objCMB.getStrContactEmail() &&  objCMB.getStrContactEmail().trim().length()>0)
		 			  {
			 			strSpecificContent=strSpecificContent.replace(IUMCConstants.ST_CENTER_EMAIL, objCMB.getStrContactEmail());
		 			  }
		 			else
		 			{
		 				strSpecificContent=strSpecificContent.replace(IUMCConstants.ST_CENTER_EMAIL, "");
		 			}
		 			
		 			 
		 			 
					 strSpecificContent = strSpecificContent.replaceAll(" ", "%20");
					 strSMSAPIForCust=strSMSAPIForCust.replace(IUMCConstants.ST_SEND_TO,objMMB.getStrMobileNumber());
					 strSMSAPIForCust = strSMSAPIForCust.replace(IUMCConstants.ST_SMS_TEXT, builder.append(strSpecificContent).toString());
			           
					 System.out.print(strSMSAPIForCust);
						try {
							url = new URL(strSMSAPIForCust);
							System.out.println(strSMSAPIForCust);
							uc = (HttpURLConnection)url.openConnection();
							//uc.getResponseMessage();
							int responseCode = uc.getResponseCode();
							
							System.out.println("GET Response Code :: " + responseCode);
							
							if (responseCode == HttpURLConnection.HTTP_OK) { // success
								BufferedReader in = new BufferedReader(new InputStreamReader(
										uc.getInputStream()));
								String inputLine;
								StringBuffer response = new StringBuffer();

								while ((inputLine = in.readLine()) != null) {
									response.append(inputLine);
								}
								in.close();

								// print result
								System.out.println(response.toString());
							} else {
								System.out.println("GET request not worked");
							}
						    uc.disconnect();
						    System.out.print(strSMSAPIForCust);
						    blnExecuted= true;
						} catch (MalformedURLException e) {
							// TODO Auto-generated catch block
							e.printStackTrace();
							blnExecuted= false;
						}
						catch (IOException e) {
					// TODO Auto-generated catch block
							e.printStackTrace();
							blnExecuted= false;
						}
		   }
	}
	
	
	
	//Here we update the Member and log transaction bean
	public static void updateBDayAniversaryAttendRewardPoint(MemberMasterBean objMMB, EntityPlanMasterView objEPMV, RewardPointsConfigurationBean objRPCB,long lngReasonType){
		boolean blnUpdatedReward=false;
			try {
				blnUpdatedReward=DBUtilStatic.updateRecordsOfCenter(objEPMV, IUMCConstants.BN_MEMBER_MASTER_BEAN, "dblRewardPoints =Round(dblRewardPoints +"+objRPCB.getDblAnivBDayPoints()+",2)",IUMCConstants.GET_ACTIVE_ROWS+" and lngID = "+objMMB.getLngID());
			
			//Get the Active financial year
			FinancialYearMasterBean objFYMB =(FinancialYearMasterBean)DBUtilStatic.getRecordFromCenter(objEPMV, IUMCConstants.BN_FINANCIAL_YEAR_MASTER_BEAN, "", IUMCConstants.GET_ACTIVE_ROWS+" and intIsActive = 1", "");
			 if(null==objFYMB)
			 {
				 objFYMB=new FinancialYearMasterBean();
			 }
			//Here we create a logg for adding the points
	    	LoggTransactionBean objLTB=new LoggTransactionBean();
	    	objLTB.setLngLoggReasonID(lngReasonType);
	    	objLTB.setLngRedemptionType(IUMCConstants.LNG_ADD_POINTS);
	    	objLTB.setLngMemID(objMMB.getLngID());
	    	objLTB.setDblPoints(objRPCB.getDblReferalPoints());
	    	//objLTB.setLngCreatedBy(1);
	    	//objLTB.setLngUpdatedBy(1);
	    	objLTB.setDtCreatedOn(WebUtil.getCurrentDate(strTimeGMT));
	    	objLTB.setDtUpdatedOn(WebUtil.getCurrentDate(strTimeGMT));
	    	objLTB.setLngFinancialYearID(objFYMB.getLngID());
	    	objLTB.setLngCompanyID(objMMB.getLngCompanyID());
	    	objLTB.setLngFormsRefID(1);
	    	objLTB.setIntStatus(IUMCConstants.STATUS_ACTIVE);
	    	boolean blnModified = DBUtilStatic.modifyRecord(objEPMV,objLTB);
		} 
    	catch (UMBException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
        
	}
	
	private static boolean sendPaymentDueMembershipRemainderToMembers(EntityPlanMasterView objEPMV) {
		// TODO Auto-generated method stub
		
		List lstPlanExpirySevenBfrMemberList=null;
		boolean blnSendToMemberShipDueAmt=false;
		 Calendar c = Calendar.getInstance();
		 GlobalSettingBean objGSB=(GlobalSettingBean) DBUtilStatic.getRecordFromCenter(objEPMV, IUMCConstants.BN_GLOBAL_SETTING, "", IUMCConstants.GET_ACTIVE_ROWS, "");
		 double dblMinBalanceAmt=0;
		 int intTotalSMS=0;
		 int intSMSInterval=0;
		 if(null!=objGSB){
			 dblMinBalanceAmt=objGSB.getDblMinBalanceRemainderAmt();
			 intTotalSMS=objGSB.getIntNoOfAlertSMS();
			 intSMSInterval=objGSB.getIntSMSIntervalDays();
		 }
		 
		 c.add(Calendar.DATE, intSMSInterval);  // number of days to add
		 String strIntervalDate= WebUtil.formatDate(c.getTime(), IUMCConstants.DB_DATE_FORMAT);
		 strTimeGMT =getTimeZone(objEPMV);
		 String  strMemExpWhereClause=IUMCConstants.GET_ACTIVE_ROWS+" and dblBalanceAmt > "+dblMinBalanceAmt+" and intSMSNotification = 1 and intIsCurrent = 1 and intSMSCount <="+intTotalSMS +" and (dtlastSMSDate like '"+intSMSInterval+"%' or dtlastSMSDate is null)";
		 lstPlanExpirySevenBfrMemberList=DBUtilStatic.getRecordsFromCenter(objEPMV, IUMCConstants.BN_PLAN_SUBS_MEM_NAME, "", strMemExpWhereClause, "");
		// Get SMS configuration
				 String strWhereSMS=IUMCConstants.GET_ACTIVE_ROWS+" and intSMSType = "+IUMCConstants.TRANSACTIONAL;
				 SMSVariableCodeBean objSMSVar=(SMSVariableCodeBean) DBUtilStatic.getRecordFromCenter(objEPMV, IUMCConstants.BN_SMS_VARIABLE_CODE_BEAN, "", strWhereSMS, "");
				 if(null==objSMSVar) return false;
		 
				//Fetching the members whoms balance is more than the given configuration balance amount from membership
				 PlanSubsPaidMemNameTransactionViewBean objPSPMN= new PlanSubsPaidMemNameTransactionViewBean();
				   if(null!=lstPlanExpirySevenBfrMemberList)
				   {
				     //for loop of the list of members
					 for(int i=0;i<lstPlanExpirySevenBfrMemberList.size();i++)
					 {
						 objPSPMN=new PlanSubsPaidMemNameTransactionViewBean(); 
						 objPSPMN = (PlanSubsPaidMemNameTransactionViewBean)lstPlanExpirySevenBfrMemberList.get(i);
						 
		                if(null!=objPSPMN)
		                {
				           //for each member get the appropriate sms template
		                	String strWhereSMSTemplate=IUMCConstants.GET_ACTIVE_ROWS+" and lngMsgTypeID = "+IUMCConstants.MEMBERSHIP_DUE+" and intSend = 1 and lngCompanyID= "+objPSPMN.getLngCompanyID();
		 				    SMSTemplateNotificationBean objSMSTNB=(SMSTemplateNotificationBean)DBUtilStatic.getRecordFromCenter(objEPMV, IUMCConstants.BN_SMS_TEMP_NOTIFICATION, "", strWhereSMSTemplate, "");
		 				   
		 				   String strWhereCompany=IUMCConstants.GET_ACTIVE_ROWS+" and lngID = "+objPSPMN.getLngCompanyID();
		 				   CompanyMasterBean objCMB=(CompanyMasterBean)DBUtilStatic.getRecordFromCenter(objEPMV, IUMCConstants.BN_COMPANY_MASTER_BEAN, "", strWhereCompany, "");
		                   
		 				  //look
						   StringBuilder builder = new StringBuilder();
						   String strSMSAPI = "";
							 if(null!=objSMSVar && objSMSVar.getStrName().trim().length()>0)
							 {
							 strSMSAPI=objSMSVar.getStrName();
							 }
							 String strSMSAPIForCust = strSMSAPI ;
							 URL url = null; 
							 HttpURLConnection uc;
							 //look
		 				    
							 if(null!=objSMSTNB)
							 {
								//look
						         String strContent="";
						         String strSpecificContent = "";
							     strContent =objSMSTNB.getStrContent();
								 strContent = strContent.replaceAll(" ", "%20");
								 strSpecificContent=strContent;
								 
								 //To get the Admin Name of the center
								 String strWhereStaff= IUMCConstants.GET_ACTIVE_ROWS+" and lngID = "+objCMB.getLngAdminID();
								 StaffMasterBean objAdminStaff=(StaffMasterBean) DBUtilStatic.getRecordFromCenter(objEPMV, IUMCConstants.BN_STAFF_MASTER_BEAN, "", strWhereStaff, "");
								 String strAdminName="";
								 if(null!=objAdminStaff && objAdminStaff.getStrName().trim().length()>0)
								 {
									 strAdminName=objAdminStaff.getStrName()+" "+objAdminStaff.getStrLastName();
								 }
							   //look
								 
								 
								 // replace the sms template with actual text : replacing tokens to actual values
								   
									
								 String strName=objPSPMN.getStrMemberName()+" "+objPSPMN.getStrMemberLastName();
						 			if(null!=strName && strName.trim().length()>0)
						 			 {
						 			    strSpecificContent=strSpecificContent.replace(IUMCConstants.ST_CUST_NAME, strName);
						 			 }
						 			 else
						 			 {
						 				strSpecificContent=strSpecificContent.replace(IUMCConstants.ST_CUST_NAME,"");
						 			 }
						 			    
						 			  strSpecificContent=strSpecificContent.replace(IUMCConstants.ST_CUST_ADDRESS, "");
						 			  strSpecificContent=strSpecificContent.replace(IUMCConstants.ST_CUST_PH_NO, "");
						 			   
						 			  if(null!=objPSPMN.getStrContactNo() &&  objPSPMN.getStrContactNo().trim().length()>0)
						 			  {
						 			    strSpecificContent=strSpecificContent.replace(IUMCConstants.ST_CUST_MOB_NO, objPSPMN.getStrContactNo());
						 			  }
						 			  else
						 			  {
						 				 strSpecificContent=strSpecificContent.replace(IUMCConstants.ST_CUST_MOB_NO,"");
						 			  }
						 			 if(null!=objPSPMN.getStrEmailID() &&  objPSPMN.getStrEmailID().trim().length()>0)
						 			  {
							 			strSpecificContent=strSpecificContent.replace(IUMCConstants.ST_CUST_EMAIL, objPSPMN.getStrEmailID());
						 			  }
						 			 else
						 			 {
						 				strSpecificContent=strSpecificContent.replace(IUMCConstants.ST_CUST_EMAIL,"");
						 			 }
						 			 
						 			strSpecificContent=strSpecificContent.replace(IUMCConstants.ST_DOB,"");
						 		    strSpecificContent=strSpecificContent.replace(IUMCConstants.ST_DOM,"");
						 			
						 		    if(objPSPMN.getDblBalanceAmt()>0){
						 		    	strSpecificContent=strSpecificContent.replace(IUMCConstants.ST_DOC_AMT, ""+objPSPMN.getDblBalanceAmt());
						 		    }else{
						 		    	strSpecificContent=strSpecificContent.replace(IUMCConstants.ST_DOC_AMT, "");
						 		    }
						 		    
						 			 if(null!=objCMB.getStrName() &&  objCMB.getStrName().trim().length()>0)
						 			  {
							 			strSpecificContent=strSpecificContent.replace(IUMCConstants.ST_CENTER_NAME, objCMB.getStrName());
						 			  }	
						 			 else
						 			 {
						 				strSpecificContent=strSpecificContent.replace(IUMCConstants.ST_CENTER_NAME, "");
						 			 }
						 			if(null!=objCMB.getStrPrimaryMobileNumber() &&  objCMB.getStrPrimaryMobileNumber().trim().length()>0)
						 			  {
							 			strSpecificContent=strSpecificContent.replace(IUMCConstants.ST_CENTER_NO, objCMB.getStrPrimaryMobileNumber());
						 			  }
						 			else
						 			{
						 				strSpecificContent=strSpecificContent.replace(IUMCConstants.ST_CENTER_NO,"");
						 			}
						 			if(null!=objCMB.getStrAddress() &&  objCMB.getStrAddress().trim().length()>0)
						 			  {
							 			strSpecificContent=strSpecificContent.replace(IUMCConstants.ST_CENTER_ADDRESS, objCMB.getStrAddress());
						 			  }
						 			else
						 			{
						 				strSpecificContent=strSpecificContent.replace(IUMCConstants.ST_CENTER_ADDRESS,"");
						 			}
						 			if(null!=strAdminName &&  strAdminName.trim().length()>0)
						 			  {
							 			strSpecificContent=strSpecificContent.replace(IUMCConstants.ST_CENTER_ADMIN, strAdminName);
						 			  }
						 			else
						 			{
						 				strSpecificContent=strSpecificContent.replace(IUMCConstants.ST_CENTER_ADMIN, "");
						 			}
						 			if(null!=objCMB.getStrContactEmail() &&  objCMB.getStrContactEmail().trim().length()>0)
						 			  {
							 			strSpecificContent=strSpecificContent.replace(IUMCConstants.ST_CENTER_EMAIL, objCMB.getStrContactEmail());
						 			  }
						 			else
						 			{
						 				strSpecificContent=strSpecificContent.replace(IUMCConstants.ST_CENTER_EMAIL, "");
						 			}
						 			 
						 			 
						 			 
						 			 strSpecificContent = strSpecificContent.replaceAll(" ", "%20");
									 strSMSAPIForCust=strSMSAPIForCust.replace(IUMCConstants.ST_SEND_TO,objPSPMN.getStrContactNo());
									 strSMSAPIForCust = strSMSAPIForCust.replace(IUMCConstants.ST_SMS_TEXT, builder.append(strSpecificContent).toString());
						         
								 System.out.print(strSMSAPIForCust);
							     try {
									url = new URL(strSMSAPIForCust);
									System.out.println(strSMSAPIForCust);
									uc = (HttpURLConnection)url.openConnection();
									uc.getResponseMessage();
								    uc.disconnect();
								    System.out.print(strSMSAPIForCust);
								    blnSendToMemberShipDueAmt= true;
								    
								    if(blnSendToMemberShipDueAmt==true)
								    {
								    	try {
								    		DBUtilStatic.updateRecordsOfCenter(objEPMV, IUMCConstants.BN_MEMBER_PLAN_SUBS_FROM_TO, "intSMSCount=intSMSCount+"+1+",dtlastSMSDate = '"+WebUtil.formatDate(WebUtil.getCurrentDate(strTimeGMT), IUMCConstants.DB_DATE_TIME_SEC_FORMAT)+"'", "lngID="+objPSPMN.getLngID());
											//new DBUtil(request).updateRecords(IUMCConstants.BN_MEMBER_PLAN_SUBS_FROM_TO, "intSMSCount=intSMSCount+"+1+",dtlastSMSDate = '"+WebUtil.formatDate(WebUtil.getCurrentDate(request), IUMCConstants.DB_DATE_TIME_SEC_FORMAT)+"'","lngID="+objPSPMN.getLngID());
										} catch (UMBException e) {
											// TODO Auto-generated catch block
											e.printStackTrace();
										}
								    }
								} catch (MalformedURLException e) {
									// TODO Auto-generated catch block
									e.printStackTrace();
									blnSendToMemberShipDueAmt= false;
								}
								catch (IOException e) {
							// TODO Auto-generated catch block
									e.printStackTrace();
									blnSendToMemberShipDueAmt= false;
								}
							 }
						 	 
							 
		                }
					 }
				  //End of loop
				   }
				 //End of Members with membership due amount
		
		 
		 return blnSendToMemberShipDueAmt;
	}
	
	
	private static boolean sendPaymentDueServiceRemainderToMembers(EntityPlanMasterView objEPMV) {
		// TODO Auto-generated method stub
		
		List lstPlanExpirySevenBfrMemberList=null;
		boolean blnSendToServiceDue=false;
		
		 Calendar c = Calendar.getInstance();
		 GlobalSettingBean objGSB=(GlobalSettingBean) DBUtilStatic.getRecordFromCenter(objEPMV, IUMCConstants.BN_GLOBAL_SETTING, "", IUMCConstants.GET_ACTIVE_ROWS, "");
		 double dblMinBalanceAmt=0;
		 int intTotalSMSCount=0;
		 int intSMSInterval=0;
		 if(null!=objGSB){
			 dblMinBalanceAmt=objGSB.getDblMinBalanceRemainderAmt();
			 intTotalSMSCount=objGSB.getIntNoOfAlertSMS();
			 intSMSInterval=objGSB.getIntSMSIntervalDays();
		 }
		
		 c.add(Calendar.DATE, intSMSInterval);  // number of days to add
		 String strIntervalDate= WebUtil.formatDate(c.getTime(), IUMCConstants.DB_DATE_FORMAT);
		//To get the selected time zone of different countries
	     strTimeGMT =getTimeZone(objEPMV);
	   //Getting  the time zone of different countries ends
		 
		 String  strMemExpWhereClause=IUMCConstants.GET_ACTIVE_ROWS+" and dblTotalBalance > "+dblMinBalanceAmt+" and dblPayableAmt >0 and intSMSNotification = 1  and intSMSCount <="+intTotalSMSCount+" and (dtlastSMSDate like '"+strIntervalDate+"%' or dtlastSMSDate is null)";
		 lstPlanExpirySevenBfrMemberList=DBUtilStatic.getRecordsFromCenter(objEPMV, IUMCConstants.BN_SERVICE_FORM_VIEW, "", strMemExpWhereClause, "");
		// Get SMS configuration
				 String strWhereSMS=IUMCConstants.GET_ACTIVE_ROWS+" and intSMSType = "+IUMCConstants.TRANSACTIONAL;
				 SMSVariableCodeBean objSMSVar=(SMSVariableCodeBean) DBUtilStatic.getRecordFromCenter(objEPMV, IUMCConstants.BN_SMS_VARIABLE_CODE_BEAN, "", strWhereSMS, "");
				 if(null==objSMSVar) return false;
		 
				//Members with service payment due than the given configuration amount
				 ServiceFormTransactionViewBean objPSPMN= new ServiceFormTransactionViewBean();
				   if(null!=lstPlanExpirySevenBfrMemberList)
				   {
				     //for loop of the list of members
					 for(int i=0;i<lstPlanExpirySevenBfrMemberList.size();i++)
					 {
						 objPSPMN=new ServiceFormTransactionViewBean(); 
						 objPSPMN = (ServiceFormTransactionViewBean)lstPlanExpirySevenBfrMemberList.get(i);
						 
		                if(null!=objPSPMN)
		                {
				           //for each member get the appropriate sms template
		                	String strWhereSMSTemplate=IUMCConstants.GET_ACTIVE_ROWS+" and lngMsgTypeID = "+IUMCConstants.SERVICE_DUE+" and intSend = 1 and lngCompanyID= "+objPSPMN.getLngCompanyID();
		 				    SMSTemplateNotificationBean objSMSTNB=(SMSTemplateNotificationBean)DBUtilStatic.getRecordFromCenter(objEPMV, IUMCConstants.BN_SMS_TEMP_NOTIFICATION, "", strWhereSMSTemplate, "");
		 				   
		 				   String strWhereCompany=IUMCConstants.GET_ACTIVE_ROWS+" and lngID = "+objPSPMN.getLngCompanyID();
		 				   CompanyMasterBean objCMB=(CompanyMasterBean)DBUtilStatic.getRecordFromCenter(objEPMV, IUMCConstants.BN_COMPANY_MASTER_BEAN, "", strWhereCompany, "");
		                   
		 				  //look
						   StringBuilder builder = new StringBuilder();
						   String strSMSAPI = "";
							 if(null!=objSMSVar && objSMSVar.getStrName().trim().length()>0)
							 {
							 strSMSAPI=objSMSVar.getStrName();
							 }
							 String strSMSAPIForCust = strSMSAPI ;
							 URL url = null; 
							 HttpURLConnection uc;
							 //look
		 				    
							 if(null!=objSMSTNB)
							 {
								//look
						         String strContent="";
						         String strSpecificContent = "";
							     strContent =objSMSTNB.getStrContent();
								 strContent = strContent.replaceAll(" ", "%20");
								 strSpecificContent=strContent;
								 
								 //To get the Admin Name of the center
								 String strWhereStaff= IUMCConstants.GET_ACTIVE_ROWS+" and lngID = "+objCMB.getLngAdminID();
								 StaffMasterBean objAdminStaff=(StaffMasterBean) DBUtilStatic.getRecordFromCenter(objEPMV, IUMCConstants.BN_STAFF_MASTER_BEAN, "", strWhereStaff, "");
								 String strAdminName="";
								 if(null!=objAdminStaff && objAdminStaff.getStrName().trim().length()>0)
								 {
									 strAdminName=objAdminStaff.getStrName()+" "+objAdminStaff.getStrLastName();
								 }
							   //look
								 
								 
								 // replace the sms template with actual text : replacing tokens to actual values
								   
									
								 String strName=objPSPMN.getStrMemberName();
						 			if(null!=strName && strName.trim().length()>0)
						 			 {
						 			    strSpecificContent=strSpecificContent.replace(IUMCConstants.ST_CUST_NAME, strName);
						 			 }
						 			 else
						 			 {
						 				strSpecificContent=strSpecificContent.replace(IUMCConstants.ST_CUST_NAME,"");
						 			 }
						 			    
						 			  strSpecificContent=strSpecificContent.replace(IUMCConstants.ST_CUST_ADDRESS, "");
						 			  strSpecificContent=strSpecificContent.replace(IUMCConstants.ST_CUST_PH_NO, "");
						 			   
						 			  if(null!=objPSPMN.getStrMemberMobNo() &&  objPSPMN.getStrMemberMobNo().trim().length()>0)
						 			  {
						 			    strSpecificContent=strSpecificContent.replace(IUMCConstants.ST_CUST_MOB_NO, objPSPMN.getStrMemberMobNo());
						 			  }
						 			  else
						 			  {
						 				 strSpecificContent=strSpecificContent.replace(IUMCConstants.ST_CUST_MOB_NO,"");
						 			  }
						 			 if(null!=objPSPMN.getStrMemberEmailID() &&  objPSPMN.getStrMemberEmailID().trim().length()>0)
						 			  {
							 			strSpecificContent=strSpecificContent.replace(IUMCConstants.ST_CUST_EMAIL, objPSPMN.getStrMemberEmailID());
						 			  }
						 			 else
						 			 {
						 				strSpecificContent=strSpecificContent.replace(IUMCConstants.ST_CUST_EMAIL,"");
						 			 }
						 			 
						 			strSpecificContent=strSpecificContent.replace(IUMCConstants.ST_DOB,"");
						 		    strSpecificContent=strSpecificContent.replace(IUMCConstants.ST_DOM,"");
						 			
						 		   if(objPSPMN.getDblTotalBalance() >0)
						 			  {
							 			strSpecificContent=strSpecificContent.replace(IUMCConstants.ST_DOC_AMT, objPSPMN.getDblTotalBalance()+"");
						 			  }
						 			 else
						 			 {
						 				strSpecificContent=strSpecificContent.replace(IUMCConstants.ST_DOC_AMT,"");
						 			 }
						 		   
						 		  if(objPSPMN.getLngReceiptID() >0)
					 			  {
						 			strSpecificContent=strSpecificContent.replace(IUMCConstants.ST_DOC_NO, objPSPMN.getLngReceiptID()+"");
					 			  }
					 			 else
					 			 {
					 				strSpecificContent=strSpecificContent.replace(IUMCConstants.ST_DOC_NO,"");
					 			 }
						 		  
						 		    
						 			 if(null!=objCMB.getStrName() &&  objCMB.getStrName().trim().length()>0)
						 			  {
							 			strSpecificContent=strSpecificContent.replace(IUMCConstants.ST_CENTER_NAME, objCMB.getStrName());
						 			  }	
						 			 else
						 			 {
						 				strSpecificContent=strSpecificContent.replace(IUMCConstants.ST_CENTER_NAME, "");
						 			 }
						 			if(null!=objCMB.getStrPrimaryMobileNumber() &&  objCMB.getStrPrimaryMobileNumber().trim().length()>0)
						 			  {
							 			strSpecificContent=strSpecificContent.replace(IUMCConstants.ST_CENTER_NO, objCMB.getStrPrimaryMobileNumber());
						 			  }
						 			else
						 			{
						 				strSpecificContent=strSpecificContent.replace(IUMCConstants.ST_CENTER_NO,"");
						 			}
						 			if(null!=objCMB.getStrAddress() &&  objCMB.getStrAddress().trim().length()>0)
						 			  {
							 			strSpecificContent=strSpecificContent.replace(IUMCConstants.ST_CENTER_ADDRESS, objCMB.getStrAddress());
						 			  }
						 			else
						 			{
						 				strSpecificContent=strSpecificContent.replace(IUMCConstants.ST_CENTER_ADDRESS,"");
						 			}
						 			if(null!=strAdminName &&  strAdminName.trim().length()>0)
						 			  {
							 			strSpecificContent=strSpecificContent.replace(IUMCConstants.ST_CENTER_ADMIN, strAdminName);
						 			  }
						 			else
						 			{
						 				strSpecificContent=strSpecificContent.replace(IUMCConstants.ST_CENTER_ADMIN, "");
						 			}
						 			if(null!=objCMB.getStrContactEmail() &&  objCMB.getStrContactEmail().trim().length()>0)
						 			  {
							 			strSpecificContent=strSpecificContent.replace(IUMCConstants.ST_CENTER_EMAIL, objCMB.getStrContactEmail());
						 			  }
						 			else
						 			{
						 				strSpecificContent=strSpecificContent.replace(IUMCConstants.ST_CENTER_EMAIL, "");
						 			}
						 			 
						 			 
						 			 
						 			 strSpecificContent = strSpecificContent.replaceAll(" ", "%20");
									 strSMSAPIForCust=strSMSAPIForCust.replace(IUMCConstants.ST_SEND_TO,objPSPMN.getStrMemberMobNo());
									 strSMSAPIForCust = strSMSAPIForCust.replace(IUMCConstants.ST_SMS_TEXT, builder.append(strSpecificContent).toString());
						         
								 System.out.print(strSMSAPIForCust);
							     try {
									url = new URL(strSMSAPIForCust);
									System.out.println(strSMSAPIForCust);
									uc = (HttpURLConnection)url.openConnection();
									uc.getResponseMessage();
								    uc.disconnect();
								    System.out.print(strSMSAPIForCust);
								    blnSendToServiceDue= true;
								     if(blnSendToServiceDue==true )
								    {
								    	try {
								    		
								    		DBUtilStatic.updateRecordsOfCenter(objEPMV, IUMCConstants.BN_SERVICE_FORM,  "intSMSCount=intSMSCount+"+1+",dtlastSMSDate = "+WebUtil.getCurrentDate(strTimeGMT), "lngID="+objPSPMN.getLngID());
								    		//DBUtilStatic.updateRecordsOfCenter(objEPMV,IUMCConstants.BN_SERVICE_FORM, "intSMSCount=intSMSCount+"+1+",dtlastSMSDate = "+WebUtil.getCurrentDate(strTimeGMT), IUMCConstants.DB_DATE_TIME_SEC_FORMAT)+"'", "lngID="+objPSPMN.getLngID());
											//new DBUtil(request).updateRecords(IUMCConstants.BN_SERVICE_FORM, "intSMSCount=intSMSCount+"+1+",dtlastSMSDate = "+WebUtil.getCurrentDate(request),"lngID="+objPDNB.getLngPlanRelatedID());
										} catch (UMBException e) {
											// TODO Auto-generated catch block
											e.printStackTrace();
										}
								    }
								    
								} catch (MalformedURLException e) {
									// TODO Auto-generated catch block
									e.printStackTrace();
									blnSendToServiceDue= false;
								}
								catch (IOException e) {
							// TODO Auto-generated catch block
									e.printStackTrace();
									blnSendToServiceDue= false;
								}
							 }
						 	 
							 
		                }
					 }
				  //End of loop
				   }
				 //End of Members with service due amount
		
		 
		 return blnSendToServiceDue;
	}
	
	
	
}
