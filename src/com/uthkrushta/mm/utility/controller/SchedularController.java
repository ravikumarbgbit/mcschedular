package com.uthkrushta.mm.utility.controller;

import java.io.IOException;
import java.net.HttpURLConnection;
import java.net.MalformedURLException;
import java.net.URL;
import java.text.ParseException;
import java.util.Date;
import java.util.List;

import javax.crypto.SecretKey;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.xml.bind.DatatypeConverter;

import org.apache.naming.factory.SendMailFactory;

import com.ibm.icu.text.SimpleDateFormat;
import com.ibm.icu.util.Calendar;
import com.uthkrushta.basis.bean.MessageBean;
import com.uthkrushta.basis.constants.ILabelConstants;
import com.uthkrushta.basis.constants.ITextConstants;
import com.uthkrushta.basis.constants.IUMCConstants;
import com.uthkrushta.mm.code.bean.MailVariableCodeBean;
import com.uthkrushta.mm.code.bean.SMSVariableCodeBean;
import com.uthkrushta.mm.configuration.bean.GlobalSettingBean;
import com.uthkrushta.mm.configuration.bean.TimeZoneConfigurationBean;
import com.uthkrushta.mm.gym.bean.AttendanceGymBean;
import com.uthkrushta.mm.master.bean.CompanyMasterBean;
import com.uthkrushta.mm.master.bean.MemberMasterBean;
import com.uthkrushta.mm.master.bean.StaffMasterBean;
import com.uthkrushta.mm.master.bean.StaffRoleMasterBeanView;
import com.uthkrushta.mm.supreme.control.EntityPlanMasterView;
import com.uthkrushta.mm.transaction.bean.MemberPaymentTransactionBean;
import com.uthkrushta.mm.utility.bean.SMSTemplateNotificationBean;
import com.uthkrushta.utils.DBUtil;
import com.uthkrushta.utils.DBUtilStatic;
import com.uthkrushta.utils.SendMailToCenter;
import com.uthkrushta.utils.UMBException;
import com.uthkrushta.utils.WebUtil;

public class SchedularController {
	GlobalSettingBean objGSB =null;
	public static String strTimeGMT="";
	
	public static boolean delegateCall(EntityPlanMasterView objEPMV){
		boolean blnExecuted = false;
		boolean blnSendExpiryReminder=false;
		boolean blnSendAbsenceMembersList=false;
		
		/*//Get the end Day of the week 
		Date dtCurrent = WebUtil.getCurrentDate(request);
		Calendar c = Calendar.getInstance();
		c.setTime(dtCurrent);
		int dayOfWeek = c.get(Calendar.DAY_OF_WEEK) - c.getFirstDayOfWeek();
		c.add(Calendar.DAY_OF_MONTH, -dayOfWeek);
		c.add(Calendar.DAY_OF_MONTH, 7); 
		Date weekEnd = c.getTime();
		//Ends
*/		
		strTimeGMT=getTimeZone(objEPMV);
		Date dtCurrent = WebUtil.getCurrentDate(strTimeGMT);
		SimpleDateFormat sdfTime =new SimpleDateFormat("HH:mm:ss");
		String strCurrentTime=sdfTime.format(dtCurrent);
		String strGivenStartTime="21:00:00";
		String strGivenEndTime="23:59:59";
		Date dtCurrentTime=null;
		Date dtGivenStartTime=null;
		Date dtGivenEndTime=null;
		try {
			 dtCurrentTime=sdfTime.parse(strCurrentTime);
			 dtGivenStartTime=sdfTime.parse(strGivenStartTime);
			 dtGivenEndTime=sdfTime.parse(strGivenEndTime);
			 if(dtCurrentTime.compareTo(dtGivenStartTime) >= 0 && dtCurrentTime.compareTo(dtGivenEndTime) <= 0)
			 {
				// If the center has SMS pack
				 if(objEPMV.getIntIsTransactionalSMS() == 1){
					//Working blnExecuted = sendSMSToAdmin(objEPMV);
					 blnExecuted =sendSMSToAdminDailySummary(objEPMV);
				 }
			 }
		} catch (ParseException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		
		//checking whether center has enabled email
		
		
		if(null!=objEPMV && objEPMV.getIntIsMail()==1 && isEmailEnabledForCenter(objEPMV))
		{
			sendPendingFeedbackMemberMail(objEPMV);
		}
		
		
		//isAutoLogout is center has enabled SMS notification
		if(!isAutoLogoutEnabledForCenter(objEPMV)) return true;
		
		blnExecuted =autoLogout(objEPMV);
		
		return blnExecuted;
	}
	
	private static boolean isAutoLogoutEnabledForCenter(EntityPlanMasterView objEPMV) {
		String strTableName = IUMCConstants.BN_GLOBAL_SETTING;
		String strSelectList = "";
		String  strWhereClause = IUMCConstants.GET_ACTIVE_ROWS;
		String strSortClause = "strName, strLastName";
		
		GlobalSettingBean objGSB = (GlobalSettingBean)	DBUtilStatic.getRecordFromCenter(objEPMV, strTableName, strSelectList, strWhereClause, "");
		
		if( null!=objGSB && objGSB.getIntAutoLogout()==1 )
		{
			return true;
		}
		else
		{
		return false;
		}
	}
	
	private static boolean autoLogout(EntityPlanMasterView objEPMV)
	{
		//List all members with logindatetime-3 and is_logged=1 and logg_type=1 
          List lstAttendanceMembers=null;
          boolean blnUpdated=false;
          
          String strTableName = IUMCConstants.BN_GLOBAL_SETTING;
  		String strSelectList = "";
  		String  strWhereClause = IUMCConstants.GET_ACTIVE_ROWS;
  		String strSortClause = "strName, strLastName";
  		
  		GlobalSettingBean objGSB = (GlobalSettingBean)	DBUtilStatic.getRecordFromCenter(objEPMV, strTableName, strSelectList, strWhereClause, "");
  		int hour=0;
          
          if(null!=objGSB)
          {
        	  hour=objGSB.getIntAutoLogoutTime(); 
          }
          
          //Getting the time zone of the country
          strTimeGMT=getTimeZone(objEPMV);
          //Getting time zone ends
          
        //Subtracting the time
          Calendar cal = Calendar.getInstance();
          cal.setTime(WebUtil.getCurrentDate(strTimeGMT));
          cal.add(Calendar.HOUR, -hour);
          Date dtThreeHourBack = cal.getTime();
          SimpleDateFormat sdf=new SimpleDateFormat("yyyy-MM-dd"); 
         /* SimpleDateFormat sdfTime=new SimpleDateFormat("yyyy-MM-dd HH:mm:ss"); */
          String strCurrDate=sdf.format(WebUtil.getCurrentDate(strTimeGMT));
          
          /*String strThreeHourBack=sdfTime.format(dtThreeHourBack);*/
          Date dtCurrDate=null;
          
          
          String strBDWhereClause=IUMCConstants.GET_ACTIVE_ROWS+" and intLoggTypeID = "+IUMCConstants.INT_LOGGED_MEMBER+" and intIsLogged = "+IUMCConstants.INT_CURRENTLY_LOGGED_IN+" and dtLoginDate <= '"+WebUtil.formatDate(dtThreeHourBack, IUMCConstants.DB_DATE_TIME_SEC_FORMAT)+"' " ;//and dtLoginDate like '"+strCurrDate+"%'
          lstAttendanceMembers=DBUtilStatic.getRecordsFromCenter(objEPMV, IUMCConstants.BN_ATTENDANCE_GYM_BEAN, "", strBDWhereClause, "");
          
          
          
          if(null!=lstAttendanceMembers)
		 {
        	  String strAttendanceID="";
			 for(int i=0;i<lstAttendanceMembers.size();i++)
			 {
				 AttendanceGymBean objAGB=new AttendanceGymBean();
				 objAGB=(AttendanceGymBean) lstAttendanceMembers.get(i);
				 if(null!=objAGB)
				 {
					 strAttendanceID=objAGB.getLngID()+",";
				 }
			 }
			 if(null!=strAttendanceID && strAttendanceID.trim().length()!=0)
			 {
				 strAttendanceID=strAttendanceID.substring(0, strAttendanceID.length()-1);
			 }
			 String strUpdateClause="dtLogoutDate = '"+WebUtil.formatDate(WebUtil.getCurrentDate(strTimeGMT), IUMCConstants.DB_DATE_TIME_SEC_FORMAT)+"', intIsLogged = 0 , intIsAutoLogout = 1";
			 String strWhereClauseAttendance="lngID in ("+strAttendanceID+") and intLoggTypeID = "+IUMCConstants.INT_LOGGED_MEMBER;
			 try {
				 blnUpdated=DBUtilStatic.updateRecordsOfCenter(objEPMV, IUMCConstants.BN_ATTENDANCE_GYM_BEAN, strUpdateClause, strWhereClauseAttendance);
			} catch (UMBException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
		 }
		
		
		return blnUpdated;
		
	}
	
	public static boolean sendSMSToAdmin(EntityPlanMasterView objEPMV)
	{
		boolean blnSent=false;
		String strWhereClause=IUMCConstants.GET_ACTIVE_ROWS+" and lngRoleTypeID = "+IUMCConstants.INT_ADMIN;
		
		//Getting the time zone of the country
		strTimeGMT=getTimeZone(objEPMV);
		//End of getting time zone of the country
		
		//Get the admin 
		StaffRoleMasterBeanView objSRMBV= new StaffRoleMasterBeanView();
		objSRMBV=(StaffRoleMasterBeanView)DBUtilStatic.getRecordFromCenter(objEPMV, IUMCConstants.BN_STAFF_ROLE_MASTER_BEAN_VIEW, "", strWhereClause, "");
		
		//Get the sum of payment
		MemberPaymentTransactionBean objMPTB=new MemberPaymentTransactionBean();
		double dblPayment=DBUtilStatic.getDoubleFromCenter(objEPMV, IUMCConstants.BN_MEMBER_PAYMENT_TRANSACTION, "sum(dbPayingAmount)", IUMCConstants.GET_ACTIVE_ROWS+" and dtReceiptDate = '"+WebUtil.formatDate(WebUtil.getCurrentDate(strTimeGMT), IUMCConstants.DB_DATE_FORMAT)+"'", "");
		
		// Get SMS configuration
				 String strWhereSMS=IUMCConstants.GET_ACTIVE_ROWS+" and intSMSType = "+IUMCConstants.TRANSACTIONAL;
				 SMSVariableCodeBean objSMSVar=(SMSVariableCodeBean) DBUtilStatic.getRecordFromCenter(objEPMV, IUMCConstants.BN_SMS_VARIABLE_CODE_BEAN, "", strWhereSMS, "");
				 if(null==objSMSVar) return false;
		
		if(null!=objSRMBV)
		{
		    
			sendMessageToAdmin(objSMSVar,objSRMBV,objEPMV);
			
		  String strWhereSMSTemplate=IUMCConstants.GET_ACTIVE_ROWS+" and lngMsgTypeID = "+IUMCConstants.PAYMENT_SUMMARY+" and intSend = 1 and lngCompanyID ="+objSRMBV.getLngCompanyID();
		   SMSTemplateNotificationBean objSMSTNB=(SMSTemplateNotificationBean)DBUtilStatic.getRecordFromCenter(objEPMV, IUMCConstants.BN_SMS_TEMP_NOTIFICATION, "", strWhereSMSTemplate, "");
		   
		   String strWhereCompany=IUMCConstants.GET_ACTIVE_ROWS+" and lngID = "+objSRMBV.getLngCompanyID();
		   CompanyMasterBean objCMB=(CompanyMasterBean)DBUtilStatic.getRecordFromCenter(objEPMV, IUMCConstants.BN_COMPANY_MASTER_BEAN, "", strWhereCompany, "");
		  
		   //look
		   StringBuilder builder = new StringBuilder();
		   String strSMSAPI = "";
			 if(null!=objSMSVar && objSMSVar.getStrName().trim().length()>0)
			 {
			 strSMSAPI=objSMSVar.getStrName();
			 }
			 String strSMSAPIForCust = strSMSAPI ;
			 URL url = null; 
			 HttpURLConnection uc;
			 //look
		   if(null!=objSMSTNB)
		   {
			   //look
		         String strContent="";
		         String strSpecificContent = "";
			     strContent =objSMSTNB.getStrContent();
				 strContent = strContent.replaceAll(" ", "%20");
				 strSpecificContent=strContent;
				 
				 //To get the Admin Name of the center
				 String strWhereStaff= IUMCConstants.GET_ACTIVE_ROWS+" and lngID = "+objCMB.getLngAdminID();
				 StaffMasterBean objAdminStaff=(StaffMasterBean) DBUtilStatic.getRecordFromCenter(objEPMV, IUMCConstants.BN_STAFF_MASTER_BEAN, "", strWhereStaff, "");
				 String strAdminName="";
				 if(null!=objAdminStaff && objAdminStaff.getStrName().trim().length()>0)
				 {
					 strAdminName=objAdminStaff.getStrName()+" "+objAdminStaff.getStrLastName();
				 }
			   //look
			   
      // replace the sms template with actual text : replacing tokens to actual values
			   
				
		 			 String strName=objSRMBV.getStrName()+" "+objSRMBV.getStrLastName();
		 			 
		 			 if(null!=strName && strName.trim().length()>0)
		 			 {
		 			    strSpecificContent=strSpecificContent.replace(IUMCConstants.ST_CUST_NAME, strName);
		 			 }
		 			 else
		 			 {
		 				strSpecificContent=strSpecificContent.replace(IUMCConstants.ST_CUST_NAME,"");
		 			 }
		 			    if(null!=objSRMBV.getStrAddress() &&  objSRMBV.getStrAddress().trim().length()>0)
		 			    {
		 			    strSpecificContent=strSpecificContent.replace(IUMCConstants.ST_CUST_ADDRESS, objSRMBV.getStrAddress());
		 			    }
		 			    else
		 			    {
		 			    	strSpecificContent=strSpecificContent.replace(IUMCConstants.ST_CUST_ADDRESS, "");
		 			    }
		 			    
		 			   if(null!=objSRMBV.getStrLandlineNumber() &&  objSRMBV.getStrLandlineNumber().trim().length()>0)
		 			    {
		 			    strSpecificContent=strSpecificContent.replace(IUMCConstants.ST_CUST_PH_NO, objSRMBV.getStrLandlineNumber());
		 			    }
		 			   else
		 			   {
		 				  strSpecificContent=strSpecificContent.replace(IUMCConstants.ST_CUST_PH_NO, "");
		 			   }
		 			  if(null!=objSRMBV.getStrMobileNumber() &&  objSRMBV.getStrMobileNumber().trim().length()>0)
		 			  {
		 			    strSpecificContent=strSpecificContent.replace(IUMCConstants.ST_CUST_MOB_NO, objSRMBV.getStrMobileNumber());
		 			  }
		 			  else
		 			  {
		 				 strSpecificContent=strSpecificContent.replace(IUMCConstants.ST_CUST_MOB_NO,"");
		 			  }
		 			 if(null!=objSRMBV.getStrEmail() &&  objSRMBV.getStrEmail().trim().length()>0)
		 			  {
			 			strSpecificContent=strSpecificContent.replace(IUMCConstants.ST_CUST_EMAIL, objSRMBV.getStrEmail());
		 			  }
		 			 else
		 			 {
		 				strSpecificContent=strSpecificContent.replace(IUMCConstants.ST_CUST_EMAIL,"");
		 			 }
		 			 if(null!=objSRMBV.getDtDOB())
		 			  {
			 			strSpecificContent=strSpecificContent.replace(IUMCConstants.ST_DOB, objSRMBV.getDtDOB()+"");
		 			  }	
		 			 else
		 			 {
		 				strSpecificContent=strSpecificContent.replace(IUMCConstants.ST_DOB,"");
		 			 }
		 			if(null!=objSRMBV.getDtDOM() )
		 			  {
			 			strSpecificContent=strSpecificContent.replace(IUMCConstants.ST_DOM, objSRMBV.getDtDOM()+"");
		 			  }
		 			else
		 			{
		 				strSpecificContent=strSpecificContent.replace(IUMCConstants.ST_DOM,"");
		 			}
		 			
		 			//Added for Payment Summary
		 			
		 			strSpecificContent=strSpecificContent.replace(IUMCConstants.ST_PAYMENT_COLLECTED, dblPayment+"");
		 			
		 			 if(null!=objCMB.getStrName() &&  objCMB.getStrName().trim().length()>0)
		 			  {
			 			strSpecificContent=strSpecificContent.replace(IUMCConstants.ST_CENTER_NAME, objCMB.getStrName());
		 			  }	
		 			 else
		 			 {
		 				strSpecificContent=strSpecificContent.replace(IUMCConstants.ST_CENTER_NAME, "");
		 			 }
		 			if(null!=objCMB.getStrPrimaryMobileNumber() &&  objCMB.getStrPrimaryMobileNumber().trim().length()>0)
		 			  {
			 			strSpecificContent=strSpecificContent.replace(IUMCConstants.ST_CENTER_NO, objCMB.getStrPrimaryMobileNumber());
		 			  }
		 			else
		 			{
		 				strSpecificContent=strSpecificContent.replace(IUMCConstants.ST_CENTER_NO,"");
		 			}
		 			if(null!=objCMB.getStrAddress() &&  objCMB.getStrAddress().trim().length()>0)
		 			  {
			 			strSpecificContent=strSpecificContent.replace(IUMCConstants.ST_CENTER_ADDRESS, objCMB.getStrAddress());
		 			  }
		 			else
		 			{
		 				strSpecificContent=strSpecificContent.replace(IUMCConstants.ST_CENTER_ADDRESS,"");
		 			}
		 			if(null!=strAdminName &&  strAdminName.trim().length()>0)
		 			  {
			 			strSpecificContent=strSpecificContent.replace(IUMCConstants.ST_CENTER_ADMIN, strAdminName);
		 			  }
		 			else
		 			{
		 				strSpecificContent=strSpecificContent.replace(IUMCConstants.ST_CENTER_ADMIN, "");
		 			}
		 			if(null!=objCMB.getStrContactEmail() &&  objCMB.getStrContactEmail().trim().length()>0)
		 			  {
			 			strSpecificContent=strSpecificContent.replace(IUMCConstants.ST_CENTER_EMAIL, objCMB.getStrContactEmail());
		 			  }
		 			else
		 			{
		 				strSpecificContent=strSpecificContent.replace(IUMCConstants.ST_CENTER_EMAIL, "");
		 			}
		 			
		 			 
		 			 
					 strSpecificContent = strSpecificContent.replaceAll(" ", "%20");
					 strSMSAPIForCust=strSMSAPIForCust.replace(IUMCConstants.ST_SEND_TO,objSRMBV.getStrMobileNumber());
					 strSMSAPIForCust = strSMSAPIForCust.replace(IUMCConstants.ST_SMS_TEXT, builder.append(strSpecificContent).toString());
			           
					 System.out.print(strSMSAPIForCust);
						try {
							url = new URL(strSMSAPIForCust);
							System.out.println(strSMSAPIForCust);
							uc = (HttpURLConnection)url.openConnection();
							uc.getResponseMessage();
						    uc.disconnect();
						    System.out.print(strSMSAPIForCust);
						    blnSent= true;
						} catch (MalformedURLException e) {
							// TODO Auto-generated catch block
							e.printStackTrace();
							blnSent= false;
						}
						catch (IOException e) {
					// TODO Auto-generated catch block
							e.printStackTrace();
							blnSent= false;
						}
				 
		   }
		}
		
		return blnSent;
		
	}
	
	public static boolean sendSMSToAdminDailySummary(EntityPlanMasterView objEPMV)
	{
		boolean blnSent=false;
		String strWhereClause=IUMCConstants.GET_ACTIVE_ROWS+" and lngRoleTypeID = "+IUMCConstants.INT_ADMIN;
		
		//Getting the time zone of the country
		strTimeGMT=getTimeZone(objEPMV);
		//End of getting time zone of the country
		
		//Get the admin 
		StaffRoleMasterBeanView objSRMBV= new StaffRoleMasterBeanView();
		objSRMBV=(StaffRoleMasterBeanView)DBUtilStatic.getRecordFromCenter(objEPMV, IUMCConstants.BN_STAFF_ROLE_MASTER_BEAN_VIEW, "", strWhereClause, "");
		
		//Get the sum of payment
		MemberPaymentTransactionBean objMPTB=new MemberPaymentTransactionBean();
		double dblPayment=DBUtilStatic.getDoubleFromCenter(objEPMV, IUMCConstants.BN_MEMBER_PAYMENT_TRANSACTION, "sum(dbPayingAmount)", IUMCConstants.GET_ACTIVE_ROWS+" and dtReceiptDate = '"+WebUtil.formatDate(WebUtil.getCurrentDate(strTimeGMT), IUMCConstants.DB_DATE_FORMAT)+"'", "");
		
		//Get Newly joined members
		Long LngNew=(Long)DBUtilStatic.getRecordFromCenter(objEPMV, IUMCConstants.BN_MEMBER_MASTER_BEAN_VIEW, "count(*)", IUMCConstants.GET_ACTIVE_ROWS+"  and dtDOJ = '"+WebUtil.formatDate(WebUtil.getCurrentDate(strTimeGMT), IUMCConstants.DB_DATE_FORMAT)+"'", "");
		long lngNew=0;
		if(null!=LngNew)
		{
			lngNew=(LngNew).longValue();
		}
		
		//Get Enquiry Members
		
		 String strWhereEnq=IUMCConstants.GET_ACTIVE_ROWS+"  dtEnquiryDate like '"+WebUtil.formatDate(WebUtil.getCurrentDate(strTimeGMT), IUMCConstants.DB_DATE_FORMAT)+"%'";
		 List  lstEnq=new DBUtilStatic().getRecordsFromCenter(objEPMV, IUMCConstants.BN_ENQUIRY_HEADER_TRANSACTION_BEAN, "", strWhereEnq, "");
		 long lngEnq=0;
		 
			if(null!=lstEnq && lstEnq.size()>0)
			{
				lngEnq=lstEnq.size();
			}
		
		//Attendance Member 
		
		 String strWhereAttendanceMem=IUMCConstants.GET_ACTIVE_ROWS+" and intLoggTypeID ="+IUMCConstants.LOGGED_IS_MEMBER +"  and dtLoginDate like '"+WebUtil.formatDate(WebUtil.getCurrentDate(strTimeGMT), IUMCConstants.DB_DATE_FORMAT)+"%' group by lngMemberID";
		 List  lstAttendMem=new DBUtilStatic().getRecordsFromCenter(objEPMV, IUMCConstants.BN_ATTENDANCE_GYM_BEAN, "", strWhereAttendanceMem, "");
		 long lngMemAttendance=0;
		 
			if(null!=lstAttendMem && lstAttendMem.size()>0)
			{
				lngMemAttendance=lstAttendMem.size();
			}
			 
		//Attendace Staff	
			String strWhereAttendanceStaff=IUMCConstants.GET_ACTIVE_ROWS+"  and intLoggTypeID ="+IUMCConstants.LOGGED_IS_STAFF +"  and dtLoginDate like '"+WebUtil.formatDate(WebUtil.getCurrentDate(strTimeGMT), IUMCConstants.DB_DATE_FORMAT)+"%' group by lngMemberID";
			List lstAttendStaff=new DBUtilStatic().getRecordsFromCenter(objEPMV, IUMCConstants.BN_ATTENDANCE_GYM_BEAN, "", strWhereAttendanceStaff, "");		
		   long lngStaffAttendance=0;
			if(null!=lstAttendStaff && lstAttendStaff.size()>0)
			{
				lngStaffAttendance=lstAttendStaff.size();
			}
			
	   //Renewal Today Done
			String strWhereRenewalDone=IUMCConstants.GET_ACTIVE_ROWS+" and "+IUMCConstants.LNG_MEM_RENEW_PAYMENT+" and dtReceiptDate ='"+WebUtil.formatDate(WebUtil.getCurrentDate(strTimeGMT), IUMCConstants.DB_DATE_FORMAT)+"'";
			List lstRenewalMem=new DBUtilStatic().getRecordsFromCenter(objEPMV, IUMCConstants.BN_MEMBER_PAYMENT_TRANSACTION, "", strWhereRenewalDone, "");
		    long lngRenewalCount=0;
		    
		    if(null!=lstRenewalMem && lstRenewalMem.size()>0)
		    {
		    	lngRenewalCount=lstRenewalMem.size();
		    }
			
			//Today Sales Amount
			//dblSalesTAmt=(Double)DBUtilStatic.getDoubleFromCenter(objEPMV, strTableName, "sum(dblPaidAmount)", strWhereClause, strSortClause)("sum(dblPaidAmount)",IUMCConstants.BN_INVOICE_HEADER,IUMCConstants.GET_ACTIVE_ROWS+"and dtInvoiceDate = '"+WebUtil.formatDate(WebUtil.getCurrentDate(strTimeGMT), IUMCConstants.DB_DATE_FORMAT)+"'","");
			
			
		// Get SMS configuration
				 String strWhereSMS=IUMCConstants.GET_ACTIVE_ROWS+" and intSMSType = "+IUMCConstants.TRANSACTIONAL;
				 SMSVariableCodeBean objSMSVar=(SMSVariableCodeBean) DBUtilStatic.getRecordFromCenter(objEPMV, IUMCConstants.BN_SMS_VARIABLE_CODE_BEAN, "", strWhereSMS, "");
				 if(null==objSMSVar) return false;
		
		if(null!=objSRMBV)
		{
			sendMessageToAdmin(objSMSVar,objSRMBV,objEPMV);
			
		  String strWhereSMSTemplate=IUMCConstants.GET_ACTIVE_ROWS+" and lngMsgTypeID = "+IUMCConstants.DAILY_SUMMARY+" and intSend = 1 and lngCompanyID ="+objSRMBV.getLngCompanyID();
		   SMSTemplateNotificationBean objSMSTNB=(SMSTemplateNotificationBean)DBUtilStatic.getRecordFromCenter(objEPMV, IUMCConstants.BN_SMS_TEMP_NOTIFICATION, "", strWhereSMSTemplate, "");
		   
		   String strWhereCompany=IUMCConstants.GET_ACTIVE_ROWS+" and lngID = "+objSRMBV.getLngCompanyID();
		   CompanyMasterBean objCMB=(CompanyMasterBean)DBUtilStatic.getRecordFromCenter(objEPMV, IUMCConstants.BN_COMPANY_MASTER_BEAN, "", strWhereCompany, "");
		  
		   //look
		   StringBuilder builder = new StringBuilder();
		   String strSMSAPI = "";
			 if(null!=objSMSVar && objSMSVar.getStrName().trim().length()>0)
			 {
			 strSMSAPI=objSMSVar.getStrName();
			 }
			 String strSMSAPIForCust = strSMSAPI ;
			 URL url = null; 
			 HttpURLConnection uc;
			 //look
		   if(null!=objSMSTNB)
		   {
			   //look
		         String strContent="";
		         String strSpecificContent = "";
			     strContent =objSMSTNB.getStrContent();
				 strContent = strContent.replaceAll(" ", "%20");
				 strSpecificContent=strContent;
				 
				 //To get the Admin Name of the center
				 String strWhereStaff= IUMCConstants.GET_ACTIVE_ROWS+" and lngID = "+objCMB.getLngAdminID();
				 StaffMasterBean objAdminStaff=(StaffMasterBean) DBUtilStatic.getRecordFromCenter(objEPMV, IUMCConstants.BN_STAFF_MASTER_BEAN, "", strWhereStaff, "");
				 String strAdminName="";
				 if(null!=objAdminStaff && objAdminStaff.getStrName().trim().length()>0)
				 {
					 strAdminName=objAdminStaff.getStrName()+" "+objAdminStaff.getStrLastName();
				 }
			   //look
			   
      // replace the sms template with actual text : replacing tokens to actual values
			   
				
		 			 String strName=objSRMBV.getStrName()+" "+objSRMBV.getStrLastName();
		 			 
		 			 if(null!=strName && strName.trim().length()>0)
		 			 {
		 			    strSpecificContent=strSpecificContent.replace(IUMCConstants.ST_CUST_NAME, strName);
		 			 }
		 			 else
		 			 {
		 				strSpecificContent=strSpecificContent.replace(IUMCConstants.ST_CUST_NAME,"");
		 			 }
		 			    if(null!=objSRMBV.getStrAddress() &&  objSRMBV.getStrAddress().trim().length()>0)
		 			    {
		 			    strSpecificContent=strSpecificContent.replace(IUMCConstants.ST_CUST_ADDRESS, objSRMBV.getStrAddress());
		 			    }
		 			    else
		 			    {
		 			    	strSpecificContent=strSpecificContent.replace(IUMCConstants.ST_CUST_ADDRESS, "");
		 			    }
		 			    
		 			   if(null!=objSRMBV.getStrLandlineNumber() &&  objSRMBV.getStrLandlineNumber().trim().length()>0)
		 			    {
		 			    strSpecificContent=strSpecificContent.replace(IUMCConstants.ST_CUST_PH_NO, objSRMBV.getStrLandlineNumber());
		 			    }
		 			   else
		 			   {
		 				  strSpecificContent=strSpecificContent.replace(IUMCConstants.ST_CUST_PH_NO, "");
		 			   }
		 			  if(null!=objSRMBV.getStrMobileNumber() &&  objSRMBV.getStrMobileNumber().trim().length()>0)
		 			  {
		 			    strSpecificContent=strSpecificContent.replace(IUMCConstants.ST_CUST_MOB_NO, objSRMBV.getStrMobileNumber());
		 			  }
		 			  else
		 			  {
		 				 strSpecificContent=strSpecificContent.replace(IUMCConstants.ST_CUST_MOB_NO,"");
		 			  }
		 			 if(null!=objSRMBV.getStrEmail() &&  objSRMBV.getStrEmail().trim().length()>0)
		 			  {
			 			strSpecificContent=strSpecificContent.replace(IUMCConstants.ST_CUST_EMAIL, objSRMBV.getStrEmail());
		 			  }
		 			 else
		 			 {
		 				strSpecificContent=strSpecificContent.replace(IUMCConstants.ST_CUST_EMAIL,"");
		 			 }
		 			 if(null!=objSRMBV.getDtDOB())
		 			  {
			 			strSpecificContent=strSpecificContent.replace(IUMCConstants.ST_DOB, objSRMBV.getDtDOB()+"");
		 			  }	
		 			 else
		 			 {
		 				strSpecificContent=strSpecificContent.replace(IUMCConstants.ST_DOB,"");
		 			 }
		 			if(null!=objSRMBV.getDtDOM() )
		 			  {
			 			strSpecificContent=strSpecificContent.replace(IUMCConstants.ST_DOM, objSRMBV.getDtDOM()+"");
		 			  }
		 			else
		 			{
		 				strSpecificContent=strSpecificContent.replace(IUMCConstants.ST_DOM,"");
		 			}
		 			
		 			//Added for Payment Summary
		 			
		 			strSpecificContent=strSpecificContent.replace(IUMCConstants.ST_PAYMENT_COLLECTED, dblPayment+"");
		 			
		 			strSpecificContent=strSpecificContent.replace(IUMCConstants.ST_NEWLY_JOINED, lngNew+"");
		 			strSpecificContent=strSpecificContent.replace(IUMCConstants.ST_ENQUIRY, lngEnq+"");
		 			strSpecificContent=strSpecificContent.replace(IUMCConstants.ST_MEMBER_ATTENDANCE, lstAttendMem+"");
		 			strSpecificContent=strSpecificContent.replace(IUMCConstants.ST_STAFF_ATTENDANCE, lstAttendStaff+"");
		 			strSpecificContent=strSpecificContent.replace(IUMCConstants.ST_RENEWED, lngRenewalCount+"");
		 			
		 			 if(null!=objCMB.getStrName() &&  objCMB.getStrName().trim().length()>0)
		 			  {
			 			strSpecificContent=strSpecificContent.replace(IUMCConstants.ST_CENTER_NAME, objCMB.getStrName());
		 			  }	
		 			 else
		 			 {
		 				strSpecificContent=strSpecificContent.replace(IUMCConstants.ST_CENTER_NAME, "");
		 			 }
		 			if(null!=objCMB.getStrPrimaryMobileNumber() &&  objCMB.getStrPrimaryMobileNumber().trim().length()>0)
		 			  {
			 			strSpecificContent=strSpecificContent.replace(IUMCConstants.ST_CENTER_NO, objCMB.getStrPrimaryMobileNumber());
		 			  }
		 			else
		 			{
		 				strSpecificContent=strSpecificContent.replace(IUMCConstants.ST_CENTER_NO,"");
		 			}
		 			if(null!=objCMB.getStrAddress() &&  objCMB.getStrAddress().trim().length()>0)
		 			  {
			 			strSpecificContent=strSpecificContent.replace(IUMCConstants.ST_CENTER_ADDRESS, objCMB.getStrAddress());
		 			  }
		 			else
		 			{
		 				strSpecificContent=strSpecificContent.replace(IUMCConstants.ST_CENTER_ADDRESS,"");
		 			}
		 			if(null!=strAdminName &&  strAdminName.trim().length()>0)
		 			  {
			 			strSpecificContent=strSpecificContent.replace(IUMCConstants.ST_CENTER_ADMIN, strAdminName);
		 			  }
		 			else
		 			{
		 				strSpecificContent=strSpecificContent.replace(IUMCConstants.ST_CENTER_ADMIN, "");
		 			}
		 			if(null!=objCMB.getStrContactEmail() &&  objCMB.getStrContactEmail().trim().length()>0)
		 			  {
			 			strSpecificContent=strSpecificContent.replace(IUMCConstants.ST_CENTER_EMAIL, objCMB.getStrContactEmail());
		 			  }
		 			else
		 			{
		 				strSpecificContent=strSpecificContent.replace(IUMCConstants.ST_CENTER_EMAIL, "");
		 			}
		 			
		 			 
		 			 
					 strSpecificContent = strSpecificContent.replaceAll(" ", "%20");
					 strSMSAPIForCust=strSMSAPIForCust.replace(IUMCConstants.ST_SEND_TO,objSRMBV.getStrMobileNumber());
					 strSMSAPIForCust = strSMSAPIForCust.replace(IUMCConstants.ST_SMS_TEXT, builder.append(strSpecificContent).toString());
			           
					 System.out.print(strSMSAPIForCust);
						try {
							url = new URL(strSMSAPIForCust);
							System.out.println(strSMSAPIForCust);
							uc = (HttpURLConnection)url.openConnection();
							uc.getResponseMessage();
						    uc.disconnect();
						    System.out.print(strSMSAPIForCust);
						    blnSent= true;
						} catch (MalformedURLException e) {
							// TODO Auto-generated catch block
							e.printStackTrace();
							blnSent= false;
						}
						catch (IOException e) {
					// TODO Auto-generated catch block
							e.printStackTrace();
							blnSent= false;
						}
				 
		   }
		}
		
		return blnSent;
		
	}
	
	public static String getTimeZone(EntityPlanMasterView objEPMV)
	{
	GlobalSettingBean objGSB = (GlobalSettingBean)	DBUtilStatic.getRecordFromCenter(objEPMV, IUMCConstants.BN_GLOBAL_SETTING, "", IUMCConstants.GET_ACTIVE_ROWS, "");
			
			if( null!=objGSB )
			{
				//To get the selected time zone of different countries
				 List lstTimeZone=DBUtilStatic.getRecordsFromCenter(objEPMV, IUMCConstants.BN_TIME_ZONE, "", IUMCConstants.GET_ACTIVE_ROWS, "");
		         
		         
		         if(null!=lstTimeZone)
		         {
		        	 
		        	 for(int i=0;i<lstTimeZone.size();i++)
		        	 {
		        	    TimeZoneConfigurationBean objTZCB=(TimeZoneConfigurationBean)lstTimeZone.get(i);
		        	    if(null!=objTZCB && objTZCB.getLngID()==objGSB.getLngTimeZone())
		        	    {
		        	    	if(null!=objTZCB.getStrTimeZone())
		        	    	{
		        	    		strTimeGMT=objTZCB.getStrTimeZone();
		        	    	}
		        	    }
		        	 }
		         }
				//Getting  the time zone of different countries ends
			}
			
			return strTimeGMT;
	}
	
	private static boolean isEmailEnabledForCenter(EntityPlanMasterView objEPMV) {
		String strTableName = IUMCConstants.BN_GLOBAL_SETTING;
		String strSelectList = "";
		String  strWhereClause = IUMCConstants.GET_ACTIVE_ROWS;
		String strSortClause = "strName, strLastName";
		
		GlobalSettingBean objGSB = (GlobalSettingBean)	DBUtilStatic.getRecordFromCenter(objEPMV, strTableName, strSelectList, strWhereClause, "");
		
		if( null!=objGSB && objGSB.getIntIsEmail()==1 )
		{
			return true;
		}
		else
		{
		return false;
		}
		
		
	}
	
	
	//Code related to pending Feedback Member
	private static boolean sendPendingFeedbackMemberMail(EntityPlanMasterView objEPMV){
		String strTableName=IUMCConstants.BN_MEMBER_MASTER_BEAN;
		String strSelectList="";
		String strSortClause="";
		String strWhereClause=IUMCConstants.GET_ACTIVE_ROWS+" and dtNextFeedbackDate <= '"+WebUtil.formatDate(WebUtil.getCurrentDate(strTimeGMT),IUMCConstants.DB_DATE_FORMAT)+"'";
		List lstPendingFeedbackMember= DBUtilStatic.getRecordsFromCenter(objEPMV, strTableName, strSelectList, strWhereClause, strSortClause);
		
		if(null!=lstPendingFeedbackMember && lstPendingFeedbackMember.size()>0)
		{
			for(int i=0;i<lstPendingFeedbackMember.size();i++)
			{
				MemberMasterBean objMMB=(MemberMasterBean)lstPendingFeedbackMember.get(i);
				
				if(null!=objMMB && objMMB.getIntEmailNotification()==1 &&   objMMB.getStrEmailID().trim().length()>0)
				{
					
					
					try {
						String strMemberID=objMMB.getLngID()+"";
						 String encodedID = DatatypeConverter.printBase64Binary(strMemberID.getBytes());
						
					
				
					MailVariableCodeBean objMVCB=null;
					objMVCB=(MailVariableCodeBean)DBUtilStatic.getRecordFromCenter(objEPMV, IUMCConstants.BN_MAIL_VARIABLE_CODE_BEAN, strSelectList, "", "");
					String strFrom = objMVCB.getStrUserName();		
					String strTo=objMMB.getStrEmailID();		
							
							
							 
							String strSubject = "Feedback form " ; 
							String strDesc = "Hi, <br> Click here to fill the feedback form.<a href='http://localhost:8080/MemberCentrum/Transaction/feedBackFormAll.jsp?"+ILabelConstants.ID+"="+encodedID+"&"+ILabelConstants.FEEDBACKTYPE+"="+IUMCConstants.INT_FEEDBACK_TYPE_MEMBEER+"&"+IUMCConstants.FROM_PAGE+"="+IUMCConstants.INT_FEEDBACK_PENDING+"&PAGE=MAIL_FEEDBACK' target='_blank'> Link </a><br>Regards,<br>MC";
							//get batch list 
							
							
							
								 SendMailToCenter.sendMailToCenter(objEPMV,strFrom,strTo, null, null, strSubject, strDesc,objMVCB);
								//blnMailSent=true;
								
								
							} catch (Exception e) {
								// TODO Auto-generated catch block
								e.printStackTrace();
								
								 
							}
				}			
			}
			
		}
		
		return false;
		
	}
	
	
	public static void sendMessageToAdmin(SMSVariableCodeBean objSMSVar,StaffRoleMasterBeanView objSRMBV,EntityPlanMasterView objEPMV){
		
		boolean blnSent = false;
		 String strWhereSMSTemplate=IUMCConstants.GET_ACTIVE_ROWS+" and lngMsgTypeID = "+IUMCConstants.DAILY_SUMMARY+" and intSend = 1 and lngCompanyID ="+objSRMBV.getLngCompanyID();
		   SMSTemplateNotificationBean objSMSTNB=(SMSTemplateNotificationBean)DBUtilStatic.getRecordFromCenter(objEPMV, IUMCConstants.BN_SMS_TEMP_NOTIFICATION, "", strWhereSMSTemplate, "");
		   
		   String strWhereCompany=IUMCConstants.GET_ACTIVE_ROWS+" and lngID = "+objSRMBV.getLngCompanyID();
		   CompanyMasterBean objCMB=(CompanyMasterBean)DBUtilStatic.getRecordFromCenter(objEPMV, IUMCConstants.BN_COMPANY_MASTER_BEAN, "", strWhereCompany, "");
		  
		 //Get the sum of payment
			MemberPaymentTransactionBean objMPTB=new MemberPaymentTransactionBean();
			double dblPayment=DBUtilStatic.getDoubleFromCenter(objEPMV, IUMCConstants.BN_MEMBER_PAYMENT_TRANSACTION, "sum(dbPayingAmount)", IUMCConstants.GET_ACTIVE_ROWS+" and dtReceiptDate = '"+WebUtil.formatDate(WebUtil.getCurrentDate(strTimeGMT), IUMCConstants.DB_DATE_FORMAT)+"'", "");
			
			//Get Newly joined members
			Long LngNew=(Long)DBUtilStatic.getRecordFromCenter(objEPMV, IUMCConstants.BN_MEMBER_MASTER_BEAN_VIEW, "count(*)", IUMCConstants.GET_ACTIVE_ROWS+"  and dtDOJ = '"+WebUtil.formatDate(WebUtil.getCurrentDate(strTimeGMT), IUMCConstants.DB_DATE_FORMAT)+"'", "");
			long lngNew=0;
			if(null!=LngNew)
			{
				lngNew=(LngNew).longValue();
			}
			
			//Get Enquiry Members
			
			 String strWhereEnq=IUMCConstants.GET_ACTIVE_ROWS+"  dtEnquiryDate like '"+WebUtil.formatDate(WebUtil.getCurrentDate(strTimeGMT), IUMCConstants.DB_DATE_FORMAT)+"%'";
			 List  lstEnq=new DBUtilStatic().getRecordsFromCenter(objEPMV, IUMCConstants.BN_ENQUIRY_HEADER_TRANSACTION_BEAN, "", strWhereEnq, "");
			 long lngEnq=0;
			 
				if(null!=lstEnq && lstEnq.size()>0)
				{
					lngEnq=lstEnq.size();
				}
			
			//Attendance Member 
			
			 String strWhereAttendanceMem=IUMCConstants.GET_ACTIVE_ROWS+" and intLoggTypeID ="+IUMCConstants.LOGGED_IS_MEMBER +"  and dtLoginDate like '"+WebUtil.formatDate(WebUtil.getCurrentDate(strTimeGMT), IUMCConstants.DB_DATE_FORMAT)+"%' group by lngMemberID";
			 List  lstAttendMem=new DBUtilStatic().getRecordsFromCenter(objEPMV, IUMCConstants.BN_ATTENDANCE_GYM_BEAN, "", strWhereAttendanceMem, "");
			 long lngMemAttendance=0;
			 
				if(null!=lstAttendMem && lstAttendMem.size()>0)
				{
					lngMemAttendance=lstAttendMem.size();
				}
				 
			//Attendace Staff	
				String strWhereAttendanceStaff=IUMCConstants.GET_ACTIVE_ROWS+"  and intLoggTypeID ="+IUMCConstants.LOGGED_IS_STAFF +"  and dtLoginDate like '"+WebUtil.formatDate(WebUtil.getCurrentDate(strTimeGMT), IUMCConstants.DB_DATE_FORMAT)+"%' group by lngMemberID";
				List lstAttendStaff=new DBUtilStatic().getRecordsFromCenter(objEPMV, IUMCConstants.BN_ATTENDANCE_GYM_BEAN, "", strWhereAttendanceStaff, "");		
			   long lngStaffAttendance=0;
				if(null!=lstAttendStaff && lstAttendStaff.size()>0)
				{
					lngStaffAttendance=lstAttendStaff.size();
				}
				
		   //Renewal Today Done
				String strWhereRenewalDone=IUMCConstants.GET_ACTIVE_ROWS+" and "+IUMCConstants.LNG_MEM_RENEW_PAYMENT+" and dtReceiptDate ='"+WebUtil.formatDate(WebUtil.getCurrentDate(strTimeGMT), IUMCConstants.DB_DATE_FORMAT)+"'";
				List lstRenewalMem=new DBUtilStatic().getRecordsFromCenter(objEPMV, IUMCConstants.BN_MEMBER_PAYMENT_TRANSACTION, "", strWhereRenewalDone, "");
			    long lngRenewalCount=0;
			    
			    if(null!=lstRenewalMem && lstRenewalMem.size()>0)
			    {
			    	lngRenewalCount=lstRenewalMem.size();
			    }
		   
		   //look
		   StringBuilder builder = new StringBuilder();
		   String strSMSAPI = "";
			 if(null!=objSMSVar && objSMSVar.getStrName().trim().length()>0)
			 {
			 strSMSAPI=objSMSVar.getStrName();
			 }
			 String strSMSAPIForCust = strSMSAPI ;
			 URL url = null; 
			 HttpURLConnection uc;
			 //look
		   if(null!=objSMSTNB)
		   {
			   //look
		         String strContent="";
		         String strSpecificContent = "";
			     strContent =objSMSTNB.getStrContent();
				 strContent = strContent.replaceAll(" ", "%20");
				 strSpecificContent=strContent;
				 
				 //To get the Admin Name of the center
				 String strWhereStaff= IUMCConstants.GET_ACTIVE_ROWS+" and lngID = "+objCMB.getLngAdminID();
				 StaffMasterBean objAdminStaff=(StaffMasterBean) DBUtilStatic.getRecordFromCenter(objEPMV, IUMCConstants.BN_STAFF_MASTER_BEAN, "", strWhereStaff, "");
				 String strAdminName="";
				 if(null!=objAdminStaff && objAdminStaff.getStrName().trim().length()>0)
				 {
					 strAdminName=objAdminStaff.getStrName()+" "+objAdminStaff.getStrLastName();
				 }
			   //look
			   
    // replace the sms template with actual text : replacing tokens to actual values
			   
				
		 			 String strName=objSRMBV.getStrName()+" "+objSRMBV.getStrLastName();
		 			 
		 			 if(null!=strName && strName.trim().length()>0)
		 			 {
		 			    strSpecificContent=strSpecificContent.replace(IUMCConstants.ST_CUST_NAME, strName);
		 			 }
		 			 else
		 			 {
		 				strSpecificContent=strSpecificContent.replace(IUMCConstants.ST_CUST_NAME,"");
		 			 }
		 			    if(null!=objSRMBV.getStrAddress() &&  objSRMBV.getStrAddress().trim().length()>0)
		 			    {
		 			    strSpecificContent=strSpecificContent.replace(IUMCConstants.ST_CUST_ADDRESS, objSRMBV.getStrAddress());
		 			    }
		 			    else
		 			    {
		 			    	strSpecificContent=strSpecificContent.replace(IUMCConstants.ST_CUST_ADDRESS, "");
		 			    }
		 			    
		 			   if(null!=objSRMBV.getStrLandlineNumber() &&  objSRMBV.getStrLandlineNumber().trim().length()>0)
		 			    {
		 			    strSpecificContent=strSpecificContent.replace(IUMCConstants.ST_CUST_PH_NO, objSRMBV.getStrLandlineNumber());
		 			    }
		 			   else
		 			   {
		 				  strSpecificContent=strSpecificContent.replace(IUMCConstants.ST_CUST_PH_NO, "");
		 			   }
		 			  if(null!=objSRMBV.getStrMobileNumber() &&  objSRMBV.getStrMobileNumber().trim().length()>0)
		 			  {
		 			    strSpecificContent=strSpecificContent.replace(IUMCConstants.ST_CUST_MOB_NO, objSRMBV.getStrMobileNumber());
		 			  }
		 			  else
		 			  {
		 				 strSpecificContent=strSpecificContent.replace(IUMCConstants.ST_CUST_MOB_NO,"");
		 			  }
		 			 if(null!=objSRMBV.getStrEmail() &&  objSRMBV.getStrEmail().trim().length()>0)
		 			  {
			 			strSpecificContent=strSpecificContent.replace(IUMCConstants.ST_CUST_EMAIL, objSRMBV.getStrEmail());
		 			  }
		 			 else
		 			 {
		 				strSpecificContent=strSpecificContent.replace(IUMCConstants.ST_CUST_EMAIL,"");
		 			 }
		 			 if(null!=objSRMBV.getDtDOB())
		 			  {
			 			strSpecificContent=strSpecificContent.replace(IUMCConstants.ST_DOB, objSRMBV.getDtDOB()+"");
		 			  }	
		 			 else
		 			 {
		 				strSpecificContent=strSpecificContent.replace(IUMCConstants.ST_DOB,"");
		 			 }
		 			if(null!=objSRMBV.getDtDOM() )
		 			  {
			 			strSpecificContent=strSpecificContent.replace(IUMCConstants.ST_DOM, objSRMBV.getDtDOM()+"");
		 			  }
		 			else
		 			{
		 				strSpecificContent=strSpecificContent.replace(IUMCConstants.ST_DOM,"");
		 			}
		 			
		 			//Added for Payment Summary
		 			
		 			
		 			strSpecificContent=strSpecificContent.replace(IUMCConstants.ST_PAYMENT_COLLECTED, dblPayment+"");
		 			
		 			strSpecificContent=strSpecificContent.replace(IUMCConstants.ST_NEWLY_JOINED, lngNew+"");
		 			strSpecificContent=strSpecificContent.replace(IUMCConstants.ST_ENQUIRY, lngEnq+"");
		 			strSpecificContent=strSpecificContent.replace(IUMCConstants.ST_MEMBER_ATTENDANCE, lstAttendMem+"");
		 			strSpecificContent=strSpecificContent.replace(IUMCConstants.ST_STAFF_ATTENDANCE, lstAttendStaff+"");
		 			strSpecificContent=strSpecificContent.replace(IUMCConstants.ST_RENEWED, lngRenewalCount+"");
		 			
		 			 if(null!=objCMB.getStrName() &&  objCMB.getStrName().trim().length()>0)
		 			  {
			 			strSpecificContent=strSpecificContent.replace(IUMCConstants.ST_CENTER_NAME, objCMB.getStrName());
		 			  }	
		 			 else
		 			 {
		 				strSpecificContent=strSpecificContent.replace(IUMCConstants.ST_CENTER_NAME, "");
		 			 }
		 			if(null!=objCMB.getStrPrimaryMobileNumber() &&  objCMB.getStrPrimaryMobileNumber().trim().length()>0)
		 			  {
			 			strSpecificContent=strSpecificContent.replace(IUMCConstants.ST_CENTER_NO, objCMB.getStrPrimaryMobileNumber());
		 			  }
		 			else
		 			{
		 				strSpecificContent=strSpecificContent.replace(IUMCConstants.ST_CENTER_NO,"");
		 			}
		 			if(null!=objCMB.getStrAddress() &&  objCMB.getStrAddress().trim().length()>0)
		 			  {
			 			strSpecificContent=strSpecificContent.replace(IUMCConstants.ST_CENTER_ADDRESS, objCMB.getStrAddress());
		 			  }
		 			else
		 			{
		 				strSpecificContent=strSpecificContent.replace(IUMCConstants.ST_CENTER_ADDRESS,"");
		 			}
		 			if(null!=strAdminName &&  strAdminName.trim().length()>0)
		 			  {
			 			strSpecificContent=strSpecificContent.replace(IUMCConstants.ST_CENTER_ADMIN, strAdminName);
		 			  }
		 			else
		 			{
		 				strSpecificContent=strSpecificContent.replace(IUMCConstants.ST_CENTER_ADMIN, "");
		 			}
		 			if(null!=objCMB.getStrContactEmail() &&  objCMB.getStrContactEmail().trim().length()>0)
		 			  {
			 			strSpecificContent=strSpecificContent.replace(IUMCConstants.ST_CENTER_EMAIL, objCMB.getStrContactEmail());
		 			  }
		 			else
		 			{
		 				strSpecificContent=strSpecificContent.replace(IUMCConstants.ST_CENTER_EMAIL, "");
		 			}
		 			
		 			 
		 			 
					 strSpecificContent = strSpecificContent.replaceAll(" ", "%20");
					 strSMSAPIForCust=strSMSAPIForCust.replace(IUMCConstants.ST_SEND_TO,objSRMBV.getStrMobileNumber());
					 strSMSAPIForCust = strSMSAPIForCust.replace(IUMCConstants.ST_SMS_TEXT, builder.append(strSpecificContent).toString());
			           
					 System.out.print(strSMSAPIForCust);
						try {
							url = new URL(strSMSAPIForCust);
							System.out.println(strSMSAPIForCust);
							uc = (HttpURLConnection)url.openConnection();
							uc.getResponseMessage();
						    uc.disconnect();
						    System.out.print(strSMSAPIForCust);
						    blnSent= true;
						} catch (MalformedURLException e) {
							// TODO Auto-generated catch block
							e.printStackTrace();
							blnSent= false;
						}
						catch (IOException e) {
					// TODO Auto-generated catch block
							e.printStackTrace();
							blnSent= false;
						}
				 
		   }
	}
	
}
