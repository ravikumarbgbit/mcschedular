package com.uthkrushta.utils;
import java.util.Date;
import java.util.List;
import java.util.Random;
import java.util.TimerTask;

import javax.servlet.http.HttpServletRequest;

import org.hibernate.Session;

import com.uthkrushta.basis.constants.IUMCConstants;
import com.uthkrushta.mm.supreme.control.EntityPlanMasterView;
import com.uthkrushta.mm.utility.controller.NotificationController;



public class AutoTrigger extends TimerTask{
 
	
	Date now;
	@Override
	public void run() {
		 final AutoTrigger timer = new AutoTrigger();
		 EntityPlanMasterView objEPMV = new EntityPlanMasterView();
		 
		 // Get List of Centers from super centrum DB
		 List lstCenters = DBUtilStatic.getActiveCenters(IUMCConstants.BN_ENTITY_VIEW);
		 List lstMembers = null;
		 if(null == lstCenters)return;
		
		 
		 // Loop on ma_entity and connect to all active gyms having sms notification set to their plan
		 for(int i=0;i<lstCenters.size();i++){
			 objEPMV = (EntityPlanMasterView) lstCenters.get(i);
			 
			 // Delegate to Notification Controller 
			 if(null == objEPMV) continue;
			 
			 // If the center has SMS pack
			 if(objEPMV.getIntIsTransactionalSMS() == 0) continue;
			 
			 NotificationController.delegateCall(objEPMV);
			 
		 }
		 
		
		 
		
		
		
	}
}
