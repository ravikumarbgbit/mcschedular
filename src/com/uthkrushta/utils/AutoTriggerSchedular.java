package com.uthkrushta.utils;

import java.util.List;
import java.util.TimerTask;

import com.uthkrushta.basis.constants.IUMCConstants;
import com.uthkrushta.mm.supreme.control.EntityPlanMasterView;
import com.uthkrushta.mm.utility.controller.NotificationController;
import com.uthkrushta.mm.utility.controller.SchedularController;

public class AutoTriggerSchedular extends TimerTask {

	@Override
	public void run() {
		// TODO Auto-generated method stub
       
		 final AutoTriggerSchedular timer = new AutoTriggerSchedular();
		 EntityPlanMasterView objEPMV = new EntityPlanMasterView();
		 
		 // Get List of Centers from super centrum DB
		 List lstCenters = DBUtilStatic.getActiveCenters(IUMCConstants.BN_ENTITY_VIEW);
		 List lstMembers = null;
		 if(null == lstCenters)return;
		 System.out.println("check");
		 
		 // Loop on ma_entity and connect to all active gyms having sms notification set to their plan
		 for(int i=0;i<lstCenters.size();i++){
			 objEPMV = (EntityPlanMasterView) lstCenters.get(i);
			 
			 // Delegate to Notification Controller 
			 if(null == objEPMV) continue;
			 NotificationController.delegateCall(objEPMV);
			 SchedularController.delegateCall(objEPMV);
		 }
		
	}
  
}
