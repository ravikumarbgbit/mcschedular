package com.uthkrushta.utils;

import java.util.Date;
import java.util.Properties;
import javax.mail.*;
import javax.mail.internet.*;
import javax.servlet.http.HttpServletRequest;

import com.uthkrushta.mm.code.bean.MailVariableCodeBean;
import com.uthkrushta.mm.supreme.control.EntityPlanMasterView;
import com.uthkrushta.mm.utility.controller.SchedularController;


public class SendMailToCenter {
	
	static private String strUserName="";
	static private String strPassword="";
	 public static boolean sendMailToCenter(EntityPlanMasterView objEPMV, String strFrom,String to,String strCC,String strBCC,String subject,String content,MailVariableCodeBean objMVCB) throws Exception 
	    {
		    strUserName=objMVCB.getStrUserName();
			strPassword=objMVCB.getStrPassword();
		 	boolean blnMailSent = false;
	        String host =objMVCB.getStrHostName();//"bh-31.webhostbox.net"; //"serverla1.netider.com";
	        //String host = "localhost";
	        Properties props = new Properties();
	    //    props.load(new FileInputStream(props));
	        props.put("mail.smtp.host", host);
	        props.put("mail.smtp.auth", "true");
	        SMTPAuthenticator auth = new SMTPAuthenticator();

	        Session session = Session.getDefaultInstance(props, auth);
	        //BodyPart bp2 = getFileBodyPart("MailTemplates/mailTempECSConfirmation.jsp", "text/html");

	        Message msg = new MimeMessage(session);
	      
	        msg.setFrom(new InternetAddress(strFrom));
	        InternetAddress[] address = { new InternetAddress(to) };
	        msg.setRecipients(Message.RecipientType.TO, address);
	        
	        if(null != strCC){
	        	InternetAddress[] addressCC = { new InternetAddress(strCC) };
	        	msg.setRecipients(Message.RecipientType.CC, addressCC);
	        }
	        if(null != strBCC){
	        	InternetAddress[] addressBCC = { new InternetAddress(strBCC) };
	        	msg.setRecipients(Message.RecipientType.BCC, addressBCC);
	        }
	        
	        msg.setSubject(subject);
	        msg.setSentDate(WebUtil.getCurrentDate(SchedularController.getTimeZone(objEPMV)));
	        //msg.setText(content);
	        msg.setContent(content, "text/html");
	        Transport.send(msg);
	        blnMailSent = true;
	        System.out.print(" Mail Sent");
	        return blnMailSent;
	    }

	    private static class SMTPAuthenticator extends javax.mail.Authenticator 
	    {
	        public PasswordAuthentication getPasswordAuthentication() 
	        {
	            return new PasswordAuthentication(strUserName,strPassword);// info@brickjoy.com  ,brickjoy@2015
	        }
	    }
	    

	    public static void main(String[] args) throws Exception 
	    {
	    	String strContent = "<!DOCTYPE html>" +
	    						"<html><head><meta http-equiv='Content-Type' content='text/html; charset=ISO-8859-1'>" +
	    						"<title>Insert title here</title></head>" +
	    						"<body>" +
	    						"<form method='post'>" +
	    						"<center><img border='0' src='http://localhost:8080/uPackNMove/Icons/Layout/logo.png' ><center><font color='blue'> This is the content </font>" +
	    						"</form> " +
	    						"</body> </html>";
	    			
	    			//"<center><img border='0' src='http://localhost:8080/uPackNMove/Icons/Layout/logo.png'><center><font color='blue'> This is the content </font>";
	     //   sendMail("tosanthoshv@gmail.com","tosanthoshv@gmail.com","santhosh.v@uthkrushta.in","santhosh.v@uthkrushta.in", "Hi", strContent);
	    	
	    	
	    }   

}
