package com.uthkrushta.utils;

import java.io.IOException;
import java.util.ArrayList;
import java.util.StringTokenizer;

import javax.servlet.Filter;
import javax.servlet.FilterChain;
import javax.servlet.FilterConfig;
import javax.servlet.ServletException;
import javax.servlet.ServletRequest;
import javax.servlet.ServletResponse;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

public class SessionCheckFilter implements Filter{

	private ArrayList<String> urlList;
	 
	@Override
	public void destroy() {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void doFilter(ServletRequest servletRequest, ServletResponse servletResponse,
			FilterChain filterChain) throws IOException, ServletException {
		
		HttpServletRequest request = (HttpServletRequest) servletRequest;
        HttpServletResponse response = (HttpServletResponse) servletResponse;
        String url = request.getServletPath();
        boolean allowedRequest = false;
        System.out.println("Before : allowedRequest : " + allowedRequest  + " url : " + url);
        if(urlList.contains(url)) {
            allowedRequest = true;
        }
        System.out.println("AFTER : allowedRequest : " + allowedRequest);    
        
        if (!allowedRequest) {
            HttpSession session = request.getSession(false);
            System.out.println("CHECK SESSION  : " + session);
            System.out.println("User Action  : " + session);
            if (null == session) {
                response.sendRedirect("../sessionOut.jsp");
                return;
            }
        }
         
        filterChain.doFilter(servletRequest, servletResponse);
		
		
	}

	@Override
	public void init(FilterConfig filterConfig) throws ServletException {
		String urls = filterConfig.getInitParameter("avoid-urls");
        StringTokenizer token = new StringTokenizer(urls, ",");
        System.out.println("ALLOWED URLs  : " + urls);
        urlList = new ArrayList<String>();
 
        while (token.hasMoreTokens()) {
            urlList.add(token.nextToken());
 
        }
        System.out.println("ALLOWED URL urlList  : " + urlList.size());
		
	}

}
